////////////////////////////////////////////////////////////////////////////////
// Filename: buttonclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _BUTTONCLASS_H_
#define _BUTTONCLASS_H_

///////////////////////
// MY CLASS INCLUDES //
///////////////////////
// ����� ������� � ��������� ���� ����� ��������������� ����

// ������  delegate ��� function �� ������ �� ���� :)
#include "widgetclass.h"

#include "textclass.h"

enum class type_state_control { NORMAL, HOVER, ACTIVE };


class ButtonClass: public BoxClass{
public:
	type_state_control state;
	// �������, ��������� ��� ��������� � ������ �� ����������
	typedef CDelegate3<void, int, int,int> Callback;
	ButtonClass();
	~ButtonClass();
	bool Update(InputClass*, int, int );
	bool Create(ID3D10Device*, TextureManagerClass*, int, int);
	bool Render(ID3D10Device*, ShaderManagerClass*, TextureManagerClass*,
				D3DXMATRIX, D3DXMATRIX, D3DXMATRIX);
	void Shutdown(void);
	void SetText(std::string);
	// ������� �������, ����� ������?
	Callback eventOnClick;
	void onClick(int, int, int);
	KeyTextureClass normal; // �������?
	KeyTextureClass hover;
	KeyTextureClass active;
private:
	TextClass* m_Text;


};

#endif

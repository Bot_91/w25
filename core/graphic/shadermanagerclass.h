////////////////////////////////////////////////////////////////////////////////
// Filename: shadermanagerclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _SHADERMANAGERCLASS_H_
#define _SHADERMANAGERCLASS_H_
///////////////////////
// MY CLASS INCLUDES //
///////////////////////
#include "./shader/d3d/d3dclass.h"
#include "./shader/textureshaderclass.h"
#include "./shader/fontshaderclass.h"
#include "./shader/lightshaderclass.h"
#include "./shader/bumpmapshaderclass.h"
#include "./shader/multitextureshaderclass.h"
#include "./shader/terrainshaderclass.h"

#include "modelmanagerclass.h"
#include "texturemanagerclass.h"

////////////////////////////////////////////////////////////////////////////////
// Class name: ShaderManagerClass
////////////////////////////////////////////////////////////////////////////////
class ShaderManagerClass{
public:
	ShaderManagerClass();
	ShaderManagerClass(const ShaderManagerClass&);
	~ShaderManagerClass();

	bool Initialize(ID3D10Device*, HWND, TextureManagerClass*);
	void Shutdown();

	void RenderTextureShader(ID3D10Device*, int, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D10ShaderResourceView*);

	void RenderFontShader(ID3D10Device*, int, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D10ShaderResourceView*, D3DXVECTOR4);

	void RenderLightShader(ID3D10Device*, int, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D10ShaderResourceView*, 
						  D3DXVECTOR3, D3DXVECTOR4, D3DXVECTOR4, D3DXVECTOR3, D3DXVECTOR4, float);

	void RenderBumpMapShader(ID3D10Device*, int, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D10ShaderResourceView*, 
							ID3D10ShaderResourceView*, D3DXVECTOR3, D3DXVECTOR4);

	void RenderMultiTextureShader(ID3D10Device*, int, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D10ShaderResourceView*,
								 ID3D10ShaderResourceView*);

	void RenderTerrainShader(ID3D10Device*, int, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, 
							std::vector<MaterialInfoType>, 
							D3DXVECTOR3, D3DXVECTOR4, float);
private:
	TextureShaderClass* m_TextureShader;
	LightShaderClass* m_LightShader;
	BumpMapShaderClass* m_BumpMapShader;
	TerrainShaderClass* m_TerrainShader;
	FontShaderClass* m_FontShader;
	MultiTextureShaderClass* m_MultiTextureShader;
	//MaterialShaderClass* m_MaterialShader;
	// ��������� �� ��������� ��������
	TextureManagerClass* p_TextureManager;
};

#endif
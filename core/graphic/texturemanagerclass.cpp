// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
////////////////////////////////////////////////////////////////////////////////
// Filename: texturemanagerclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "texturemanagerclass.h"
PackTexture::PackTexture(void){
	m_Texture = 0;	
	countLink = 0;
	m_Path = "NULL";
}
bool PackTexture::Shutdown(void){
	if (m_Texture){
		m_Texture->Shutdown();
		m_Texture = 0;
	}

	countLink = 0;
	return true;
}
// ���������� ��������
PackTexture::~PackTexture(void){
}

TextureManagerClass::TextureManagerClass(void){
	m_Pack.clear();
}
bool TextureManagerClass::Initialize(ID3D10Device* device){
	bool result;
	// ��������� �������� ������
	PackTexture buffTexture;
	buffTexture.countLink = 1;
	buffTexture.m_Path = "NULL";
	// ��������� � �������
	buffTexture.m_Texture = new TextureClass;
	result = buffTexture.m_Texture->Initialize(device, "./data/errorTexture.jpg");
	if (!result) {
		return false;
	}
	// ��������� ����� ��������
	m_Pack.insert(std::pair<std::string, PackTexture>("NULL", buffTexture));
	return true;
}

// ��������� �������, ���� � �������� �������� ���� � �����
std::string TextureManagerClass::Add(ID3D10Device* device, std::string path){
	bool result;
	std::map<std::string,PackTexture>::iterator iter;
	// ���� ���� � ����������
	iter = m_Pack.find(path);
	// ���� ������ �� ����������� ������� ������ �� ��������
	if (iter!=m_Pack.end()){
		// ���������� � ������
		iter->second.countLink +=1;
	// ����� ��������� ����� ��������
	} else{
		// ��������� ���������
		//
		if (path == "NULL"){
			return "ERROR";
		}
		////////////////////////////////////////////////
		PackTexture buffTexture;
		buffTexture.countLink = 1;
		buffTexture.m_Path = path;
		buffTexture.m_Texture = new TextureClass;
		result = buffTexture.m_Texture->Initialize(device, path);
		if (!result){
			return "ERROR";
		}
		// ��������� ����� ��������
		m_Pack.insert( std::pair<std::string,PackTexture>(path,buffTexture) );
	}
	
	// ���������� ���� ��������
	return path;
}
// ��������� ��������������� ��������
void TextureManagerClass::Add(ID3D10Device* device, PackTexture inPackTexture) {
	std::map<std::string, PackTexture>::iterator iter;
	// ���� ���� � ����������
	iter = m_Pack.find(inPackTexture.m_Path);
	// ���� ������ �� ����������� ������� ������ �� ��������
	if (iter != m_Pack.end()) {
		// ���������� � ������
		iter->second.countLink = 1;
		iter->second.m_Texture->SetTexture(inPackTexture.m_Texture->GetTexture());
		// ����� ��������� ����� ��������
	}else {
		////////////////////////////////////////////////
		m_Pack.insert(std::pair<std::string, PackTexture>(inPackTexture.m_Path, inPackTexture));
	}
}

// ��������� ���������� ������ �� ��������, ��� ���������� ������ ����������� ������
void TextureManagerClass::Erase(std::string key){

	std::map<std::string,PackTexture>::iterator iter;
	// ���� ���� � ����������
	iter = m_Pack.find(key);
	// ��������� ������� ������ �� ��������
	if (iter!=m_Pack.end() && key != "ERROR" && key != "NULL"){
		// ������� ��� ��������� ��������, ��� ������� (������, ��� �� ���������
		if (iter->second.countLink == 0){
			MessageBox(NULL, L"Texture", L"Warhing", MB_OK);
			return;
		}
		iter->second.countLink -=1;
		// ������ �� �������� ����������� ������ ������
		if (iter->second.countLink==0 ){
			iter->second.Shutdown();
			m_Pack.erase(iter);
		}
	}
	return;
}

// ���������� �������� �� �����
ID3D10ShaderResourceView* TextureManagerClass::GetTexture(std::string key){
	std::map<std::string,PackTexture>::iterator iter;
	// ���� ���� � ����������
	iter = m_Pack.find(key);
	if (iter!=m_Pack.end()){
		return iter->second.m_Texture->GetTexture();
	// �� ����� ���� �������� ������
	} else {	
		iter = m_Pack.find("NULL");
		if (iter != m_Pack.end()) {
			return iter->second.m_Texture->GetTexture();
		}
	}
	return NULL;
}

// ������� ��� �������� �� ������
void TextureManagerClass::Shutdown(void){
	// ERROR NULL �������� �� ��������� ��������
	std::map<std::string,PackTexture>::iterator iter;
	for ( iter = m_Pack.begin(); iter != m_Pack.end(); ++iter){
		iter->second.Shutdown();
		//m_Pack.erase(iter);
	}
	m_Pack.clear();
}

// ���������� ��������� �������
unsigned int  TextureManagerClass::GetCountPack(void){	
	return m_Pack.size();
}
// ���������� ������������ �������� �� ��������
unsigned int TextureManagerClass::GetCountLink(std::string key){
	std::map<std::string,PackTexture>::iterator iter;
	// ���� ���� � ����������
	iter = m_Pack.find(key);
	if (iter!=m_Pack.end()){
		return iter->second.countLink;
	}
	return 0;
}

// ���������� �������� �� ������ � � ����������
ID3D10ShaderResourceView* TextureManagerClass::GetTextureByItem(unsigned int index){
	std::map<std::string,PackTexture>::iterator iter;
	int count=0;
	for ( iter = m_Pack.begin(); iter != m_Pack.end(); iter++ ){
		if (index==count){
			return iter->second.m_Texture->GetTexture();
		}
		count++;
	}
	return NULL;
}
std::string TextureManagerClass::GetPathTexture(unsigned int index){
	std::map<std::string,PackTexture>::iterator iter;
	int count=0;
	for ( iter = m_Pack.begin(); iter != m_Pack.end(); iter++ ){
		if (index==count){
			return iter->second.m_Path;
		}
		count++;
	}
	return 0;
}
////////////////////////////////////////////////////////////////////////////////
// Filename: TerrainClass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _TerrainClass_H_
#define _TerrainClass_H_


//////////////
// INCLUDES //
//////////////
#include "terraintileclass.h"

#include "shadermanagerclass.h"
#include "texturemanagerclass.h"
#include "lightclass.h"
// ������ ���������� ��������� �������
#include "frustumclass.h"
#include <list>
////////////////////////////////////////////////////////////////////////////////
// Class name: TerrainClass
////////////////////////////////////////////////////////////////////////////////
// ��������� ��������� �� ������
class QuadTreeClass{
public:
	QuadTreeClass();
	~QuadTreeClass();
	// ���������� ������� �����
	bool GetIndexVisibleTile(FrustumClass* FrustumView, std::vector<int>&);
	int GetIndexTilePoint(D3DXVECTOR3 position);

	// ������� ������ �� �������� ������ ��� ����� � ����������� �� pos
	// � ��������� width x height � ������� widthTile x heightTile
	void CreatTree(	::vector<int> indexTile, D3DXVECTOR3 pos, 
					int width, int height,
					float widthTile, float heightTile);
private:
	// ������ �����
	D3DXVECTOR3 QuadPosition;
	D3DXVECTOR3 QuadSize;
	int m_IndexQuad;
	// �������
	QuadTreeClass* northWest;
	QuadTreeClass* northEast;
	QuadTreeClass* southWest;
	QuadTreeClass* southEast;
};

class TerrainClass {
public:
	long int delayTime;
	bool Initialize(ID3D10Device*, ShaderManagerClass*, TextureManagerClass*, LightClass*, std::string);
	bool SaveTerrainMap(std::string );
	bool LoadTerrainMap(std::string );
	bool CreateTerrainMap(	ID3D10Device*, int, int);
	void Render(ID3D10Device*, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, FrustumClass*);
	void SetStateRender(int);
	void Shutdown(void);
	void AlterArea(ID3D10Device* device, D3DXVECTOR3 position,
				   float value, int radius, alter_argument argument);
	bool AlterPoint(D3DXVECTOR3 point, float, alter_argument argument);
	bool SetCamputeMeanNTB(D3DXVECTOR3 point);
	bool GetHeight(D3DXVECTOR3, float&);
	bool GetHeight(D3DXVECTOR3, float&, D3DXVECTOR3&);
	bool GetIntersects(D3DXVECTOR3, D3DXVECTOR3, D3DXVECTOR3&);
	bool CreateRound(ID3D10Device* device, D3DXVECTOR3 position, float radius);
	TerrainClass();
private:
	std::list<int> listEditTile;
	TerrainTileClass::VertexType TerrainClass::GetVertex(D3DXVECTOR3 point);
	void fileReadString(std::ifstream& file, std::string& text);
	void fileWriteString(std::ofstream& file, std::string text);
	std::string pathMap;
	TerrainRoundClass* m_Round;
	TerrainTileClass* m_Tile;
	// ������ ��������� � �������
	int m_TerrainWidth;
	int m_TerrainHeight;
	int m_TileWidth;
	int m_TileHeight;
	int m_TileCount;
	// Quad ������ ������
	QuadTreeClass* m_QuadTree;
	std::vector<MaterialInfoType> m_Material;
	ShaderManagerClass* p_ShaderManager;
	TextureManagerClass* p_TextureManager;
	LightClass* p_Light;

};


#endif
////////////////////////////////////////////////////////////////////////////////
// Filename: objectmanagerclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _OBJECTMANAGERCLASS_H_
#define _OBJECTMANAGERCLASS_H_


//////////////
// INCLUDES //
//////////////
#include "modelmanagerclass.h"
#include "texturemanagerclass.h"
#include <vector>
using namespace std;

////////////////////////////////////////////////////////////////////////////////
// Class name: PackObject
////////////////////////////////////////////////////////////////////////////////
class PackObject 
{
public:
	PackObject();					// �������������� ��������� -1
	D3DXVECTOR3 position;		    // ������� � ������� �����������
	// ���� �������� �� ���������� ��������� � ��������
	float angleX;					// ������� �� ��� X
	float angleY;					// ������� �� ��� Y
	float sizeModel;				// ������ ������ ������
	std::string key_Model;			// ����� ������ ������������
	std::string Name;				// �������� �������
	float valueHP;
	int key_Shader;					// ����� ������� ����� ��������

};

////////////////////////////////////////////////////////////////////////////////
// Class name: ObjectManagerClass
////////////////////////////////////////////////////////////////////////////////
class ObjectManagerClass{
public:

	bool AddObject(PackObject);
	bool LoadObjectData(ID3D10Device*, ModelManagerClass*, TextureManagerClass*);
	bool DeleteObject(ModelManagerClass*, TextureManagerClass*, int);
	void Shutdown(ModelManagerClass*, TextureManagerClass*);
	int GetIndexIntersection(D3DXVECTOR3,D3DXVECTOR3);
	int GetCountObject();
	std::string GetModel(int);
	D3DXVECTOR3 GetPosition(int);
	float GetAngleY(int);
	float GetAngleX(int);
	PackObject GetPackObject(std::string);
	PackObject GetPackObject(int);
	void SetPackObject(PackObject);
private:
	std::vector<PackObject> m_Object;	
	
};

#endif


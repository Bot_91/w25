// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
////////////////////////////////////////////////////////////////////////////////
// Filename: TerrainClass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "terrainclass.h"


QuadTreeClass::QuadTreeClass() {
	northWest = 0;
	northEast = 0;
	southWest = 0;
	southEast = 0;
	m_IndexQuad = -1;
}
QuadTreeClass::~QuadTreeClass() {
	if (northWest != 0) {
		delete northWest;
		northWest = 0;
	}
	if (northEast != 0) {
		delete northEast;
		northEast = 0;
	}	
	if (southWest != 0) {
		delete southWest;
		southWest = 0;
	}	
	if (southEast != 0) {
		delete southEast;
		southEast = 0;
	}
}
// ���������� ������� �����
bool QuadTreeClass::GetIndexVisibleTile(FrustumClass* FrustumView, std::vector<int> &result) {
	
	// ���������� ����� ������� �������� � �����
	if (FrustumView->CheckSphere(QuadPosition, QuadSize.x*0.7071069f)==true) {
		// Quad ����� ����� �������� �����
		if (m_IndexQuad != -1) {
			result.push_back(m_IndexQuad);
		}
		if (northWest != 0) {
			northWest->GetIndexVisibleTile(FrustumView, result);
		}
		if (northEast != 0) {
			northEast->GetIndexVisibleTile(FrustumView, result);
		}
		if (southWest != 0) {
			southWest->GetIndexVisibleTile(FrustumView, result);
		}
		if (southEast != 0) {
			southEast->GetIndexVisibleTile(FrustumView, result);
		}
		return true;
	}
	return false;
}
int QuadTreeClass::GetIndexTilePoint(D3DXVECTOR3 position) {
	int result = -1;
	return result;
}

// ������� ������
void QuadTreeClass::CreatTree(	std::vector<int> indexTile, D3DXVECTOR3 pos, 
								int width, int height,
								float widthTile, float heightTile) {
	// ��������� ������� ������ �� �������� �����
	std::vector<int> indexNW; // �-�
	std::vector<int> indexNE; // �-�
	std::vector<int> indexSW; // �-�
	std::vector<int> indexSE; // �-�
	QuadPosition = pos;
	QuadSize = D3DXVECTOR3((float)width*widthTile, 0.0f, (float)height*heightTile);
	// ������������ ����� ������
	if (indexTile.size() > 1) {
		for (int j = 0; j < height; j++) {
			for (int i = 0; i < width; i++) {
				// ���������� �����
				if (j >= (height / 2)) {
					// ���������� ������-�����				
					if (i < (width / 2)) {
						indexNW.push_back(indexTile[j*width + i]);
					// ���������� ������-������
					}else {
						indexNE.push_back(indexTile[j*width + i]);
					}
				// ���������� ��
				} else {
					// ���������� ���-�����
					if (i < (width / 2)) {
						indexSW.push_back(indexTile[j*width + i]);
					// ���������� ���-������
					}else {
						indexSE.push_back(indexTile[j*width + i]);
					}
				}
			}
		}
		// ������-�����
		if (indexNW.size() > 0) {
			northWest = new QuadTreeClass;
			D3DXVECTOR3 posNW;
			posNW = pos + D3DXVECTOR3(	-(float)width*widthTile / 4.0f, 0.0f, 
										(float)height*heightTile / 4.0f);
			northWest->CreatTree(indexNW, posNW, width / 2, height / 2, widthTile, heightTile);
		}
		// ������-������
		if (indexNE.size() > 0) {
			northEast = new QuadTreeClass;
			D3DXVECTOR3 posNE;
			posNE = pos + D3DXVECTOR3(	(float)width*widthTile / 4.0f, 0.0f, 
										(float)height*heightTile / 4.0f);
			northEast->CreatTree(indexNE, posNE, width / 2, height / 2, widthTile, heightTile);
		}
		// ���-�����
		if (indexSW.size() > 0) {
			southWest = new QuadTreeClass;
			D3DXVECTOR3 posSW;
			posSW = pos + D3DXVECTOR3(	-(float)width*widthTile / 4.0f, 0.0f, 
										-(float)height*heightTile / 4.0f);
			southWest->CreatTree(indexSW, posSW, width / 2, height / 2, widthTile, heightTile);
		}
		// ���-������
		if (indexSE.size() > 0) {
			southEast = new QuadTreeClass;
			D3DXVECTOR3 posSE;
			posSE = pos + D3DXVECTOR3(	(float)width*widthTile / 4.0f, 0.0f, 
										-(float)height*heightTile / 4.0f);
			southEast->CreatTree(indexSE, posSE, width / 2, height / 2, widthTile, heightTile);
		}
	} else {
		m_IndexQuad = indexTile[0];
	}

	
}




TerrainClass::TerrainClass() {
	m_Tile = 0;
	m_Round = 0;
	m_TileCount = 0;
	p_TextureManager = 0;
	p_ShaderManager = 0;
	p_Light = 0;
	m_QuadTree = 0;
	m_TerrainWidth = 0;
	m_TerrainHeight = 0;
	delayTime = 0;
	listEditTile.clear();
}

bool TerrainClass::Initialize(ID3D10Device* device, ShaderManagerClass* p_ShaderMng,
								TextureManagerClass* p_TextureMng,
								LightClass* light, std::string filename) {
	bool result;
	// ����������� ���������
	p_TextureManager = p_TextureMng;
	p_ShaderManager = p_ShaderMng;
	p_Light = light;
	// ��������� �������� �� �����
	result = LoadTerrainMap(filename);
	// ��������� ������ ��� ������, ������� ���������
	if (result == false) {
		// ��������� ��������
		MaterialInfoType material;
		material.pathDiffuse = "./data/grass.bmp";
		material.pathNormal = "./data/normal.dds";
		m_Material.push_back(material);
		material.pathDiffuse = "./data/dirt.dds";
		material.pathNormal = "./data/normal.dds";
		m_Material.push_back(material);
		// ������� ���������
		result = CreateTerrainMap(device, 8, 8);
	}
	// ��������  ��������/�������� ��������
	if (result == true) {
		for (int i = 0; i < m_TileCount; i++) {
			m_Tile[i].Initialize(device);
		}
		// ��������� �������� ��������� � �������
		for (unsigned int i = 0; i < m_Material.size(); i++) {
			p_TextureManager->Add(device, m_Material[i].pathDiffuse);
			p_TextureManager->Add(device, m_Material[i].pathNormal);
		}
	}
	// ������� ��������� ������
	m_QuadTree = new QuadTreeClass;
	std::vector<int> indexTile;
	for (int j = 0; j < m_TerrainHeight; j++) {
		for (int i = 0; i < m_TerrainWidth; i++) {
			indexTile.push_back(j * m_TerrainWidth + i);
		}
	}
	D3DXVECTOR3 pos((float)(m_TerrainWidth*(m_Tile[0].m_Width-1)) / 2.0f, 0, 
					(float)(m_TerrainHeight*(m_Tile[0].m_Height-1)) / 2.0f);
	m_QuadTree->CreatTree(indexTile, pos, m_TerrainWidth, m_TerrainHeight, 
							(float)m_Tile[0].m_Width-1, (float)m_Tile[0].m_Height-1);

	return result;
}
bool TerrainClass::CreateTerrainMap(ID3D10Device* device, int widthTerrain, int heightTerrain) {
	// ������� ������ ��������
	Shutdown();
	// ��������� ���������� ������ � ���������
	m_TerrainWidth = widthTerrain;
	m_TerrainHeight = heightTerrain;
	m_TileCount = m_TerrainWidth * m_TerrainHeight;

	// �������� ������ ��� ������ ����
	m_Tile = new TerrainTileClass[m_TileCount];
	if (m_Tile == 0) {
		return false;
	}
	// ��������� �� ���� ������
	for (int j = 0; j < m_TerrainHeight; j++) {
		for (int i = 0; i < m_TerrainWidth; i++) {
			// ������� ������� ����
			// ������ �� ��� ������ ��� ����� (��������)
			m_TileWidth = 32;
			m_TileHeight = 32;
			m_Tile[j * m_TerrainWidth + i].CreatePlane((float)i*(m_TileWidth - 1),
														(float)j*(m_TileHeight - 1),
														m_TileWidth, m_TileHeight);
		}
	}

	for (int j = 0; j < m_TerrainHeight*(m_TileHeight-1); j++) {
		for (int i = 0; i < m_TerrainWidth*(m_TileWidth-1); i++) {
			D3DXVECTOR3 point((float)i, 0.0f, (float)j);
			SetCamputeMeanNTB(point);
		}
	}
	

	return true;
}
// ������ ������ ����� ���������
void TerrainClass::fileReadString(std::ifstream& file, std::string& text) {
	unsigned int length;
	// ������ ���������� �������� � ������
	file.read((char*)&length, sizeof(unsigned int));
	char* line = new char[length];
	// ������ ������
	file.read(line, length);
	text = line;
	// ������ ���������� ����� �� �������
	text.resize(length);
	delete[] line;
}
// ������ ������, ����� ���������
void TerrainClass::fileWriteString(std::ofstream& file, std::string text) {
	// �������� ������ ��� ������
	// ������ ���������� �������� � ������
	unsigned int count = text.length();
	// ������ ���������� ��������

	file.write((char*)&count, sizeof(unsigned int));
	// ������ ������
	file.write(text.c_str(), count);
}
// ��������� ���� ��������
bool TerrainClass::SaveTerrainMap(std::string filename){
	// ��������� �� ����� ������ � ����
	std::ofstream fileSaveTerrain;
	fileSaveTerrain.open(filename, std::ios::binary);
	// ���������, ��� ���� ������� �������
	if (fileSaveTerrain.fail()) {
		return false;
	}
	// ���������� ���������� ������� 
	int countTexture = (int)m_Material.size();
	fileSaveTerrain.write((char*)&countTexture, sizeof(int));

	std::string pathTexture;
	// ���������� ���� �� �������� (����) 
	for (int i = 0; i < countTexture; i++) {
		// �������� ��������
		fileWriteString(fileSaveTerrain, m_Material[i].pathDiffuse);
		// ���������� ��������
		fileWriteString(fileSaveTerrain, m_Material[i].pathNormal);
	}
	// ���������� ���������� ������ � ���������
	fileSaveTerrain.write((char*)&m_TerrainWidth, sizeof(LONG));
	fileSaveTerrain.write((char*)&m_TerrainHeight, sizeof(LONG));

	// ���������� ��� �����
	for (int i = 0; i < m_TileCount; i++) {
		m_Tile[i].SaveTerrainMap(fileSaveTerrain);
	}
	fileSaveTerrain.close();
	return false;
}

// ��������� �������
bool TerrainClass::LoadTerrainMap(std::string filename) {
	// ������� ������ ��������
	Shutdown();
	// ��������� �� ����� ���������� �����
	std::ifstream fileOpenTerrain;
	fileOpenTerrain.open(filename, std::ios::binary);
	// ���������, ��� ���� ������� �������
	if (fileOpenTerrain.fail()) {
		return false;
	}
	// ���������� ���� �� �������� ����� 
	// ���� ���� '/' � ����� ������
	for (int i = filename.length() - 1; i>0; i--) {
		// �����
		if (filename[i] == '/') {
			// �������� ������ ���� �� �������� ����� ������ �� ������ '/'
			pathMap = std::string(filename, 0, i + 1);
			break;
		}
	}
	// ��������� ���������� ������� 
	int countTexture;
	fileOpenTerrain.read((char*)&countTexture, sizeof(int));

	std::string pathTexture;
	// ��������� ���� �� �������� (����) 
	for (int i = 0; i < countTexture; i++) {
		MaterialInfoType material;
		// �������� ��������
		fileReadString(fileOpenTerrain, pathTexture);
		material.pathDiffuse = pathTexture;
		// ���������� ��������
		fileReadString(fileOpenTerrain, pathTexture);
		material.pathNormal = pathTexture;
		// ��������� ��������
		m_Material.push_back(material);
	}

	// ��������� ���������� ������ � ���������
	fileOpenTerrain.read((char*)&m_TerrainWidth, sizeof(LONG));
	fileOpenTerrain.read((char*)&m_TerrainHeight, sizeof(LONG));	
	m_TileCount = m_TerrainWidth * m_TerrainHeight;
	// �������� ������ ��� ������ ����
	m_Tile = new TerrainTileClass[m_TileCount];
	if (m_Tile == 0) {
		return false;
	}

	// ��������� ��� �����
	for (int i = 0; i < m_TileCount; i++) {
		// ��������� ��������
		m_Tile[i].LoadTerrainMap(fileOpenTerrain);
		// ���������� ������
		m_TileWidth = m_Tile[i].m_Width;
		m_TileHeight = m_Tile[i].m_Height;
	}
	
	fileOpenTerrain.close();
	return true;
}

void TerrainClass::Render(	ID3D10Device* device, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix,
							D3DXMATRIX projectionMatrix, FrustumClass* p_Frustum) {
	delayTime = timeGetTime();
	std::vector<int> visibleTile;
	m_QuadTree->GetIndexVisibleTile(p_Frustum, visibleTile);
	for (unsigned int i = 0; i< visibleTile.size(); i++) {
		int index = visibleTile[i];
		m_Tile[index].Render(device);
		std::vector<MaterialInfoType> vecMaterial;
		for (int j = 0; j < m_Tile[index].GetCountMaterial(); j++) {
			vecMaterial.push_back(m_Material[m_Tile[index].GetMaterial(j)]);
		}
		p_ShaderManager->RenderTerrainShader(	device, m_Tile[index].GetIndexCount(),
												worldMatrix, viewMatrix, projectionMatrix,
												vecMaterial, p_Light->GetDirection(), 
												p_Light->GetDiffuseColor(), 32.0f);
	}
	if (m_Round) {
		if (m_Round->flagCreat == true){
			m_Round->Render(device);
			p_ShaderManager->RenderTextureShader(	device, m_Round->m_IndexCount,
													worldMatrix, viewMatrix, projectionMatrix,
													p_TextureManager->GetTexture("./data/RED.bmp"));	
		}
	}

	delayTime = timeGetTime() - delayTime;

}
void TerrainClass::SetStateRender(int state) {
	for (int i = 0; i < m_TileCount; i++) {
		m_Tile[i].SetStateRender(state);
	}
}
bool TerrainClass::GetHeight(D3DXVECTOR3 point, float& height) {
	// ���������� ������ ����� 
	int indexTile_X = (int)((point.x + int(point.x / m_TileWidth)) / m_TileWidth);
	int indexTile_Z = (int)((point.z + int(point.z / m_TileHeight)) / m_TileHeight);
	int indexTile = (indexTile_Z * m_TerrainWidth) + indexTile_X;
	// ����� ��������� �� �����
	if (indexTile_X >= 0 && indexTile_X < (m_TerrainWidth - 1) &&
		indexTile_Z >= 0 && indexTile_Z < (m_TerrainHeight - 1)){
		// ���������� ������ �������
		int indexVertex_X = (int)point.x - indexTile_X * (m_TileWidth - 1);
		int indexVertex_Z = (int)point.z - indexTile_Z * (m_TileHeight - 1);
		// ����� �������
		int indexVertex = (indexVertex_Z * m_TileWidth) + indexVertex_X;
		if (indexVertex_X != (m_TileWidth - 1) && 
			indexVertex_Z != (m_TileHeight - 1)) {
			if (m_Tile[indexTile].GetHeight(point, height) == true) {
				return true;
			}
		}
		// ������� �����������
		if (indexVertex_X == (m_TileWidth - 1) &&
			indexVertex_Z != (m_TileHeight - 1)) {
			indexTile = (indexTile_Z * m_TerrainWidth) + indexTile_X + 1;
			if (m_Tile[indexTile].GetHeight(point, height) == true) {
				return true;
			}
		}
		// ������� �� ���������
		if (indexVertex_X != (m_TileWidth - 1) &&
			indexVertex_Z == (m_TileHeight - 1)) {
			indexTile = ((indexTile_Z + 1) * m_TerrainWidth) + indexTile_X;
			if (m_Tile[indexTile].GetHeight(point, height) == true) {
				return true;
			}
		}
		// ������� �� ���������
		if (indexVertex_X == (m_TileWidth - 1) &&
			indexVertex_Z == (m_TileHeight - 1)) {
			indexTile = ((indexTile_Z + 1) * m_TerrainWidth) + indexTile_X + 1;
			if (m_Tile[indexTile].GetHeight(point, height) == true) {
				return true;
			}
		}
	}
	return false;
}
bool TerrainClass::GetHeight(D3DXVECTOR3 point, float &height, D3DXVECTOR3 &normal) {
	// ���������� ������ ����� 
	int indexTile_X = (int)((point.x + int(point.x / m_TileWidth)) / m_TileWidth);
	int indexTile_Z = (int)((point.z + int(point.z / m_TileHeight)) / m_TileHeight);
	int indexTile = (indexTile_Z * m_TerrainWidth) + indexTile_X;
	// ����� ��������� �� �����
	if (indexTile_X >= 0 && indexTile_X < (m_TerrainWidth - 1) &&
		indexTile_Z >= 0 && indexTile_Z < (m_TerrainHeight - 1)) {
		// ���������� ������ �������
		int indexVertex_X = (int)point.x - indexTile_X * (m_TileWidth - 1);
		int indexVertex_Z = (int)point.z - indexTile_Z * (m_TileHeight - 1);
		// ����� �������
		int indexVertex = (indexVertex_Z * m_TileWidth) + indexVertex_X;
		if (indexVertex_X != (m_TileWidth - 1) &&
			indexVertex_Z != (m_TileHeight - 1)) {
			if (m_Tile[indexTile].GetHeight(point, height, normal) == true) {
				return true;
			}
		}
		// ������� �����������
		if (indexVertex_X == (m_TileWidth - 1) &&
			indexVertex_Z != (m_TileHeight - 1)) {
			indexTile = (indexTile_Z * m_TerrainWidth) + indexTile_X + 1;
			if (m_Tile[indexTile].GetHeight(point, height, normal) == true) {
				return true;
			}
		}
		// ������� �� ���������
		if (indexVertex_X != (m_TileWidth - 1) &&
			indexVertex_Z == (m_TileHeight - 1)) {
			indexTile = ((indexTile_Z + 1) * m_TerrainWidth) + indexTile_X;
			if (m_Tile[indexTile].GetHeight(point, height, normal) == true) {
				return true;
			}
		}
		// ������� �� ���������
		if (indexVertex_X == (m_TileWidth - 1) &&
			indexVertex_Z == (m_TileHeight - 1)) {
			indexTile = ((indexTile_Z + 1) * m_TerrainWidth) + indexTile_X + 1;
			if (m_Tile[indexTile].GetHeight(point, height, normal) == true) {
				return true;
			}
		}
	}
	return false;

}
// ���������� ������� �� ����� �����
TerrainTileClass::VertexType TerrainClass::GetVertex(D3DXVECTOR3 point) {
	// ���������� ������ ����� 
	int indexTile_X = (int)((point.x + int(point.x / m_TileWidth)) / m_TileWidth);
	int indexTile_Z = (int)((point.z + int(point.z / m_TileHeight)) / m_TileHeight);
	int indexTile = (indexTile_Z * m_TerrainWidth) + indexTile_X;
	if (indexTile >= 0 && indexTile < m_TileCount) {
		int indexVertex_X = (int)point.x - indexTile_X * (m_TileWidth - 1);
		int indexVertex_Z = (int)point.z - indexTile_Z * (m_TileHeight - 1);
		int indexVertex = (indexVertex_Z * m_TileWidth) + indexVertex_X;
		return m_Tile[indexTile].GetVertex(point);
	}
	return TerrainTileClass::VertexType();
}
bool TerrainClass::SetCamputeMeanNTB(D3DXVECTOR3 point) {

	int indexTile_X = (int)((point.x + int(point.x / m_TileWidth)) / m_TileWidth);
	int indexTile_Z = (int)((point.z + int(point.z / m_TileHeight)) / m_TileHeight);
	int indexTile = (indexTile_Z * m_TerrainWidth) + indexTile_X;
	int indexVertex_X = (int)point.x - indexTile_X * (m_TileWidth - 1);
	int indexVertex_Z = (int)point.z - indexTile_Z * (m_TileHeight - 1);
	int indexVertex = (indexVertex_Z * m_TileWidth) + indexVertex_X;
	// ����� ��������� �� �����
	if (indexTile_X > 0 && indexTile_X < (m_TerrainWidth - 1) &&
		indexTile_Z > 0 && indexTile_Z < (m_TerrainHeight - 1)) {
		// ������ ����
		//		1
		//	4	0	2 
		//		3
		TerrainTileClass::VertexType vertex[5];
		// ������� �������� ������� � � �������
		vertex[0] = GetVertex(point);
		vertex[1] = GetVertex(point + D3DXVECTOR3(0.0f, 0.0f, -1.0f));
		vertex[2] = GetVertex(point + D3DXVECTOR3(-1.0f, 0.0f, 0.0f));
		vertex[3] = GetVertex(point + D3DXVECTOR3(0.0f, 0.0f, 1.0f));
		vertex[4] = GetVertex(point + D3DXVECTOR3(1.0f, 0.0f, 0.0f));

		m_Tile[indexTile].SetCamputeNodeMeanNormalTangentBinormal(vertex, indexVertex);
		// �������� ������� ��������� ����� �� �����������
		if (indexVertex_X == (m_TileWidth - 1)) {
			indexTile = (indexTile_Z * m_TerrainWidth) + indexTile_X + 1;
			indexVertex = (indexVertex_Z * m_TileWidth) + 0;
			m_Tile[indexTile].SetCamputeNodeMeanNormalTangentBinormal(vertex, indexVertex);
			listEditTile.push_back(indexTile);
		}
		// �������� ������� ��������� ����� �� ���������
		if (indexVertex_Z == (m_TileHeight - 1)) {
			indexTile = ((indexTile_Z + 1) * m_TerrainWidth) + indexTile_X;
			indexVertex = indexVertex_X;
			m_Tile[indexTile].SetCamputeNodeMeanNormalTangentBinormal(vertex, indexVertex);
			listEditTile.push_back(indexTile);
		}
		// �������� ������� ��������� ����� �� ���������
		if (indexVertex_Z == (m_TileHeight - 1) && 
			indexVertex_X == (m_TileWidth - 1)){
			indexTile = ((indexTile_Z + 1) * m_TerrainWidth) + indexTile_X + 1;
			indexVertex = 0;
			m_Tile[indexTile].SetCamputeNodeMeanNormalTangentBinormal(vertex, indexVertex);
			listEditTile.push_back(indexTile);
		}
		return true;
	}
	return false;
}


// ���������� ����� ������� ����������� �����
bool TerrainClass::AlterPoint(D3DXVECTOR3 point, float value, alter_argument argument) {

	// ���������� ������ ����� 
	int indexTile_X = (int)((point.x + int(point.x / m_TileWidth)) / m_TileWidth);
	int indexTile_Z = (int)((point.z + int(point.z / m_TileHeight)) / m_TileHeight);
	int indexTile = (indexTile_Z * m_TerrainWidth) + indexTile_X;
	// ����� ��������� �� �����
	if (indexTile_X > 0 && indexTile_X < (m_TerrainWidth - 1) &&
		indexTile_Z > 0 && indexTile_Z < (m_TerrainHeight - 1)) {
		// ���������� ������ �������
		int indexVertex_X = (int)point.x - indexTile_X * (m_TileWidth - 1);
		int indexVertex_Z = (int)point.z - indexTile_Z * (m_TileHeight - 1);
		int indexVertex = (indexVertex_Z * m_TileWidth) + indexVertex_X;
		// �������� ������ � ������� �����
		m_Tile[indexTile].AlterPoint(indexVertex, value, argument);
		listEditTile.push_back(indexTile);
		// �������� ������� ��������� ����� �� �����������
		if (indexVertex_X == (m_TileWidth - 1)) {
			indexTile = (indexTile_Z * m_TerrainWidth) + indexTile_X + 1;
			indexVertex = (indexVertex_Z * m_TileWidth) + 0;
			m_Tile[indexTile].AlterPoint(indexVertex, value, argument);
			listEditTile.push_back(indexTile);
		}
		// �������� ������� ��������� ����� �� ���������
		if (indexVertex_Z == (m_TileHeight - 1)) {
			indexTile = ((indexTile_Z + 1) * m_TerrainWidth) + indexTile_X;
			indexVertex = indexVertex_X;
			m_Tile[indexTile].AlterPoint(indexVertex, value, argument);
			listEditTile.push_back(indexTile);
		}
		// �������� ������� ��������� ����� �� ���������
		if (indexVertex_Z == (m_TileHeight - 1) && 
			indexVertex_X == (m_TileWidth - 1)) {
			indexTile = ((indexTile_Z + 1) * m_TerrainWidth) + indexTile_X + 1;
			indexVertex = 0;
			m_Tile[indexTile].AlterPoint(indexVertex, value, argument);
			listEditTile.push_back(indexTile);
		}
		return true;
	}
	return false;
}
// �������� ������ ����� ������� � �������
void TerrainClass::AlterArea(ID3D10Device* device, D3DXVECTOR3 position, 
							 float value, int radius, alter_argument argument) {
	// ���������� ������ �����
	D3DXVECTOR3 point;
	listEditTile.clear();
	position = D3DXVECTOR3((float)(int(position.x)), (float)(int(position.y)), (float)(int(position.z)));
	// ��������� �� ��������
	for (int j = 0; j < 2 * radius + 1; j++) {
		for (int i = 0; i < 2 * radius + 1; i++) {
			point = position + D3DXVECTOR3((float)(i - radius), 0.0f, (float)(j - radius));
			// ����������
			if ((int)(pow(j - radius, 2) + pow(i - radius, 2)) < radius*radius) {
				// ���������� ����
				AlterPoint(point, value, argument);
			}
		}
	}
	// �������� �������� ���������� ������
	for (int j = 0; j < 2 * (radius + 1) + 1; j++) {
		for (int i = 0; i < 2 * (radius + 1) + 1; i++) {
			point = position + D3DXVECTOR3((float)(i - radius), 0.0f, (float)(j - radius));
			SetCamputeMeanNTB(point);
		}
	}
	// ������� ������������� ��������
	listEditTile.unique();
	// ��������� �� ���� ����������� ������
	for (std::list<int>::iterator it = listEditTile.begin(); it != listEditTile.end(); ++it) {
		// ������� ����� ��������� ����� (��������� ����� ���� �� ��������)
		m_Tile[(*it)].Initialize(device);
	}

}
bool TerrainClass::CreateRound(ID3D10Device* device, D3DXVECTOR3 position, float radius) {
	
	if (m_Round) {
		m_Round->Shutdown();
		delete m_Round;
		m_Round = 0;
	}
	m_Round = new TerrainRoundClass();
	std::vector<D3DXVECTOR3> vertex;
	// ���������� ���������� �� ��������
	vertex.resize(120);
	float width = 0.5f;
	float step = 360.0f / (float)vertex.size();
	for (unsigned int i = 0; i < vertex.size(); i += 2) {
		vertex[i].x = position.x + (float)radius*cos(step*(float)i*0.0174532925f);
		vertex[i].z = position.z + (float)radius*sin(step*(float)i*0.0174532925f);
		vertex[i].y = 0;
		// ���������� ������ � �����
		GetHeight(vertex[i], vertex[i].y);
		vertex[i].y += 0.50f;

		vertex[i + 1].x = position.x + (float)(radius + width)*cos(step*(float)i*0.0174532925f);
		vertex[i + 1].z = position.z + (float)(radius + width)*sin(step*(float)i*0.0174532925f);
		vertex[i + 1].y = vertex[i].y;

	}
	m_Round->Initialize(device, vertex);
	return true;
}
bool TerrainClass::GetIntersects(D3DXVECTOR3 position, D3DXVECTOR3 view, D3DXVECTOR3& point) {
	// ��� �������� ������ ���������
	D3DXVECTOR3 rayStep = view*0.5f;
	D3DXVECTOR3 rayStartPosition = position;
	// ����� ��� ��������� ������
	D3DXVECTOR3 startPosition(0.0f, 0.0f, 0.0f);
	D3DXVECTOR3 endPosition(0.0f, 0.0f, 0.0f);
	float height = 0.0f;
	bool flag = true;
	int iteration = 0;
	// ���� ���� �� ������ ��� �������� ��� �� ���������� :)
	while ((position.y + view.y) > height && iteration < 1024) {
		startPosition = view;
		view = view + rayStep;
		float h;
		flag = GetHeight(position + view, h);
		// ���������, ��� ������� ���������� ������
		if (flag == true) {
			height = h;
		}
		iteration++;
	}
	// ���� ��� ������������ � ����������
	if (flag == true) {
		startPosition = startPosition + position;
		endPosition = view + position;

		// �������� �����. ���� ������ ����� ����������� 
		for (int i = 0; i < 32; i++) {
			// ������ ��������� ������
			D3DXVECTOR3 middlePoint = (startPosition + endPosition) * 0.5f;

			if (middlePoint.y < height)
				endPosition = middlePoint;
			else
				startPosition = middlePoint;
		}

		point = (startPosition + endPosition) * 0.5f;
		return true;
	}
	return false;
}
void TerrainClass::Shutdown(void) {
	// ����������� ��������� �������
	for (int i = 0; i < m_TileCount; i++) {
		if (m_Tile != 0) {
			m_Tile[i].Shutdown();
		}
	}
	m_TileCount = 0;
	if (m_Tile != 0) {
		delete []m_Tile;
		m_Tile = 0;
	}		
	if (m_QuadTree) {
		delete m_QuadTree;
		m_QuadTree = 0;
	}
	if (p_TextureManager != 0) {
		for (unsigned int i = 0; i < m_Material.size(); i++) {
			p_TextureManager->Erase(m_Material[i].pathDiffuse);
			p_TextureManager->Erase(m_Material[i].pathNormal);
		}
	}
	if (m_Round) {
		m_Round->Shutdown();
		delete m_Round;
		m_Round = 0;
	}


}

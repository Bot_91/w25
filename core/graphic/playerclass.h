////////////////////////////////////////////////////////////////////////////////
// Filename: objclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _PLAYERCLASS_H_
#define _PLAYERCLASS_H_


//#define MINF 1.7e-300
//#define INF 1.7e+300
//////////////
// INCLUDES //
//////////////
// ��������


// �������� � ���������
#include <D3DX10math.h>
#include <string>
#include <cmath>
#include "cameraclass.h"
//���������� �������. ������ � OBJ
class PlayerClass {
public:
	std::string keyModel;
	D3DXVECTOR3 position;
	D3DXVECTOR3 rotation;
	D3DXVECTOR3 markerMove;
	bool flagRun;
	float speed;
	CameraClass *p_Camera;
	bool MoveTo(float deltaTime);
	bool MoveStep(D3DXVECTOR3);
	void SetMoveTo(D3DXVECTOR3 marker);
	PlayerClass();
	void Initialize(CameraClass *camera, std::string pathModel);
	//~PlayerClass();
	D3DXVECTOR3 GetPosition();
	D3DXVECTOR3 GetRotation();
	bool GetDirection(D3DXVECTOR3&);
private:
};


#endif
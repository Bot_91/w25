// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
////////////////////////////////////////////////////////////////////////////////
// Filename: textureclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "textureclass.h"


TextureClass::TextureClass(){
	m_Texture = 0;
}


TextureClass::TextureClass(const TextureClass& other){
	m_Texture = 0;
}


TextureClass::~TextureClass(){
}


bool TextureClass::Initialize(ID3D10Device* device, std::string path){
	HRESULT result;
	// Load the texture in.
	// ���������� ���� std::string � LPCWSTR
	std::wstring stemp = std::wstring(path.begin(), path.end());
	LPCWSTR filename = stemp.c_str();
	result = D3DX10CreateShaderResourceViewFromFile(device, filename, NULL, NULL, &m_Texture, NULL);
	if(FAILED(result)){
		return false;
	}

	return true;
}




void TextureClass::Shutdown(){
	// Release the texture resource.
	if(m_Texture){
		m_Texture->Release();
		m_Texture = 0;
	}

	return;
}


ID3D10ShaderResourceView* TextureClass::GetTexture(){
	return m_Texture;
}
void TextureClass::SetTexture(ID3D10ShaderResourceView* texture) {
	m_Texture = texture;
}
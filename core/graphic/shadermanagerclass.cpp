// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
////////////////////////////////////////////////////////////////////////////////
// Filename: shadermanagerclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "shadermanagerclass.h"


ShaderManagerClass::ShaderManagerClass()
{
	m_TextureShader = 0;
	m_FontShader = 0;
	m_LightShader = 0;
	m_BumpMapShader = 0;
	m_MultiTextureShader = 0;
	m_TerrainShader = 0;

	p_TextureManager = 0;
}


ShaderManagerClass::ShaderManagerClass(const ShaderManagerClass& other)
{
}


ShaderManagerClass::~ShaderManagerClass()
{
}


bool ShaderManagerClass::Initialize(ID3D10Device* device, HWND hwnd, 
									TextureManagerClass* textureManager){
	bool result;

	// ����������� ���������� � ��������� ��������
	this->p_TextureManager = textureManager;
	// Create the texture shader object.
	m_TextureShader = new TextureShaderClass;
	if(!m_TextureShader){
		return false;
	}
	// Initialize the texture shader object.
	result = m_TextureShader->Initialize(device, hwnd);
	if(!result){
		MessageBox(hwnd, L"Could not initialize the texture shader object.", L"Error", MB_OK);
		return false;
	}

	// Create the font shader object.
	m_FontShader = new FontShaderClass;
	if(!m_FontShader){
		return false;
	}
	// Initialize the font shader object.
	result = m_FontShader->Initialize(device, hwnd);
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize the font shader object.", L"Error", MB_OK);
		return false;
	}

	// Create the light shader object.
	m_LightShader = new LightShaderClass;
	if(!m_LightShader)
	{
		return false;
	}
	// Initialize the light shader object.
	result = m_LightShader->Initialize(device, hwnd);
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize the light shader object.", L"Error", MB_OK);
		return false;
	}

	// Create the bump map shader object.
	m_BumpMapShader = new BumpMapShaderClass;
	if(!m_BumpMapShader)
	{
		return false;
	}
	// Initialize the bump map shader object.
	result = m_BumpMapShader->Initialize(device, hwnd);
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize the bump map shader object.", L"Error", MB_OK);
		return false;
	}

	// Create the multi texture shader object.
	m_MultiTextureShader = new MultiTextureShaderClass;
	if(!m_MultiTextureShader)
	{
		return false;
	}
	// Initialize the multi texture shader object.
	result = m_MultiTextureShader->Initialize(device, hwnd);
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize the multi texture shader object.", L"Error", MB_OK);
		return false;
	}

	// Create the terrain shader object.
	m_TerrainShader = new TerrainShaderClass;
	if(!m_TerrainShader)
	{
		return false;
	}
	// Initialize the terrain shader object.
	result = m_TerrainShader->Initialize(device, hwnd);
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize the terrain shader object.", L"Error", MB_OK);
		return false;
	}



	return true;
}


void ShaderManagerClass::Shutdown()
{
	// Release the multi texture shader object.
	if(m_MultiTextureShader)
	{
		m_MultiTextureShader->Shutdown();
		delete m_MultiTextureShader;
		m_MultiTextureShader = 0;
	}
	// Release the bump map shader object.
	if(m_BumpMapShader)
	{
		m_BumpMapShader->Shutdown();
		delete m_BumpMapShader;
		m_BumpMapShader = 0;
	}

	// Release the light shader object.
	if(m_LightShader)
	{
		m_LightShader->Shutdown();
		delete m_LightShader;
		m_LightShader = 0;
	}

	// Release the texture shader object.
	if(m_TextureShader)
	{
		m_TextureShader->Shutdown();
		delete m_TextureShader;
		m_TextureShader = 0;
	}
	
	// Release the font shader object.
	if(m_FontShader)
	{
		m_FontShader->Shutdown();
		delete m_FontShader;
		m_FontShader = 0;
	}
	// Release the terrain shader object.
	if(m_TerrainShader)
	{
		m_TerrainShader->Shutdown();
		delete m_TerrainShader;
		m_TerrainShader = 0;
	}

	return;
}

void ShaderManagerClass::RenderTextureShader(	ID3D10Device* device, int indexCount, D3DXMATRIX worldMatrix,
												D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix, 
												ID3D10ShaderResourceView* texture)
{
	// Render the model using the texture shader.
	m_TextureShader->Render(device, indexCount, worldMatrix, viewMatrix, projectionMatrix, texture);

	return;
}

void ShaderManagerClass::RenderFontShader(	ID3D10Device* device, int indexCount, D3DXMATRIX worldMatrix,
											D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix, 
											ID3D10ShaderResourceView* texture, D3DXVECTOR4 pixelColor)
{
	// Render the model using the font shader.
	m_FontShader->Render(device, indexCount, worldMatrix, viewMatrix, projectionMatrix, texture, pixelColor);

	return;
}


void ShaderManagerClass::RenderLightShader(ID3D10Device* device, int indexCount, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix, 
										   ID3D10ShaderResourceView* texture, D3DXVECTOR3 lightDirection, D3DXVECTOR4 ambient, D3DXVECTOR4 diffuse, 
										   D3DXVECTOR3 cameraPosition, D3DXVECTOR4 specular, float specularPower)
{
	// Render the model using the light shader.
	m_LightShader->Render(device, indexCount, worldMatrix, viewMatrix, projectionMatrix, texture, lightDirection, ambient, diffuse, cameraPosition, specular, 
						  specularPower);

	return;
}


void ShaderManagerClass::RenderBumpMapShader(ID3D10Device* device, int indexCount, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix, 
											 ID3D10ShaderResourceView* colorTexture, ID3D10ShaderResourceView* normalTexture, D3DXVECTOR3 lightDirection, 
											 D3DXVECTOR4 diffuse)
{
	// Render the model using the bump map shader.
	m_BumpMapShader->Render(device, indexCount, worldMatrix, viewMatrix, projectionMatrix, colorTexture, normalTexture, lightDirection, diffuse);

	return;
}

void ShaderManagerClass::RenderMultiTextureShader(ID3D10Device* device, int indexCount, D3DXMATRIX worldMatrix, 
												  D3DXMATRIX viewMatrix,D3DXMATRIX projectionMatrix, 
												  ID3D10ShaderResourceView* texture_1, ID3D10ShaderResourceView* texture_2)
									  
{
	// Render the model using the multi texture shader.
	ID3D10ShaderResourceView* textureArray[2];
	textureArray[0] = texture_1;
	textureArray[1] = texture_2;
	m_MultiTextureShader->Render(device, indexCount, worldMatrix, viewMatrix, projectionMatrix, textureArray);
	return;
}

void ShaderManagerClass::RenderTerrainShader(ID3D10Device* device, int indexCount, D3DXMATRIX worldMatrix, 
											D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix,
											std::vector<MaterialInfoType> vecMaterial, D3DXVECTOR3 lightDirection,
											D3DXVECTOR4 lightDiffuseColor, float colorTextureBrightness)
{
	// ������ ������ ��������� ������ ��������.
	MaterialInfoType shaderMaterial[3];
	// ��������� ������, ������ ������������� ����������
	// ��� ������� �������� ���������� �������� � �������
	for (unsigned int i = 0; i < vecMaterial.size(); i++){
		if (i < 3) {
			shaderMaterial[i] = vecMaterial[i];
		}
	}
	// ��� ����� �������� ���������
	ID3D10ShaderResourceView* colorTexture[3];
	ID3D10ShaderResourceView* normalTexture[3];
	colorTexture[0] = p_TextureManager->GetTexture(shaderMaterial[0].pathDiffuse);
	colorTexture[1] = p_TextureManager->GetTexture(shaderMaterial[1].pathDiffuse);
	colorTexture[2] = p_TextureManager->GetTexture(shaderMaterial[2].pathDiffuse);
	normalTexture[0] = p_TextureManager->GetTexture(shaderMaterial[0].pathNormal);
	normalTexture[1] = p_TextureManager->GetTexture(shaderMaterial[1].pathNormal);
	normalTexture[2] = p_TextureManager->GetTexture(shaderMaterial[2].pathNormal);

	m_TerrainShader->Render(device, indexCount, worldMatrix, viewMatrix, projectionMatrix, 
							colorTexture, normalTexture, lightDirection, 
							lightDiffuseColor, colorTextureBrightness);
	return;
}
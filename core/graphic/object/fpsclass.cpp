// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
///////////////////////////////////////////////////////////////////////////////
// Filename: fpsclass.cpp
///////////////////////////////////////////////////////////////////////////////
#include "fpsclass.h"


FpsClass::FpsClass(){
	m_fps = 0;
	m_count = 0;
	m_startTime = 0;
}


FpsClass::FpsClass(const FpsClass& other)
{
	m_fps = 0;
	m_count = 0;
	m_startTime = 0;
}


FpsClass::~FpsClass()
{
}


bool FpsClass::Initialize()
{
	m_fps = 0;
	m_count = 0;
	m_startTime = timeGetTime();
	return true;
}


void FpsClass::Frame()
{
	m_count++;

	if(timeGetTime() >= (m_startTime + 1000))
	{
		m_fps = m_count;
		m_count = 0;
		
		m_startTime = timeGetTime();
	}
}


int FpsClass::GetFps()
{
	return m_fps;
}

void FpsClass::Shutdown()
{
	return;
}
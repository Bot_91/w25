// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
////////////////////////////////////////////////////////////////////////////////
// Filename: tStaticModel.cpp
////////////////////////////////////////////////////////////////////////////////
#include "staticmodel3dclass.h"

Detail3DClass::Detail3DClass(){
	m_VertexBuffer = 0;
	m_IndexBuffer = 0;
	m_Vertex = 0;
	m_Index = 0;
	countOfVertex = 0;	
	countOfIndex = 0;	
	materialID = -1;
	radius = 1.0f;
	name = "NULL";
}

Detail3DClass::~Detail3DClass(){
	m_Vertex = 0;
	m_Index = 0;
	countOfVertex = 0;	
	countOfIndex = 0;	
	name = "NULL";
// ������� ��������� ����� �����
}

void Detail3DClass::Shutdown(){
	ShutdownBuffers();
	ReleaseDetail();
	return;
}

void Detail3DClass::ShutdownBuffers()
{
	// ����������� ��������� �����.
	if(m_IndexBuffer){
		m_IndexBuffer->Release();
		m_IndexBuffer = 0;
	}
	// ����������� ��������� �����.
	if(m_VertexBuffer){
		m_VertexBuffer->Release();
		m_VertexBuffer = 0;
	}
	return;
}
void Detail3DClass::ReleaseDetail(){
	// ���������� ������
	if (m_Vertex){
		delete [] m_Vertex;
		m_Vertex = 0;
		countOfVertex = 0;	
	}
	if (m_Index){
		delete [] m_Index;
		m_Index = 0;
		countOfIndex = 0;	
	}
	return;
}

bool Detail3DClass::Initialize(ID3D10Device* device, const std::vector<VertexType> vecVertex, 
													 const std::vector<int> vecIndex){
	bool result;
	// ���������� ���������� ������
	countOfVertex = (int)vecVertex.size();
	m_Vertex = new VertexType [countOfVertex];	
	for (int i=0; i<countOfVertex; i++){
		m_Vertex[i] = vecVertex[i];
	}
	// ���������� ���������� ��������
	countOfIndex =  (int)vecIndex.size();
	m_Index = new unsigned long [countOfIndex];	
	for (int i=0; i<countOfIndex; i++){
		m_Index[i] = vecIndex[i];
	}
	// �������������� ��������� � ��������� �����
	result = InitializeBuffers(device);
	if(!result){
		return false;
	}
	return true;
}

bool Detail3DClass::Render(ID3D10Device* device){
	// �������� ��������� � ��������� ����� � ����������� �������
	bool result = RenderBuffers(device);
	return result;
}

int Detail3DClass::GetIndexCount(){
	return countOfIndex;
}
int Detail3DClass::GetVertexCount(){
	return countOfVertex;
}

bool Detail3DClass::InitializeBuffers(ID3D10Device* device)
{
	D3D10_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
    D3D10_SUBRESOURCE_DATA vertexData, indexData;
	HRESULT result;
	// ������� ���� ��� ��� ������� ����, �������� ���� ��� �� ��� ������ ��������� �������,
	//	�������� ������ �������, ��������� ��� ��� ������� ������� ���������, 
	//	������� ��� � ��������, ��� ���� ����, ��� �������, �� ���� ������.
	// Load the vertex array and index array with data.
	// ������� ������ ���������
	
	// Set up the description of the vertex buffer.
    vertexBufferDesc.Usage = D3D10_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(VertexType) * countOfVertex;
    vertexBufferDesc.BindFlags = D3D10_BIND_VERTEX_BUFFER;
    vertexBufferDesc.CPUAccessFlags = 0;
    vertexBufferDesc.MiscFlags = 0;

	// Give the subresource structure a pointer to the vertex data.
	vertexData.pSysMem = m_Vertex;

	// Now finally create the vertex buffer.
    result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_VertexBuffer);
	if(FAILED(result)){
		return false;
	}
	
	// Set up the description of the index buffer.
    indexBufferDesc.Usage = D3D10_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * countOfIndex;
    indexBufferDesc.BindFlags = D3D10_BIND_INDEX_BUFFER;
    indexBufferDesc.CPUAccessFlags = 0;
    indexBufferDesc.MiscFlags = 0;

	// Give the subresource structure a pointer to the index data.
	indexData.pSysMem = m_Index;

	// Create the index buffer.
	result = device->CreateBuffer(&indexBufferDesc, &indexData, &m_IndexBuffer);
	if(FAILED(result)){
		return false;
	}
	return true;
}



bool Detail3DClass::RenderBuffers(ID3D10Device* device)
{
	unsigned int stride;
	unsigned int offset;


	// Set vertex buffer stride and offset.
    stride = sizeof(VertexType); 
	offset = 0;
    
	// Set the vertex buffer to active in the input assembler so it can be rendered.
	device->IASetVertexBuffers(0, 1, &m_VertexBuffer, &stride, &offset);

    // Set the index buffer to active in the input assembler so it can be rendered.
    device->IASetIndexBuffer(m_IndexBuffer, DXGI_FORMAT_R32_UINT, 0);

    // Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
	// ��������
	device->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	// �����
	//device->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_LINESTRIP);
	// �����
	//device->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_POINTLIST);
	return true;
}

StaticModel3DClass::StaticModel3DClass(){
	m_Details.clear();
	m_Materials.clear();
	countOfDetail = 0;	
	countOfMaterial = 0;
}
StaticModel3DClass::~StaticModel3DClass(){
}
void StaticModel3DClass::Shutdown(){
	// ������� ������
	// ������� ���������� ���������
	for ( unsigned int i=0; i < m_Details.size(); i++){
		m_Details[i].Shutdown();
	}
	m_Details.clear();

	if (m_Materials.size()>0){
		m_Materials.clear();
	}
	countOfDetail = 0;	
	countOfMaterial = 0;

}
bool StaticModel3DClass::Initialize(ID3D10Device* device, std::string path){
	bool result = false;;
	// ����� ���������
	if (path.find(".obj") != std::string::npos){
		result = LoadModelObj(device, path);
	} else
	if (path.find(".s3m") != std::string::npos){
		result = LoadModelS3m(device, path);
	}
	// ��������� �� ����������
	return result;

}
bool StaticModel3DClass::LoadModelObj(ID3D10Device* device, std::string path){
	bool result;
	// ��������� ������ ������
	ObjClass loadObj;
	StaticModelObjType loadModel;
	// �������� ������ � ������������ ��������� � �������
	result = loadObj.ImportObj(loadModel, path);
	if (result == true){
		// ��������� �������
		std::vector<VertexType> vecVertex;
		std::vector<int> vecIndex;
		// ���������� ���������� ���������� 
		countOfMaterial = loadModel.countOfMaterials;
		m_Materials.resize(countOfMaterial);
		m_Materials = loadModel.m_Materials;
		// ���������� ���������� �������
		countOfDetail = loadModel.countOfDetails;
		// �������� ���������� ������� � ������
		m_Details.resize(countOfDetail);
		// ���������� ��� ������
		for (int i=0; i<countOfDetail; i++){
			// �������� ��������� ������� � �������
			loadModel.m_Details[i].GetCamputeDetail(vecVertex,vecIndex);
			// ������������� ����� ������� 3� ������ directx �������
			m_Details[i].Initialize(device, vecVertex, vecIndex);
			// ��������� �������������� ������
			m_Details[i].name = loadModel.m_Details[i].nameDetail;
			m_Details[i].materialID = loadModel.m_Details[i].materialID;
			m_Details[i].radius = loadModel.m_Details[i].radius;
			m_Details[i].center = loadModel.m_Details[i].center;
			m_Details[i].max = loadModel.m_Details[i].max;
			m_Details[i].min = loadModel.m_Details[i].min;
			

		}	
	}
	return result;
}
//
bool StaticModel3DClass::LoadModelS3m(ID3D10Device* device, std::string path){
	// �������� �������� 
	std::vector<VertexType> vecVertex;
	std::vector<int> vecIndex;
	// ����� ������
	std::ifstream file;
	 //��������� ���� � �������� ������ ������ ��� ������
	file.open(path,std::ios::binary);
	// ��������� �� ����������� ������ ��� ��������
	if(file.fail()){
		return false;
	}
	VertexType vertex;
	int index;
	unsigned int count;
	std::string text;
	// ��������� ������
	fileReadString(file, text);
	// ���������, ��� ��� ��� ������
	if (text != "s3m"){
		file.close();
		return false;
	}
	// ��������� ������, ����� �����
	fileReadString(file, text);
	// ��������� ���������� ������� � ������
	int countDetail;
	file.read((char*)&countDetail, sizeof(int));
	// ��������� ���������� �������
	countOfDetail = countDetail;
	m_Details.resize(countOfDetail);
	// ��������� ������ ������
	for (int i=0; i<countOfDetail; i++){
		// ��������� �������� ������
		fileReadString(file, m_Details[i].name); 
		// ���������� ������ ���������, -1, ���� ��������� �����������
		file.read((char*)&m_Details[i].materialID, sizeof(int)); 
		// ���������� ��������� ������ (����������� �������)
		file.read((char*)&m_Details[i].max, sizeof(D3DXVECTOR3));
		file.read((char*)&m_Details[i].min, sizeof(D3DXVECTOR3));
		file.read((char*)&m_Details[i].center, sizeof(D3DXVECTOR3));
		file.read((char*)&m_Details[i].radius, sizeof(float));
		// ��������� ���������� ������
		file.read((char*)&count, sizeof(unsigned int));
		vecVertex.resize(count);
		// ��������� ��� �������
		for (unsigned int k=0; k<count; k++){
			// ��������� �������
			file.read((char*)&vertex, sizeof(VertexType));
			// ��������� �������
			vecVertex[k] = vertex;
		}
		// ��������� ���������� ��������
		file.read((char*)&count, sizeof(unsigned int));
		vecIndex.resize(count);
		// ��������� ��� �������
		for (unsigned int k=0; k<count; k++){
			// ��������� ������
			file.read((char*)&index, sizeof(int));
			// ���������� ������
			vecIndex[k] = index;
		}
		// ��������� ������
		m_Details[i].Initialize(device, vecVertex, vecIndex);
		vecVertex.clear();
		vecIndex.clear();
	}
	// ��������� ���������� ����������
	int countMaterial;
	file.read((char*)&countMaterial, sizeof(int));
	// ��������� ���������� ����������
	countOfMaterial = countMaterial;
	m_Materials.resize(countOfMaterial);
	// ��������� ������ ��������
	for (int i=0; i<countOfMaterial; i++){
		fileReadString(file, m_Materials[i].nameMaterial);
		fileReadString(file, m_Materials[i].pathAmbient);
		fileReadString(file, m_Materials[i].pathDiffuse);
		fileReadString(file, m_Materials[i].pathSpecular);
		file.read((char*)&m_Materials[i].Ka, sizeof(D3DXVECTOR3));
		file.read((char*)&m_Materials[i].Kd, sizeof(D3DXVECTOR3));
		file.read((char*)&m_Materials[i].Ks, sizeof(D3DXVECTOR3));
		file.read((char*)&m_Materials[i].d, sizeof(float));
		file.read((char*)&m_Materials[i].Ns, sizeof(float));	
	}
	//��������� ����
	file.close(); 
	return true;
}
bool StaticModel3DClass::Render(ID3D10Device* device,int index){
	bool result = false;
	// ��������� ���������� ������
	if (index < 0 || index >= countOfDetail){
		return false;
	}
	m_Details[index].Render(device);

	return true;
}
int StaticModel3DClass::GetIndexCount(int index){
	return m_Details[index].GetIndexCount();
}
int StaticModel3DClass::GetVertexCount(int index){
	return m_Details[index].GetVertexCount();
}
int StaticModel3DClass::GetCountDetail(){
	return countOfDetail;
}
int StaticModel3DClass::GetCountMaterial(){
	return countOfMaterial;
}

MaterialInfoType StaticModel3DClass::GetMaterial(int index){
	return m_Materials[index];
}
float StaticModel3DClass::GetRadiusDetail(int index){
	return m_Details[index].radius;
}
D3DXVECTOR3 StaticModel3DClass::GetCenterDetail(int index){
	return m_Details[index].center;
}
void StaticModel3DClass::SetMaterialDetail(int index, MaterialInfoType material){
	// ��������� ����� ��������
	m_Materials.push_back(material);
	// ����������� ���������� ����������
	countOfMaterial+=1;
	// ������� ��� ����� �������� ���������� ����� ��������
	m_Details[index].materialID = countOfMaterial-1; // ������ ���������� ���������
}
MaterialInfoType StaticModel3DClass::GetMaterialDetail(int index){
	if (m_Details[index].materialID	>= 0 && m_Details[index].materialID < countOfMaterial){
		return m_Materials[m_Details[index].materialID];
	}
	// ������ �������� 
	MaterialInfoType result;
	return result;
}
// ������ � ������
void StaticModel3DClass::fileReadString(std::ifstream& file, std::string& text){
	unsigned int length;
	// ������ ���������� �������� � ������
	file.read((char*)&length, sizeof(unsigned int));
	char* line = new char [length];
	// ������ ������
	file.read(line, length);
	text = line;
	// ������ ���������� ����� �� �������
	text.resize(length);
	delete[] line;
}
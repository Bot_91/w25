// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
////////////////////////////////////////////////////////////////////////////////
// Filename: TerrainTileClass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "TerrainTileClass.h"
TerrainPolygonClass::TerrainPolygonClass(){
	m_VertexBuffer = 0;
	m_IndexBuffer = 0;
	m_IndexCount = 0;
	m_VertexCount = 0;
	flagCreat = false;
}
void TerrainPolygonClass::Initialize(ID3D10Device* device, std::vector<D3DXVECTOR3> vertex) {
	std::vector <VertexType> m_Model;
	this->Shutdown();
	m_Model.resize(vertex.size());
	for (unsigned int i = 0; i < vertex.size(); i++) {
		m_Model[i].position = vertex[i];
	}
	this->InitializeBuffers(device, m_Model);
	flagCreat = true;
	

}

void TerrainPolygonClass::ReleaseBuffers() {
	// ����������� ��������� �����.
	if (m_IndexBuffer) {
		m_IndexBuffer->Release();
		m_IndexBuffer = 0;
	}
	// ����������� ��������� �����
	if (m_VertexBuffer) {
		m_VertexBuffer->Release();
		m_VertexBuffer = 0;
	}
	return;
}

TerrainPolygonClass::~TerrainPolygonClass() {
	this->Shutdown();
}
void TerrainPolygonClass::Shutdown() {
	this->ReleaseBuffers();
	flagCreat = false;
	m_IndexCount = 0;
}
void TerrainPolygonClass::Render(ID3D10Device* device) {
	if (flagCreat == true) {
		this->RenderBuffers(device);
	}
}
void TerrainPolygonClass::RenderBuffers(ID3D10Device* device){
	unsigned int stride;
	unsigned int offset;
	// Set vertex buffer stride and offset.
	stride = sizeof(VertexType);
	offset = 0;

	// Set the vertex buffer to active in the input assembler so it can be rendered.
	device->IASetVertexBuffers(0, 1, &m_VertexBuffer, &stride, &offset);

	// Set the index buffer to active in the input assembler so it can be rendered.
	device->IASetIndexBuffer(m_IndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	// Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
	// ��������
	//device->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	// �����
    device->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_LINESTRIP);
	// �����
	//device->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_POINTLIST);

	return;
}
bool TerrainPolygonClass::InitializeBuffers(ID3D10Device* device,
	std::vector <VertexType> m_Model) {
	unsigned long* indices;
	VertexType *vertex;
	D3D10_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D10_SUBRESOURCE_DATA vertexData, indexData;
	HRESULT result;
	m_VertexCount = m_Model.size();
	// ������������� ���������� ��������� ����� �� ��� � ���������� ������
	m_IndexCount = m_VertexCount * 3;

	// ������� ������ ��������
	indices = new unsigned long[m_IndexCount];
	if (!indices) {
		return false;
	}
	// �������� ����������
	int numberIndex = 0;
	for (int index = 0; index+6<m_IndexCount; index += 6) {

		indices[index] = numberIndex + 1;
		indices[index + 1] = numberIndex;
		indices[index + 2] = numberIndex + 2;
		indices[index + 3] = numberIndex + 2;
		indices[index + 4] = numberIndex + 3;
		indices[index + 5] = numberIndex + 1;
		numberIndex += 2;
		//1 0 2 2 3 1
	}
	indices[m_IndexCount - 1] = numberIndex - 1;
	indices[m_IndexCount - 2] = numberIndex + 1;
	indices[m_IndexCount - 3] = numberIndex ;

	indices[m_IndexCount - 4] = numberIndex;
	indices[m_IndexCount - 5] = numberIndex - 2;
	indices[m_IndexCount - 6] = numberIndex - 1;

	vertex = new VertexType[m_VertexCount];
	for (int i = 0; i < m_VertexCount; i++) {
		vertex[i] = m_Model[i];
	}

	// Set up the description of the vertex buffer.
	vertexBufferDesc.Usage = D3D10_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(VertexType) * m_VertexCount;
	vertexBufferDesc.BindFlags = D3D10_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;

	// Give the subresource structure a pointer to the vertex data.
	vertexData.pSysMem = vertex;

	// Now finally create the vertex buffer.
	result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_VertexBuffer);
	if (FAILED(result)) {
		return false;
	}

	// Set up the description of the index buffer.
	indexBufferDesc.Usage = D3D10_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * m_IndexCount;
	indexBufferDesc.BindFlags = D3D10_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;

	// Give the subresource structure a pointer to the index data.
	indexData.pSysMem = indices;

	// Create the index buffer.
	result = device->CreateBuffer(&indexBufferDesc, &indexData, &m_IndexBuffer);
	if (FAILED(result)) {
		return false;
	}

	delete[] indices;
	indices = 0;
	delete[] vertex;
	vertex = 0;
	return true;
}
////////////////////////////////////////////////////////////////////////////////////////////
TerrainRoundClass::TerrainRoundClass(void) {
	m_VertexBuffer = 0;
	m_IndexBuffer = 0;
	m_IndexCount = 0;
	flagCreat = true;
}
bool TerrainRoundClass::InitializeBuffers(ID3D10Device* device,
	std::vector <VertexType> m_Model) {
	unsigned long* indices;
	VertexType *vertex;
	D3D10_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D10_SUBRESOURCE_DATA vertexData, indexData;
	HRESULT result;
	m_VertexCount = m_Model.size();
	// ������������� ���������� ��������� ����� �� ��� � ���������� ������
	m_IndexCount = m_VertexCount * 3;

	// ������� ������ ��������
	indices = new unsigned long[m_IndexCount];
	if (!indices) {
		return false;
	}
	// �������� ����������
	int numberIndex = 0;
	for (int index = 0; index + 6<m_IndexCount; index += 6) {

		indices[index] = numberIndex + 1;
		indices[index + 1] = numberIndex;
		indices[index + 2] = numberIndex + 2;
		indices[index + 3] = numberIndex + 2;
		indices[index + 4] = numberIndex + 3;
		indices[index + 5] = numberIndex + 1;
		numberIndex += 2;
		//1 0 2 2 3 1
	}
	// ���������� �������
	indices[m_IndexCount - 6] = numberIndex + 1;
	indices[m_IndexCount - 6 + 1] = numberIndex;
	indices[m_IndexCount - 6 + 2] = 0;
	indices[m_IndexCount - 6 + 3] = 0;
	indices[m_IndexCount - 6 + 4] = 1;
	indices[m_IndexCount - 6 + 5] = numberIndex + 1;

	vertex = new VertexType[m_VertexCount];
	for (int i = 0; i < m_VertexCount; i++) {
		vertex[i] = m_Model[i];
	}

	// Set up the description of the vertex buffer.
	vertexBufferDesc.Usage = D3D10_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(VertexType) * m_VertexCount;
	vertexBufferDesc.BindFlags = D3D10_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;

	// Give the subresource structure a pointer to the vertex data.
	vertexData.pSysMem = vertex;

	// Now finally create the vertex buffer.
	result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_VertexBuffer);
	if (FAILED(result)) {
		return false;
	}

	// Set up the description of the index buffer.
	indexBufferDesc.Usage = D3D10_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * m_IndexCount;
	indexBufferDesc.BindFlags = D3D10_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;

	// Give the subresource structure a pointer to the index data.
	indexData.pSysMem = indices;

	// Create the index buffer.
	result = device->CreateBuffer(&indexBufferDesc, &indexData, &m_IndexBuffer);
	if (FAILED(result)) {
		return false;
	}

	delete[] indices;
	indices = 0;
	delete[] vertex;
	vertex = 0;
	return true;
}
////////////////////////////////////////////////////////////////////////////////////////////
TerrainTileClass::TerrainTileClass(){
	m_MaterialIndex.clear();
	m_MaterialIndexCount = 0;
	m_VertexBuffer = 0;
	m_IndexBuffer = 0;
	m_Model = 0;
	stateRender = 0;
	m_Width = 0;
	m_Height = 0;
	m_VertexCount = 0;
	m_IndexCount = 0;
	flagInitialize = false;
	flagModel = false;
}
int TerrainTileClass::GetCountMaterial() {
	return (int)m_MaterialIndex.size();
}
TerrainTileClass::TerrainTileClass(const TerrainTileClass& other){
}

TerrainTileClass::~TerrainTileClass(){
}

// ���������� ��� ������������ ���������
int TerrainTileClass::GetMaterial(int index) {
	return m_MaterialIndex[index];
}

// ����������� ����� �����
bool TerrainTileClass::Initialize(ID3D10Device* device) {
	bool result = flagModel;
	// ������� ������
	if (result == true) {
		ReleaseBuffers();
		InitializeBuffers(device);
		flagInitialize = true;
		return true;
	}

	return false;
}


void TerrainTileClass::Shutdown(){
	flagInitialize = false;
	flagModel = false;
	stateRender = 0;
	m_MaterialIndex.clear();
	m_MaterialIndexCount = 0;
	m_IndexCount = 0;
	m_VertexCount = 0;
	// ����������� �����
	ReleaseBuffers();
	// ����������� ������
	ReleaseModel();
	return;
}


void TerrainTileClass::Render(ID3D10Device* device){
	// Put the vertex and index buffers on the graphics pipeline to prepare them for drawing.
	RenderBuffers(device);
	return;
}
void TerrainTileClass::RenderRound(ID3D10Device* device) {
	// Put the vertex and index buffers on the graphics pipeline to prepare them for drawing.
	//m_Round.Render(device);
	return;
}
void TerrainTileClass::RenderLine(ID3D10Device* device) {
	// Put the vertex and index buffers on the graphics pipeline to prepare them for drawing.
	///m_Line.Render(device);
	return;
}


int TerrainTileClass::GetIndexCount(){
	return m_IndexCount;
}

// ������� ������ �����������
bool TerrainTileClass::CreatePlane(	float offset_X, float offset_Y, int width, int height) {
	// ���������� ������ ������
	Shutdown();
	m_Width = width;
	m_Height = height;
	m_VertexCount = m_Width * m_Height;
	// ���������� ���������� �������� � �����
	int sizeTileTexture = 1;
	float stepTileTexture = 1.0f / (float)sizeTileTexture;
	// ��������� ���������
	// �������� ������
	m_Model = new TerrainTileClass::VertexType[m_VertexCount];
	// ���������� ������ ������
	for (int j = 0; j < m_Height; j++) {
		for (int i = 0; i < m_Width; i++) {
			m_Model[j*m_Width + i].position.x = (float)i + offset_X;
			m_Model[j*m_Width + i].position.y = 0.0f;
			m_Model[j*m_Width + i].position.z = (float)j + offset_Y;
			m_Model[j*m_Width + i].normal = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
			m_Model[j*m_Width + i].tangent = D3DXVECTOR3(1.0f, 0.0f, 0.0f);
			m_Model[j*m_Width + i].binormal = D3DXVECTOR3(0.0f, 0.0f, -1.0f);
			m_Model[j*m_Width + i].texture = D3DXVECTOR2(stepTileTexture * i,
															 stepTileTexture * j);
			m_Model[j*m_Width + i].mask = D3DXVECTOR4(0.0f, 0.0f, 0.0f, 0.0f);
		}
	}
	// ��������� ����� ���������
	m_MaterialIndex.push_back(0);
	m_MaterialIndex.push_back(1);
	m_MaterialIndexCount = 2;
	flagModel = true;
	return true;
}
// ��������� �������� � ����
bool TerrainTileClass::SaveTerrainMap(std::ofstream& fileSaveTerrain) {


	// ���������� ���������� ����������
	fileSaveTerrain.write((char*)&m_MaterialIndexCount, sizeof(int));
	// ���������� ������� ������� ���������
	for (int i = 0; i < m_MaterialIndexCount; i++) {
		fileSaveTerrain.write((char*)&m_MaterialIndex[i], sizeof(int));
	}
	// ���������� ������ �����
	fileSaveTerrain.write((char*)&m_Width, sizeof(LONG));
	fileSaveTerrain.write((char*)&m_Height, sizeof(LONG));
	// ���������� ��� ������� �����
	for (int i = 0; i < m_VertexCount; i++) {
		// ����������
		fileSaveTerrain.write((char*)&m_Model[i].position, sizeof(D3DXVECTOR3));
		// ������� ��������� ��������
		fileSaveTerrain.write((char*)&m_Model[i].normal, sizeof(D3DXVECTOR3));
		fileSaveTerrain.write((char*)&m_Model[i].tangent, sizeof(D3DXVECTOR3));
		fileSaveTerrain.write((char*)&m_Model[i].binormal, sizeof(D3DXVECTOR3));
		// ��� ���������� ��������
		fileSaveTerrain.write((char*)&m_Model[i].texture, sizeof(D3DXVECTOR2));
		// ������� ��������
		fileSaveTerrain.write((char*)&m_Model[i].mask, sizeof(D3DXVECTOR4));
	}

	return true;
}
// ��������� �������� �� ������� ������
bool TerrainTileClass::LoadTerrainMap(std::ifstream& fileOpenTerrain){
	// ������� ������ ���� (����� ��������� ��������� �����)
	Shutdown();
	// ��������� ���������� ����������
	fileOpenTerrain.read((char*)&m_MaterialIndexCount, sizeof(int));
	// ��������� �� ������� ���������
	for (int i = 0; i < m_MaterialIndexCount; i++) {
		int indexMaterial;
		// ��������� ������ ����������
		fileOpenTerrain.read((char*)&indexMaterial, sizeof(int));
		// ��������� ����� ���������� �����
		m_MaterialIndex.push_back(indexMaterial);
	}
	// ��������� ������ �����
	fileOpenTerrain.read((char*)&m_Width, sizeof(LONG));
	fileOpenTerrain.read((char*)&m_Height, sizeof(LONG));
	m_VertexCount = m_Width*m_Height;
	// �������� ������ ��� ������ �����
	m_Model = new TerrainTileClass::VertexType[m_VertexCount];
	if (m_Model == 0) {
		return false;
	}
	// ��������� ��� ������� �����
	for (int i = 0; i < m_VertexCount; i++) {
		// ����������
		fileOpenTerrain.read((char*)&m_Model[i].position, sizeof(D3DXVECTOR3));
		// ������� ��������� ��������
		fileOpenTerrain.read((char*)&m_Model[i].normal, sizeof(D3DXVECTOR3));
		fileOpenTerrain.read((char*)&m_Model[i].tangent, sizeof(D3DXVECTOR3));
		fileOpenTerrain.read((char*)&m_Model[i].binormal, sizeof(D3DXVECTOR3));
		// ��� ���������� ��������
		fileOpenTerrain.read((char*)&m_Model[i].texture, sizeof(D3DXVECTOR2));
		// ������� ��������
		fileOpenTerrain.read((char*)&m_Model[i].mask, sizeof(D3DXVECTOR4));
	}
	flagModel = true;
	return true;
}


// ���������� ������ 
bool TerrainTileClass::GetHeight(D3DXVECTOR3 position, float &height) {
	height = 0.0f;
	//VertexType vertex1,vertaex2,vertex3;
	int index1 = ((int)position.z - (int)m_Model[0].position.z)*m_Width +
					((int)position.x - (int)m_Model[0].position.x);
	int index2 = ((int)position.z - (int)m_Model[0].position.z)*m_Width +
					((int)position.x + 1 - (int)m_Model[0].position.x);
	int index3 = ((int)position.z + 1 - (int)m_Model[0].position.z)*m_Width +
					((int)position.x - (int)m_Model[0].position.x);
	int index4 = ((int)position.z + 1 - (int)m_Model[0].position.z)*m_Width +
					((int)position.x + 1 - (int)m_Model[0].position.x);
	// ����� �� ���������
	if (index1 < m_VertexCount && index1 >= 0 && 
		index2 < m_VertexCount && index2 >= 0 &&
		index3 < m_VertexCount && index3 >= 0 &&
		index4 < m_VertexCount && index4 >= 0) {
		// ���������� �� ����� ������� ����� ����� (������� ������������ � ���������)
		float a = (m_Model[index1].position.x - position.x) *
			(m_Model[index2].position.z - m_Model[index1].position.z) -
			(m_Model[index2].position.x - m_Model[index1].position.x) *
			(m_Model[index1].position.z - position.z);
		float b = (m_Model[index2].position.x - position.x) *
		(m_Model[index4].position.z - m_Model[index2].position.z) -
		(m_Model[index4].position.x - m_Model[index2].position.x) *
		(m_Model[index2].position.z - position.z);
		float c = (m_Model[index4].position.x - position.x) *
		(m_Model[index1].position.z - m_Model[index4].position.z) -
		(m_Model[index1].position.x - m_Model[index4].position.x) *
		(m_Model[index4].position.z - position.z);

		D3DXVECTOR3 v1, v2, v3;
		// ���������� � ������ �������� ����������� �� �����
		if ((a >= 0 && b >= 0 && c >= 0) || (a <= 0 && b <= 0 && c <= 0)) {
			v1 = m_Model[index1].position;
			v2 = m_Model[index2].position;
			v3 = m_Model[index4].position;
		}else {
			v1 = m_Model[index4].position;
			v2 = m_Model[index1].position;
			v3 = m_Model[index3].position;
		}	
		// ����������� ��������� y �� ���������
		float k1 = (v2.z - v1.z)*(v3.x - v1.x) - (v2.x - v1.x)*(v3.z - v1.z);
		float k2 = (position.x - v1.x)*((v2.z - v1.z)*(v3.y - v1.y) - (v2.y - v1.y)*(v3.z - v1.z));
		float k3 = (position.z - v1.z)*((v2.y - v1.y)*(v3.x - v1.x) - (v2.x - v1.x)*(v3.y - v1.y));
		height = (k2 + k3) / k1 + v1.y;
		return true;

	}
	return false;
}
//
//			
// ���������� ������ � ���������� ������� � ���� �����
bool TerrainTileClass::GetHeight(D3DXVECTOR3 position, float &height, D3DXVECTOR3 &normal) {
	height = 0.0f;
	//VertexType vertex1,vertaex2,vertex3;
	int index1 = ((int)position.z - (int)m_Model[0].position.z)*m_Width +
		((int)position.x - (int)m_Model[0].position.x);
	int index2 = ((int)position.z - (int)m_Model[0].position.z)*m_Width +
		((int)position.x + 1 - (int)m_Model[0].position.x);
	int index3 = ((int)position.z + 1 - (int)m_Model[0].position.z)*m_Width +
		((int)position.x - (int)m_Model[0].position.x);
	int index4 = ((int)position.z + 1 - (int)m_Model[0].position.z)*m_Width +
		((int)position.x + 1 - (int)m_Model[0].position.x);
	// ����� �� ���������
	if (index1 < m_VertexCount && index1 >= 0 &&
		index2 < m_VertexCount && index2 >= 0 &&
		index3 < m_VertexCount && index3 >= 0 &&
		index4 < m_VertexCount && index4 >= 0 &&
		position.z >= 0 && position.x >= 0) {
		// ���������� �� ����� ������� ����� ����� (������� ������������ � ���������)
		float a = (m_Model[index1].position.x - position.x) *
			(m_Model[index2].position.z - m_Model[index1].position.z) -
			(m_Model[index2].position.x - m_Model[index1].position.x) *
			(m_Model[index1].position.z - position.z);
		float b = (m_Model[index2].position.x - position.x) *
			(m_Model[index4].position.z - m_Model[index2].position.z) -
			(m_Model[index4].position.x - m_Model[index2].position.x) *
			(m_Model[index2].position.z - position.z);
		float c = (m_Model[index4].position.x - position.x) *
			(m_Model[index1].position.z - m_Model[index4].position.z) -
			(m_Model[index1].position.x - m_Model[index4].position.x) *
			(m_Model[index4].position.z - position.z);

		D3DXVECTOR3 v1, v2, v3;
		// ���������� � ������ �������� ����������� �� �����
		if ((a >= 0 && b >= 0 && c >= 0) || (a <= 0 && b <= 0 && c <= 0)) {
			v1 = m_Model[index1].position;
			v2 = m_Model[index2].position;
			v3 = m_Model[index4].position;
			D3DXVec3Cross(&normal, &D3DXVECTOR3(v1 - v2),
				&D3DXVECTOR3(v3 - v2));
		}
		else {
			v1 = m_Model[index4].position;
			v2 = m_Model[index3].position;
			v3 = m_Model[index1].position;
			D3DXVec3Cross(&normal, &D3DXVECTOR3(v2 - v1),
				&D3DXVECTOR3(v2 - v3));
		}

		// ����������� ��������� y �� ���������
		float k1 = (v2.z - v1.z)*(v3.x - v1.x) - (v2.x - v1.x)*(v3.z - v1.z);
		float k2 = (position.x - v1.x)*((v2.z - v1.z)*(v3.y - v1.y) - (v2.y - v1.y)*(v3.z - v1.z));
		float k3 = (position.z - v1.z)*((v2.y - v1.y)*(v3.x - v1.x) - (v2.x - v1.x)*(v3.y - v1.y));
		height = (k2 + k3) / k1 + v1.y;
		D3DXVec3Normalize(&normal, &normal);
		return true;

	}
	return false;
}
// ������ ������, � �� ������� ����� ����������� � �����������
bool TerrainTileClass::GetIndexVertex(D3DXVECTOR3 point, int& index) {
	// ����� ����������� �����
	point.x = point.x - m_Model[0].position.x;
	point.z = point.z - m_Model[0].position.z;

	if ((int)(point.x ) >= 0 &&
		(int)(point.x ) < m_Width &&
		(int)(point.z ) >= 0 &&
		(int)(point.z ) < m_Height) {

		index = (int)(point.z)*(m_Width-1) +(int)(point.x);
		return true;
	}
	return false;
}

void TerrainTileClass::AlterPoint(int index, float value, alter_argument argument){
	// ����������� ������, �� ������� �� �������
	if ((index >= 0) && (index < m_VertexCount)) {
		// ���������� ���������� ��������
		switch (argument) {
			case alter_argument::POSSITION:
				m_Model[index].position.y += value;
				break;
			case alter_argument::DEFAULT:
				break;
			case alter_argument::MASK1:
				m_Model[index].mask.x = value;
				D3DXVec4Normalize(&m_Model[index].mask, &m_Model[index].mask);
				break;
			case alter_argument::MASK2:
				m_Model[index].mask.y = value;
				D3DXVec4Normalize(&m_Model[index].mask, &m_Model[index].mask);
				break;
			case alter_argument::MASK3:
				m_Model[index].mask.z = value;
				D3DXVec4Normalize(&m_Model[index].mask, &m_Model[index].mask);
				break;
			case alter_argument::MASK4:
				m_Model[index].mask.w = value;
				D3DXVec4Normalize(&m_Model[index].mask, &m_Model[index].mask);
				break;		
		}		
	}
}
// 
TerrainTileClass::VertexType TerrainTileClass::GetVertex(D3DXVECTOR3 point) {
	int index = ((int)point.z - (int)m_Model[0].position.z)*m_Width +
				((int)point.x - (int)m_Model[0].position.x);
	if (index >= 0 && index < m_VertexCount) {
		return m_Model[index];
	}
	return TerrainTileClass::VertexType();
}
// ��������� � ������������� ������� �������� � ���� ������� ��������� ���������
 bool TerrainTileClass::SetCamputeNodeMeanNormalTangentBinormal(TerrainTileClass::VertexType vertex[5], 
																int index) {
	// ��������������� �������
	D3DXVECTOR3 v1, v2, v3;
	// �������
	D3DXVECTOR3 normal[4];
	// ���������
	D3DXVECTOR3 binormal[4];
	// ��������
	D3DXVECTOR3 tangent[4];
	// �������� �������� � ����
	if (index < 0  || index > m_VertexCount) {
		return false;
	}
	//		1	
	//	4	0	2 
	//		3
	for (int i = 0; i < 4; i++) {
		switch (i){
			//-------------------------------------------------------------------------
			// ������� ������� ������ ����
			//-------------------------------------------------------------------------
			case 0:
				v1 = vertex[0].position;
				v2 = vertex[2].position;
				v3 = vertex[1].position;
				break;
			//-------------------------------------------------------------------------
			// ������� ������� ����� ����
			//-------------------------------------------------------------------------
			case 1:
				v1 = vertex[0].position;
				v2 = vertex[1].position;
				v3 = vertex[4].position;
				break;
			//-------------------------------------------------------------------------
			// ������� ������ ����� ����
			//-------------------------------------------------------------------------
			case 2:
				v1 = vertex[0].position;
				v2 = vertex[4].position;
				v3 = vertex[3].position;
				break;
			//-------------------------------------------------------------------------
			// ������� ������ ������ ����
			//-------------------------------------------------------------------------
			case 3:
				v1 = vertex[0].position;
				v2 = vertex[3].position;
				v3 = vertex[2].position;
				break;
		}
		// ������� �������
		D3DXVec3Cross(&normal[i], &D3DXVECTOR3(v2 - v1), &D3DXVECTOR3(v1 - v3) );
		D3DXVec3Normalize(&normal[i], &normal[i]);
		// ������� ���������
		D3DXVec3Cross(&binormal[i], &D3DXVECTOR3(-1.0f, 0.0f, 0.0f), &normal[i] );
		D3DXVec3Normalize(&binormal[i], &binormal[i]);
		// ������� ��������
		D3DXVec3Cross(&tangent[i],  &binormal[i], &normal[i]);
		D3DXVec3Normalize(&tangent[i], &tangent[i]);
	}
	TerrainTileClass::VertexType result;
	result.normal = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	result.binormal = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	result.tangent = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	// ��������� ��������
	for (int i = 0; i < 4; i++) {
		result.normal = result.normal + normal[i];
		result.binormal = result.binormal + binormal[i];
		result.tangent = result.tangent + tangent[i];
	}
	m_Model[index].normal = result.normal / 4.0f;
	D3DXVec3Normalize(&m_Model[index].normal, &m_Model[index].normal);
	m_Model[index].binormal = result.binormal / 4.0f;
	D3DXVec3Normalize(&m_Model[index].binormal, &m_Model[index].binormal);
	m_Model[index].tangent = result.tangent / 4.0f;
	D3DXVec3Normalize(&m_Model[index].tangent, &m_Model[index].tangent);

	return true;
}


// ���������� ����� �����������, ���������� �� ����������, ��������� �� �������, � ��������� � �� �������
bool TerrainTileClass::GetIntersects(D3DXVECTOR3 position, D3DXVECTOR3 view, D3DXVECTOR3& point) {
	return false;
}

bool TerrainTileClass::InitializeBuffers(ID3D10Device* device){
	unsigned long* indices;
	D3D10_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
    D3D10_SUBRESOURCE_DATA vertexData, indexData;
	HRESULT result;
	// ���������� ������ �����
	ReleaseBuffers();
	// ������������� ���������� �������� (������ ���� �� �������)
	m_IndexCount = 6*m_VertexCount - m_Width;

	// ������� ������ ��������
	indices = new unsigned long[m_IndexCount];
	if(indices == 0){
		return false;
	}

	int n = 0, m = 0;
	// �������� ����������
	for (int i = 0; i < m_IndexCount / 6; i++) {
		// ��������� ���
		if (i + m + m_Width + 1 < m_VertexCount){
			indices[i * 6] = i + m;
			indices[i * 6 + 1] = i + m + m_Width;
			indices[i * 6 + 2] = i + m + 1;
			indices[i * 6 + 3] = i + m + 1;
			indices[i * 6 + 4] = i + m + m_Width;
			indices[i * 6 + 5] = i + m + m_Width + 1;
			n++;
			if (n == (m_Width - 1)) {
				m++;
				n = 0;
			}
		}
	}

	// Set up the description of the index buffer.
    indexBufferDesc.Usage = D3D10_USAGE_DEFAULT;
    indexBufferDesc.ByteWidth = sizeof(unsigned long) * m_IndexCount;
    indexBufferDesc.BindFlags = D3D10_BIND_INDEX_BUFFER;
    indexBufferDesc.CPUAccessFlags = 0;
    indexBufferDesc.MiscFlags = 0;

	// Give the subresource structure a pointer to the index data.
    indexData.pSysMem = indices;

	// Create the index buffer.
	result = device->CreateBuffer(&indexBufferDesc, &indexData, &m_IndexBuffer);
	if(FAILED(result)){
		return false;
	}

	delete [] indices;
	indices = 0;

	// Set up the description of the vertex buffer.
	vertexBufferDesc.Usage = D3D10_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(TerrainTileClass::VertexType) * m_VertexCount;
	vertexBufferDesc.BindFlags = D3D10_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;

	// Give the subresource structure a pointer to the vertex data.
	vertexData.pSysMem = m_Model;

	// Now finally create the vertex buffer.
	result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_VertexBuffer);
	if (FAILED(result)) {
		return false;
	}
	return true;
}

void TerrainTileClass::ReleaseModel(){
	// ����������� ������
	if(m_Model){
		delete [] m_Model;
		m_Model = 0;
	}
	m_Height = 0;
	m_Width = 0;
	return;
}

void TerrainTileClass::ReleaseBuffers(){
	// ����������� ��������� �����.
	if(m_IndexBuffer){
		m_IndexBuffer->Release();
		m_IndexBuffer = 0;
	}

	// ����������� ��������� �����
	if(m_VertexBuffer){
		m_VertexBuffer->Release();
		m_VertexBuffer = 0;
	}
	return;
}
// ������� ��������
void TerrainTileClass::CreateLine(ID3D10Device* device, D3DXVECTOR3 PosStart, D3DXVECTOR3 PosStop) {
	// ���������� ���������� �� ��������
	std::vector<D3DXVECTOR3> vertex;
	vertex.resize(1200);
	PosStart.y = 0;
	PosStop.y = 0; 
	float stepX = (PosStop.x - PosStart.x)/(float)(vertex.size()-2);
	float stepZ = (PosStop.z - PosStart.z)/ (float)(vertex.size()-2);
	D3DXVECTOR3 width;
	D3DXVec3Cross(&width, &D3DXVECTOR3(PosStop - PosStart), &D3DXVECTOR3(0.0f, 1.0f, 0.0f));
	D3DXVec3Normalize(&width, &width);
	/*if (PosStart.z > PosStop.z) {
		PosStart.z = PosStop.z;
	}*/
	for (unsigned int i = 0; i < vertex.size(); i += 2) {
		vertex[i].x = PosStart.x + stepX*(float)i;
		vertex[i].z = PosStart.z + stepZ*(float)i;
		vertex[i].y = 0;
		// �������� ������ ��� ���������
		this->GetHeight(vertex[i], vertex[i].y);
		vertex[i].y += 0.750f;

		vertex[i + 1].x = PosStart.x - width.x + (stepX)*(float)(i);
		vertex[i + 1].z = PosStart.z - width.z + (stepZ)*(float)(i);
		vertex[i + 1].y = vertex[i].y;

	}


	//m_Line.Initialize(device, vertex);
}
// ������� ���������� ��������������� �� ��������
void TerrainTileClass::CreateRound(ID3D10Device* device, D3DXVECTOR3 position, int radius) {

	// ���������� ���������� �� ��������
	std::vector<D3DXVECTOR3> vertex;
	vertex.resize(120);
	float width = 0.2f;
	float step = 360.0f / (float)vertex.size();
	for (unsigned int i = 0; i < vertex.size(); i+=2) {
		vertex[i].x = position.x + (float)radius*cos(step*(float)i*0.0174532925f);
		vertex[i].z = position.z + (float)radius*sin(step*(float)i*0.0174532925f);
		vertex[i].y = 0;
		// ������
		this->GetHeight(vertex[i], vertex[i].y);
		vertex[i].y += 0.25f;

		vertex[i+1].x = position.x + (float)(radius+ width)*cos(step*(float)i*0.0174532925f);
		vertex[i+1].z = position.z + (float)(radius+ width)*sin(step*(float)i*0.0174532925f);
		vertex[i+1].y = vertex[i].y;

	}
	//m_Round.Initialize(device, vertex);

}
int TerrainTileClass::GetIndexRoundCount(){
	return 0/* m_Round.m_IndexCount*/;
}
int TerrainTileClass::GetIndexLineCount() {
	return 0/* m_Line.m_IndexCount*/;
}
void TerrainTileClass::SetStateRender(int state){
	stateRender = state;
}
void TerrainTileClass::RenderBuffers(ID3D10Device* device){
	unsigned int stride;
	unsigned int offset;


	// Set vertex buffer stride and offset.
    stride = sizeof(TerrainTileClass::VertexType);
	offset = 0;
    
	// Set the vertex buffer to active in the input assembler so it can be rendered.
	device->IASetVertexBuffers(0, 1, &m_VertexBuffer, &stride, &offset);

    // Set the index buffer to active in the input assembler so it can be rendered.
    device->IASetIndexBuffer(m_IndexBuffer, DXGI_FORMAT_R32_UINT, 0);
	
    // Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
	// ������ ������ ������
	switch (stateRender) {
		// �����������
		case 0:
			device->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
			break;
		// �����
		case 1:
			device->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_LINESTRIP);
			break;
		// �����
		case 2:
			device->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_POINTLIST);
			break;
	}


	return;
}
////////////////////////////////////////////////////////////////////////////////
// Filename: objclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _OBJCLASS_H_
#define _OBJCLASS_H_


//#define MINF 1.7e-300
//#define INF 1.7e+300
//////////////
// INCLUDES //
//////////////
// ��������
#include <vector>
// ���������� ����� ������
#include <fstream>
#include <string>
#include <sstream>
// �������� � ���������
#include <D3DX10math.h>

struct VertexType{
	D3DXVECTOR3 position;
	D3DXVECTOR2 texture;
	D3DXVECTOR3 normal;
	D3DXVECTOR3 tangent;
	D3DXVECTOR3 binormal;
};
struct VertexIndex{
	int vertexIndex;
	int textureIndex;
	int normalIndex;
};
struct FaceType{
	int vertexIndex[3];	// ������ ��������
	int textureIndex[3];	// ������� ���������� ���������
	int normalIndex[3]; // ������� ��������
};
// ���������� � ���������
struct MaterialInfoType{
	MaterialInfoType();
	std::string  nameMaterial;		// �������� ���������
	std::string  pathAmbient;		// ���� ������� �������� �������
	std::string  pathDiffuse;		// ���� ������������ �������� �����
	std::string  pathNormal;		// ���� �� ����� �������� ��������
	std::string  pathSpecular;		// ���� ������ �������
	D3DXVECTOR3 Ka;					// Ambient-�������� ��������� 
	D3DXVECTOR3 Kd;					// Diffuse-�������� ��������� 
	D3DXVECTOR3 Ks;					// Specular-�������� ���������
	float d;						// ������������ 0..1 
	float Ns;						// ���� ��������
} ;
struct MaskInfoType{
	MaskInfoType();
	std::string pathMask;
};

//���������� �������. ������ � OBJ
class DetailObjClass {
public:
	DetailObjClass();
	~DetailObjClass();
	//void Shutdown();
	bool GetCamputeDetail(std::vector<VertexType>& vecVertex, std::vector<int>& vecIndex);
	//int GetCountVertex();			// ���������� ���������� ������
	//int GetCountIndex();			// ���������� ���������� ��������
    int materialID;					// ������ ���������, ��� ������ ������
	std::string nameDetail;			// �������� ������ ������
    D3DXVECTOR3 max;				// ������������ �������
	D3DXVECTOR3 min;				// ����������� �������
	D3DXVECTOR3 center;				// ���������� ������ ������
	float radius;					// ������ ������
	std::vector<D3DXVECTOR3> m_Vertex;          // ������� ������
	std::vector<D3DXVECTOR3> m_Normals;			// ������� ������
    std::vector<D3DXVECTOR2> m_TexVertex;		// ���������� UV ����������
	std::vector<FaceType>	 m_Faces;			// ������������,���������� � ���������� ������� ������
	int countOfVertex;				// ���������� ��������� � ������
	int countOfNormal;				// ���������� ��������
	int countOfFaces;				// ���������� ���������
	int countTexVertex;				// ���������� ���������� ���������
private:
	// ������� �������� ���������� � ��������� ������ � �������, ������� � ��������� �������
	void CamputeDetailVectors(std::vector<VertexType>& vertex);
	// ���������� ������� �� ������� � ���������
	void CamputeNormal(D3DVECTOR tangent, D3DVECTOR binormal, D3DVECTOR& normal);
	// ���������� ������� � ���������
	void CamputeTangentBinormal(VertexType vertex1, VertexType vertex2, VertexType vertex3,
								D3DVECTOR& tangent, D3DVECTOR& binormal);
	// ��������������� ������� ����������� ������� � ������
	int AddVertex(std::vector<VertexType>& vecVertex, const VertexType& vert);	
	// �������� ������������ ������ � �� ��������������
	void AddVertexAndIndex(std::vector<VertexType>& vecVertex, std::vector<int>& vecIndex, const VertexType& vert);
};

//���������� ������, OBJ ��������� (������ ����� �������� �� ���������� �������)
struct StaticModelObjType {
        int countOfDetails;										// ���������� ������� � ������
        int countOfMaterials;									// ���������� ���������� � ������
        std::vector<MaterialInfoType> m_Materials;				// ������ ����������
        std::vector<DetailObjClass> m_Details;					// ������ ������� ��� ����� ������
};

// �����, ����������� ���� ������� OBJ
class ObjClass {
public:
	ObjClass();
    // �������� ������, ���� ��������� � �������.
    bool ImportObj(StaticModelObjType& pModel, std::string path);
private:
    // ������� ����������� ����, ������������ �� ImportObj()
    void ReadObjFile(StaticModelObjType& pModel);
	// ���������� � ReadObjFile() ���� ����� ���������� � 'm' ���������� ����� � ����������� 
	void ReadMtlFile(StaticModelObjType& pModel);
	// ���������� ������������� ���������
	void ReadUseMtl(StaticModelObjType& pModel);
    // ���������� � ReadObjFile() ���� ����� ���������� � 'v'
    void ReadVertexInfo();
    // ���������� � ReadObjFile() ���� ����� ���������� � 'f'
    void ReadFaceInfo();
    // ���������� ����� �������� ���������� ���������
    void FillInObjectInfo(StaticModelObjType& pModel);
	void CreatFace(std::vector<VertexIndex> in, std::vector<FaceType>& out);
    // ��������� �� ����� ���������� �����
	std::ifstream _file;
	// ��������� �� ����� ���������� ������
	std::istringstream _stream;
	// ���� �� �������� ����� 
	std::string _path;
    // STL vector, ���������� ������ ������
    std::vector<D3DXVECTOR3>  m_Vertex;
    // STL vector, ���������� ������ UV ���������
    std::vector<D3DXVECTOR2>  m_TextureCoords;
	// STL vector, ���������� ������ ��������
    std::vector<D3DXVECTOR3>  m_Normal;
	// STL vector, ���������� ������ ���������
    std::vector<FaceType> m_Faces;
	std::string lastMaterial;
    // ������� ���, ����� �� ������ ���������� ����������
    bool m_bDetailHasUV;
	// ������� ���, ����� �� ������ �������
	bool m_bDetailHasNormal;
    // ������� ���, ��� �� ������ ��� ��������� ������ ���������, ����� �� ����� ������ ��������� ��������
    bool m_bJustReadAFace;
};

#endif
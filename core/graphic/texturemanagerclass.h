////////////////////////////////////////////////////////////////////////////////
// Filename: texturemanagerclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _TEXTUREMANAGERCLASS_H_
#define _TEXTUREMANAGERCLASS_H_
//////////////
// INCLUDES //
//////////////
#include <iostream>
#include <map>
///////////////////////
// MY CLASS INCLUDES //
///////////////////////
#include "textureclass.h"

// ����� �������
class KeyTextureClass{
public:
	std::string normal;
	std::string ambient;
	std::string diffuse;
	std::string specular;
};
////// ����������
class PackTexture{
public:
	TextureClass* m_Texture;
	int countLink;
	std::string m_Path;
	PackTexture(void);
	~PackTexture(void);
	bool Shutdown(void);
};

class TextureManagerClass{
public:
	TextureManagerClass(void);
	bool Initialize(ID3D10Device*);

	void Shutdown(void);
	std::string Add(ID3D10Device* device, std::string);
	void Add(ID3D10Device* device, PackTexture inPackTexture);
	void Erase(std::string);
	unsigned int GetCountPack(void);
	unsigned int GetCountLink(std::string);
	std::string GetPathTexture(unsigned int);
	ID3D10ShaderResourceView* GetTextureByItem(unsigned int);
	ID3D10ShaderResourceView* GetTexture(std::string);
private:
	// ������ �� �����,���� � �����
	std::map<std::string,PackTexture> m_Pack;
};


#endif
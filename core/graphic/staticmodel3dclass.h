////////////////////////////////////////////////////////////////////////////////
// Filename: tStaticModel.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _STATICMODELCLASS_H_
#define _STATICMODELCLASS_H_
//////////////
// INCLUDES //
//////////////
#include <d3d10.h>
#include <d3dx10.h>
#include <vector>
// ��������� obj �������
#include "objclass.h"

class Detail3DClass{
public:
	Detail3DClass();
	~Detail3DClass();
	std::string name; // �������� ������
	int materialID; // ������ ���������
	// ���������� ������� ������
	D3DXVECTOR3 max; 
	D3DXVECTOR3 min;
	D3DXVECTOR3 center;
	// ������ ��������� �����
	float radius;

	// ���������� ������� � ������� � ��������� ����� (����� �� ����������, ���� ���� �����)
	bool Initialize(ID3D10Device*, const std::vector<VertexType> vecVertex, const std::vector<int> vecIndex);
	void Shutdown();
	// �������� � ������� ����������
	bool Render(ID3D10Device*);
	int GetIndexCount();
	int GetVertexCount();	
private:
	ID3D10Buffer *m_VertexBuffer;	// ����� ������
	ID3D10Buffer *m_IndexBuffer;	// ����� ��������
	VertexType* m_Vertex;			// �������
	unsigned long* m_Index;			// �������
	int countOfVertex;				// ���������� ������
	int countOfIndex;				// ���������� ��������
	// ������� ����� ������ � �������� directx
	bool InitializeBuffers(ID3D10Device*);
	// ���������� ������ �� ����������� �������
	bool RenderBuffers(ID3D10Device*);
	void ShutdownBuffers();
	void ReleaseDetail();
};


////////////////////////////////////////////////////////////////////////////////
// Class name: ModelClass
////////////////////////////////////////////////////////////////////////////////
class StaticModel3DClass{
public:
	StaticModel3DClass();
	~StaticModel3DClass();
	bool Initialize(ID3D10Device*, std::string path);
	bool Render(ID3D10Device*, int index);
	void Shutdown();
	int GetIndexCount(int index);
	int GetVertexCount(int index);	
	int GetCountDetail();
	int GetCountMaterial();
	void SetMaterialDetail(int index, MaterialInfoType material); 
	MaterialInfoType GetMaterialDetail(int index); // �������� index ������
	MaterialInfoType GetMaterial(int index); // Index ��������
	float GetRadiusDetail(int index);
	D3DXVECTOR3 GetCenterDetail(int index);
private:
	int countOfDetail;								// ���������� ������� � ������
	int countOfMaterial;							// ���������� ���������� � ������
	std::vector<MaterialInfoType> m_Materials;		// ������ ����������
	std::vector<Detail3DClass> m_Details;			// ������ ������� ��� ����� ������
	// �������� ������ �� ���� � ����������� .obj
	bool LoadModelObj(ID3D10Device* device, std::string);
	// �������� ������ �� ���� � ����������� .s3m
	bool LoadModelS3m(ID3D10Device* device, std::string);
	void fileReadString(std::ifstream& file, std::string& text);
};

#endif
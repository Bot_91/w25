////////////////////////////////////////////////////////////////////////////////
// Filename: cameraclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _CAMERACLASS_H_
#define _CAMERACLASS_H_


//////////////
// INCLUDES //
//////////////
#include <d3dx10math.h>


////////////////////////////////////////////////////////////////////////////////
// Class name: CameraClass
////////////////////////////////////////////////////////////////////////////////
class CameraClass
{
public:
	CameraClass();
	CameraClass(const CameraClass&);
	~CameraClass();

	void SetPosition(float, float, float);
	void SetRotation(float, float, float);
	void AddRotation(float, float, float);
	void AddPosition(float, float, float);
	void MovePosition(float); // �������� ����������� �������
	void ShiftPosition(float, float); // �������� ��������������� �������
	D3DXVECTOR3 GetPosition();
	D3DXVECTOR3 GetRotation();
	D3DXVECTOR3 GetView();
	// �������� ����������� ������� �������
	D3DXVECTOR3 GetCursorView(int,int,float, float, float);

	void Render();
	void GetViewMatrix(D3DXMATRIX&);
	void GetMatrixGUI(D3DXMATRIX&);
	void GetMatrixModel(D3DXMATRIX&);
private:
	float m_positionX, m_positionY, m_positionZ;
	float m_rotationX, m_rotationY, m_rotationZ;
	D3DXMATRIX m_viewMatrix;
};

#endif
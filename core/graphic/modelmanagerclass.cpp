// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
////////////////////////////////////////////////////////////////////////////////
// Filename: modelmanagerclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "modelmanagerclass.h"

PackModel::PackModel(void){
	m_Path = "NULL";
	countLink = 0;
}

PackModel::~PackModel(void){
}

bool PackModel::Shutdown(){
	countLink = 0;
	m_Path = "NULL";
	m_Model.Shutdown();
	return true;
}

ModelManagerClass::ModelManagerClass(void){

}
ModelManagerClass::~ModelManagerClass(void){
	Shutdown();
}
// ��������� ������ ������ �������� ������
bool ModelManagerClass::Initialize(ID3D10Device*){
	return true;
}
std::string ModelManagerClass::Add(ID3D10Device* device, 
								   TextureManagerClass* p_TextureManager, 
								   std::string path){
	bool result;
	std::map<std::string,PackModel>::iterator iter;
	// ���� ���� � ����������
	iter = m_Pack.find(path);
	// ���� ������ �� ����������� ������� ������ �� ������
	if (iter!=m_Pack.end()){
		iter->second.countLink +=1;
	// ����� ��������� ����� ������
	} else{
		// ��������� ���������
		PackModel inModel;
		inModel.countLink = 1;
		inModel.m_Path = path;
		result = inModel.m_Model.Initialize(device, path);
		// ��������� ����� ������
		if (result == false){
			return "ERROR";
		}
		m_Pack.insert( std::pair<std::string,PackModel>(path,inModel) );

		// ��������� �� ������ ������ ������ � ��������� � ��������
		for (int i = 0; i<inModel.m_Model.GetCountDetail(); i++) {
			MaterialInfoType material = inModel.m_Model.GetMaterialDetail(i);
			// ��������� ����� ��������
			p_TextureManager->Add(device, material.pathAmbient);
			p_TextureManager->Add(device, material.pathDiffuse);
			p_TextureManager->Add(device, material.pathSpecular);
		}

	}
	
	// ���������� ���� ��������
	return path;
  	
}
// �������� ���������� ������ �� ������ 
// ��� �� ���������� ������� ������
bool ModelManagerClass::Erase(std::string key){

	std::map<std::string,PackModel>::iterator iter;
	// ���� ���� � ����������
	iter = m_Pack.find(key);
	// ��������� ������� ������ �� ��������
	if (iter!=m_Pack.end()){
		iter->second.countLink -=1;
		// ������ �� �������� ����������� ������ ������
		if (iter->second.countLink==0){	
			// ������������ �����
			iter->second.Shutdown();
			m_Pack.erase(iter);
			return true;
		}
	}
	return false;
}
int ModelManagerClass::GetCoutDetail(std::string key){
	std::map<std::string,PackModel>::iterator iter;
	// ���� ������ �� �����
	iter = m_Pack.find(key);
	if (iter!=m_Pack.end()){
		// �������� �������� � ������� � ����� ��� ������ ��������
		return iter->second.m_Model.GetCountDetail();
	}
	return 0;
}
void ModelManagerClass::Shutdown(){
	std::map<std::string,PackModel>::iterator iter;
	for ( iter = m_Pack.begin(); iter != m_Pack.end(); ++iter ){
		// ������������ �����, ���-���� ����� ���� ������ ���������, ��� �� ���� ������� ��� ���� �������
		iter->second.Shutdown();
	}
	m_Pack.clear();
}
// ���� ������ � ����� ������ ���� ������ ������� ���������� �������
bool ModelManagerClass::Render(ID3D10Device* device, std::string key, int index){
	// Put the vertex and index buffers on the graphics pipeline to prepare them for drawing.
	std::map<std::string,PackModel>::iterator iter;
	bool result;
	// ���� ������ �� �����
	iter = m_Pack.find(key);
	if (iter!=m_Pack.end()){
		// �������� �������� � ������� � ����� ��� ������ ��������
		result = iter->second.m_Model.Render(device, index);
		return result;
	}
	return false;
}
// �������� �������� ������, � ���������� ����� ���������, �������� �� ��� �������
void ModelManagerClass::SetMaterialDetail(std::string key, int index, MaterialInfoType material){
	std::map<std::string,PackModel>::iterator iter;
	// ���� ������ �� �����
	iter = m_Pack.find(key);
	// �����
	if (iter!=m_Pack.end()){
		iter->second.m_Model.SetMaterialDetail(index, material);
	}

}
// ���������� ���������� ��������
int ModelManagerClass::GetIndexCount(std::string key, int index){
	// Put the vertex and index buffers on the graphics pipeline to prepare them for drawing.
	std::map<std::string,PackModel>::iterator iter;
	// ���� ������ �� �����
	iter = m_Pack.find(key);
	if (iter!=m_Pack.end()){
		return iter->second.m_Model.GetIndexCount(index);	
	}
	return 0;
}
// ���������� ���������� ��������� � ������ ������ ��� ������
int ModelManagerClass::GetVertexCount(std::string  key, int index){
	std::map<std::string,PackModel>::iterator iter;
	// ���� ������ �� �����
	iter = m_Pack.find(key);
	// ����� ����� ������ ���������� ���������� ��������� � ������
	if (iter!=m_Pack.end()){
		return iter->second.m_Model.GetVertexCount(index);	
	}
	return 0;
}
int ModelManagerClass::GetVertexCount(std::string  key) {
	std::map<std::string, PackModel>::iterator iter;
	// ���� ������ �� �����
	int result = 0;
	iter = m_Pack.find(key);
	// ����� ����� ������ ���������� ���������� ��������� � ������
	if (iter != m_Pack.end()) {
		for (int i = 0; i < iter->second.m_Model.GetCountDetail(); i++) {
			result += iter->second.m_Model.GetVertexCount(i);
		}
	}
	return result;
}
// ���������� ��������� �������
unsigned int  ModelManagerClass::GetCountPack(void){	
	return m_Pack.size();
}
// ���������� �������� � ������ ��������� �� ������� �����
unsigned int ModelManagerClass::GetCountLink(std::string key){
	std::map<std::string,PackModel>::iterator iter;
	// ���� ���� � ����������
	iter = m_Pack.find(key);
	if (iter!=m_Pack.end()){
		return iter->second.countLink;
	}
	return 0;
}
int ModelManagerClass::GetCountMaterial(std::string key){
	std::map<std::string,PackModel>::iterator iter;
	// ���� ���� � ����������
	iter = m_Pack.find(key);
	if (iter!=m_Pack.end()){
		return iter->second.m_Model.GetCountMaterial();
	}
	return 0;
}
MaterialInfoType ModelManagerClass::GetMaterial(std::string key,int index){
	std::map<std::string,PackModel>::iterator iter;
	// ���� ���� � ����������
	iter = m_Pack.find(key);
	if (iter!=m_Pack.end()){
		return iter->second.m_Model.GetMaterial(index);
	}
	MaterialInfoType result;
	return result;

}
MaterialInfoType ModelManagerClass::GetMaterialDetail(std::string key,int index){
	std::map<std::string,PackModel>::iterator iter;
	// ���� ���� � ����������
	iter = m_Pack.find(key);
	if (iter!=m_Pack.end()){
		return iter->second.m_Model.GetMaterialDetail(index);
	}
	MaterialInfoType result;
	return result;
}
float ModelManagerClass::GetRadiusDetail(std::string key,int index){
	std::map<std::string,PackModel>::iterator iter;
	// ���� ���� � ����������
	iter = m_Pack.find(key);
	if (iter!=m_Pack.end()){
		return iter->second.m_Model.GetRadiusDetail(index);
	}
	return 0.0f;
}
D3DXVECTOR3 ModelManagerClass::GetCenterDetail(std::string key,int index){
	std::map<std::string,PackModel>::iterator iter;
	// ���� ���� � ����������
	iter = m_Pack.find(key);
	if (iter!=m_Pack.end()){
		return iter->second.m_Model.GetCenterDetail(index);
	}
	return D3DXVECTOR3(0.0f, 0.0f, 0.0f);
}
////////////////////////////////////////////////////////////////////////////////
// Filename: vertexmanagerclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _MODELMANAGERCLASS_H_
#define _MODELMANAGERCLASS_H_


//////////////
// INCLUDES //
//////////////
#include "staticmodel3dclass.h"
#include "texturemanagerclass.h"
#include <map>
using namespace std;

////////////////////////////////////////////////////////////////////////////////
// Class name: PackModel
////////////////////////////////////////////////////////////////////////////////

class PackModel{
public:
	PackModel(void);
	~PackModel(void);
	StaticModel3DClass m_Model;
	std::string m_Path; // ���� �� ������, ����
	int countLink; // ���������� ������ �� ������
	bool Shutdown();
};

////////////////////////////////////////////////////////////////////////////////
// Class name: ModelManagerClass
////////////////////////////////////////////////////////////////////////////////
class ModelManagerClass
{
public:
	ModelManagerClass(void);
	~ModelManagerClass(void);
	void Shutdown(void);
	bool Initialize(ID3D10Device*);
	std::string Add(ID3D10Device*, TextureManagerClass*, std::string);
	bool Erase(std::string);
	bool Render(ID3D10Device*, std::string, int index);
	void SetMaterialDetail(std::string key, int index, MaterialInfoType material);
	int GetIndexCount(std::string, int index);
	int GetVertexCount(std::string, int index);
	int GetVertexCount(std::string  key);
	int GetCoutDetail(std::string);
	int GetCountMaterial(std::string);
	MaterialInfoType GetMaterial(std::string,int index);
	MaterialInfoType GetMaterialDetail(std::string key, int index);
	unsigned int GetCountPack(void);
	unsigned int GetCountLink(std::string);
	float GetRadiusDetail(std::string, int index);	
	D3DXVECTOR3 GetCenterDetail(std::string, int index);
private:
	// ������ �� �����,���� � �����
	std::map<std::string,PackModel> m_Pack;
};

#endif


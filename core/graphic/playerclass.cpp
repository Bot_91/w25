// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
////////////////////////////////////////////////////////////////////////////////
// Filename: playerclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "playerclass.h"


PlayerClass::PlayerClass() {
	speed = 10.0f;
	p_Camera = 0;
	flagRun = 0;
	this->rotation = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
}
void PlayerClass::Initialize(CameraClass *camera, std::string pathModel) {
	this->p_Camera = camera;
	keyModel = pathModel;
}
void PlayerClass::SetMoveTo(D3DXVECTOR3 marker) {
	markerMove = marker;
	flagRun = true;
}
// ����������� ����� ������
bool PlayerClass::GetDirection(D3DXVECTOR3 &direction) {
	D3DXMATRIX rotationMatrix;
	direction = D3DXVECTOR3(0.0f, 0.0f, 1.0f);	
	D3DXVec3Normalize(&direction, &direction);
	// Create the rotation matrix from the yaw, pitch, and roll values.
	D3DXMatrixRotationYawPitchRoll(&rotationMatrix, this->rotation.y, 0.0f, 0.0f);
	// Transform the lookAt and up vector by the rotation matrix so the view is correctly rotated at the origin.
	D3DXVec3TransformCoord(&direction, &direction, &rotationMatrix);
	D3DXVec3Normalize(&direction, &direction);
	return true;
}
// ���������� �� ���
bool PlayerClass::MoveStep(D3DXVECTOR3 step) {
	
	return true;
}
bool PlayerClass::MoveTo(float deltaTime) {
	
	if (flagRun == true) {
		D3DXVECTOR3 direction = markerMove - position;
		if (speed*deltaTime*4 >= D3DXVec3Length(&direction)) {
			//position = markerMove;
			flagRun = false;
		}else {
			// ��������� ������ � ������� �������
			D3DXVec3Normalize(&direction, &direction);
			position = position + direction*(speed*deltaTime);
			// ������� ������ �� ����������� ��������
			D3DXVECTOR3 normalDirection(0.0f, 0.0f, 1.0f);
			//ax*by - bx*ay, ax*bx + ay*by
			rotation.y = atan2(direction.x*normalDirection.z, direction.z*normalDirection.z);

		}

	}
	return true;
}

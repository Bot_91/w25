// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
////////////////////////////////////////////////////////////////////////////////
// Filename: objectmanagerclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "objectmanagerclass.h"

PackObject::PackObject(){

	//key_Texture.ambient = "NULL";
	//key_Texture.diffuse = "NULL";
	//key_Texture.specular = "NULL";
	//key_Texture.normal = "NULL";
	key_Model = "NULL";
	valueHP = 100.0f;
	key_Shader = -1;
	sizeModel = 10.0f;
	position.x = 0.0f;
	position.y = 0.0f;
	position.z = 0.0f;
	sizeModel = 0.0f;
	angleX = 0.0f;	
	angleY = 0.0f;
}
bool ObjectManagerClass::AddObject(PackObject object){
	m_Object.push_back(object);
	return true;
}
// ��������� ������� �� �����, � ������ �� ������ � ��������
bool ObjectManagerClass::LoadObjectData(ID3D10Device* device, ModelManagerClass* p_ModelManager,
										TextureManagerClass* p_TextureManager){										
	return false;
}
// ������� ������ �� �������, � ������ �� ������ � ��������
bool ObjectManagerClass::DeleteObject(ModelManagerClass* p_ModelManager, 
									  TextureManagerClass* p_TextureManager, int idObject){
	// ���� ������, ���� �� ����� � �������
	 std::string key = m_Object[idObject].key_Model;
	 // ������� ������
	 if (p_ModelManager->Erase(key)) {
		 // ������� ��� �������� �������������� ������
		 for (int i = 0; i < p_ModelManager->GetCoutDetail(key); i++) {
			 MaterialInfoType material = p_ModelManager->GetMaterialDetail(key, i);
			 p_TextureManager->Erase(material.pathAmbient);
			 p_TextureManager->Erase(material.pathDiffuse);
			 p_TextureManager->Erase(material.pathSpecular);
		 }
	 }
	// ���������� ������ �� ���������
	m_Object[idObject].key_Model = "NULL";

	m_Object[idObject].key_Shader = -1;

	m_Object[idObject].angleY = 0.0f;
	m_Object[idObject].angleX = 0.0f;

	m_Object[idObject].position.x = 0.0f;
	m_Object[idObject].position.y = 0.0f;
	m_Object[idObject].position.z = 0.0f;
	// �� ������� ������, � ����� ����

	return true;	
}
void ObjectManagerClass::Shutdown(ModelManagerClass* p_ModelManager, TextureManagerClass* p_TextureManager){

	for (unsigned int i=0; i<m_Object.size(); i++){
		this->DeleteObject(p_ModelManager, p_TextureManager, i);
	}
	m_Object.clear();

}
// ���������� ���������� ��������
int ObjectManagerClass::GetCountObject(){
	return m_Object.size();
}
int ObjectManagerClass::GetIndexIntersection(D3DXVECTOR3 RayPosition,D3DXVECTOR3 RayDirection) {
	int result = -1;

	for (unsigned int i = 0; i < m_Object.size(); i++) {
		D3DXVECTOR3 spherePosition = m_Object[i].position;
		float sphereRadius = 4.0f; // ��� ������ �������
		//m_Object[i].position;
		// ������� ����������� ���� �� ������
		D3DXVECTOR3 vect(RayPosition.x - spherePosition.x,
			RayPosition.y - spherePosition.y,
			RayPosition.z - spherePosition.z);
		// at^2+bt+c = 0
		float a = RayDirection.x*RayDirection.x +
			RayDirection.y*RayDirection.y +
			RayDirection.z*RayDirection.z;
		float b = 2 * (vect.x*RayDirection.x +
			vect.y*RayDirection.y +
			vect.z*RayDirection.z);
		float c = pow(vect.x, 2) + pow(vect.y, 2) + pow(vect.z, 2) - pow(sphereRadius, 2);
		float D = pow(b, 2) - 4.0f * a*c;
		// ����� �����������
		if (D > 0) {
			float intersect = (-b + sqrt(D)) / (2.0f*a);
			// ����� ����������� ����� �� ����
			if (intersect >= 0) {
				result = i;
			}
		}
	}
	// ������� ������ �������
	return result;

}

// ���������� ID ������ �������
std::string ObjectManagerClass::GetModel(int item){
	if (item < (int)m_Object.size() && item >= 0){
		return m_Object[item].key_Model;	
	} 
	return  "ERROR";
}
// ���������� ���������� �������
D3DXVECTOR3 ObjectManagerClass::GetPosition(int item){
	if (item < (int)m_Object.size() && item >= 0){
		return m_Object[item].position;
	}
	return D3DXVECTOR3(0.0f,0.0f,0.0f);
}
// ���������� ���� �������� ������� �� ��� Y
float ObjectManagerClass::GetAngleY(int item){
	if (item < (int)m_Object.size() && item >= 0){
		return m_Object[item].angleY;
	}
	return 0.0f;
}
// ���������� ���� �������� ������� �� ��� X
float ObjectManagerClass::GetAngleX(int item){
	if (item < (int)m_Object.size() && item >= 0){
		return m_Object[item].angleX;
	}
	return 0.0f;
}

PackObject ObjectManagerClass::GetPackObject(std::string name) {
	for (unsigned int i = 0; i < m_Object.size(); i++) {
		if (m_Object[i].Name == name) {
			return m_Object[i];
		}
	}
	PackObject result;
	return result;
}
PackObject ObjectManagerClass::GetPackObject(int item) {
	if (item < (int)m_Object.size() && item >= 0) {
		return m_Object[item];	
	}
	return PackObject();
}
void ObjectManagerClass::SetPackObject(PackObject inObject) {
	for (unsigned int i = 0; i < m_Object.size(); i++) {
		if (m_Object[i].Name == inObject.Name) {
			m_Object[i] = inObject;
		}
	}
}
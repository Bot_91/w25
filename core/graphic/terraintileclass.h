////////////////////////////////////////////////////////////////////////////////
// Filename: TerrainTileClass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _TerrainTileClass_H_
#define _TerrainTileClass_H_


//////////////
// INCLUDES //
//////////////
#include <d3d10.h>
#include <d3dx10math.h>
#include <stdio.h>
#include <vector>

///////////////////////
// MY CLASS INCLUDES //
///////////////////////
#include "textureclass.h"
#include "objclass.h"
#include "mathsgraph.h"

////////////////////////////////////////////////////////////////////////////////
// Class name: TerrainTileClass
////////////////////////////////////////////////////////////////////////////////
class TerrainPolygonClass{
public:
	TerrainPolygonClass();
	virtual ~TerrainPolygonClass();
	void Shutdown();
	void Initialize(ID3D10Device* device, std::vector<D3DXVECTOR3>);
	void Render(ID3D10Device*);
	LONG m_VertexCount;
	LONG m_IndexCount;
	bool flagCreat;

	void RenderBuffers(ID3D10Device*);
	virtual bool InitializeBuffers(ID3D10Device*, std::vector <VertexType>);
	void ReleaseBuffers();
	ID3D10Buffer *m_VertexBuffer;
	ID3D10Buffer *m_IndexBuffer;
};

class TerrainRoundClass: public TerrainPolygonClass{
public:
	TerrainRoundClass();
private:
	bool InitializeBuffers(ID3D10Device*, std::vector <VertexType>);

};

enum class alter_argument { DEFAULT, POSSITION, MASK1, MASK2, MASK3, MASK4 };
class TerrainTileClass{
private:
	struct HeightMapType{ 
		D3DXVECTOR3 position;
		D3DXVECTOR3 normal;
		D3DXVECTOR3 tangent;
		D3DXVECTOR3 binormal;
	};
	struct TempVertexType{
		float x, y, z;
		float tu, tv;
		float nx, ny, nz;
	};
public:
	struct VertexType{
		D3DXVECTOR3 position;
		D3DXVECTOR2 texture;
	    D3DXVECTOR3 normal;
		D3DXVECTOR3 tangent;
		D3DXVECTOR3 binormal;
		D3DXVECTOR4 mask;
	};
	
public:
	bool flagInitialize;
	bool flagModel;
	int stateRender;
	TerrainTileClass();
	TerrainTileClass(const TerrainTileClass&);
	~TerrainTileClass();
	void SetStateRender(int);
	bool SaveTerrainMap(std::ofstream& file);
	bool LoadTerrainMap(std::ifstream& file);
	bool Initialize(ID3D10Device*);
	bool CreatePlane(float X, float Y, int Width, int Height);
	void Shutdown();
	void Render(ID3D10Device*);
	void RenderRound(ID3D10Device*);
	void RenderLine(ID3D10Device*);
	int GetIndexCount();
	int GetCountMaterial();
	bool TerrainTileClass::GetIntersects(D3DXVECTOR3, D3DXVECTOR3, D3DXVECTOR3&);
	bool GetHeight(D3DXVECTOR3, float&, D3DXVECTOR3&);
	bool GetHeight(D3DXVECTOR3, float&);
	void AlterPoint(int, float, alter_argument);
	// �������� ���������� ��������������� �� �����������
	void CreateRound(ID3D10Device*, D3DXVECTOR3, int radius);
	void CreateLine(ID3D10Device*, D3DXVECTOR3, D3DXVECTOR3);
	bool GetIndexVertex(D3DXVECTOR3, int&);
	int GetIndexRoundCount();
	int GetIndexLineCount();
	TerrainTileClass::VertexType GetVertex(D3DXVECTOR3 point);
	bool SetCamputeNodeMeanNormalTangentBinormal(TerrainTileClass::VertexType[5], int index);
	int GetMaterial(int);
	LONG m_Width;
	LONG m_Height;
private:

	// ��� ��������� ������ ������� ������������� ���������� � �� ������
	//TerrainRoundClass m_Round;
	//TerrainPolygonClass m_Line;
	


	bool InitializeBuffers(ID3D10Device*);
	void CamputeMeanNormal(int index[10]);
	void ReleaseBuffers();
	void ReleaseModel();
	void RenderBuffers(ID3D10Device*);

private:
	std::vector<int> m_MaterialIndex;
	int m_MaterialIndexCount;
	LONG m_VertexCount;
	LONG m_IndexCount;	
	TerrainTileClass::VertexType* m_Model;
	ID3D10Buffer *m_VertexBuffer;
	ID3D10Buffer *m_IndexBuffer;
};

#endif
// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
////////////////////////////////////////////////////////////////////////////////
// Filename: objclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "objclass.h"
MaskInfoType::MaskInfoType(){
	pathMask = "NULL";
}
MaterialInfoType::MaterialInfoType(){
	nameMaterial = "NULL";
	pathAmbient = "NULL";
	pathDiffuse = "NULL";
	pathNormal = "NULL";
	pathSpecular = "NULL";
	Ka = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	Kd = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	Ks = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	d = 0.0f;
	Ns = 0.0f;
}

ObjClass::ObjClass(){
	lastMaterial = "NULL";
	m_bDetailHasUV = 0;
	m_bDetailHasNormal = 0;
	m_bJustReadAFace = 0;
}
// ������� ��������� ���� .obj � ��������� ���������� �� ���������� ����� �����
bool ObjClass::ImportObj(StaticModelObjType& pModel, std::string path){

	m_bDetailHasUV = false;
	// ������� ���, ����� �� ������ �������
	m_bDetailHasNormal = false;
    // ������� ���, ��� �� ������ ��� ��������� ������ ���������, ����� �� ����� ������ ��������� ��������
    m_bJustReadAFace = false;;
	pModel.countOfDetails = 0;
	pModel.countOfMaterials = 0;

	_file.open(path);

	if(_file.fail()){
		return false;
	}
	// ���������� ���� �� �������� ����� 
	// ���� ���� '/' � ����� ������
	for (int i = path.length()-1; i>0; i--){
		// �����
		if (path[i]=='/'){
			// �������� ������ ���� �� �������� ����� ������ �� ������ '/'
			_path = std::string(path, 0, i+1);	
			break;
		}
	}

	// Now that we have a valid file and it's open, let's read in the info!
	ReadObjFile(pModel);

	// Now that we have the file read in, let's compute the vertex normals for lighting
	//ComputeNormals(pModel);

	// Close the .obj file that we opened
	_file.close();

	// Return a success!
	return true;
}
//�������� ������� ������ .obj �����
void ObjClass::ReadObjFile(StaticModelObjType& pModel){
	char ch = 0;
	std::string line;

	while(std::getline(_file, line)) { //������ ���������
		_stream.clear();
		_stream.str(line);
		// ������ ������ ������ ������� ������ �����
		_stream >> ch;
		switch(ch){
			// ���������, �� 'm' �� ��� mtllib ���� ����������
			case 'm':
				// ��������� ��� ���������
				ReadMtlFile(pModel);
			break;
			// ���������, �� 'u' �� ��� usemtl ��� ��������� ��� ����������� ������
			case 'u':
				// ���������� ����� �������� ����� ����������� ��� �������
				ReadUseMtl(pModel);
			break;
			// ���������, �� 'v' �� ��� (����� ���� �������/�������/�����. �����.)
			case 'v': 	          
				// ���� �� ������ ��� ������ ���������� � ��������, � ������ ������ �������,
				// ������ �� ������� � ���������� �������, � ����� ��������� ������ �����������.
				if(m_bJustReadAFace==true) {
				// ��������� ������ ���������� ������� � ��������� ������
					FillInObjectInfo(pModel);
                }
				// �������������� ������, ��� �� ������, ��� ��� vertex ("v"), normal ("vn"), ��� UV ���������� ("vt")
				ReadVertexInfo();
                break;
			// ���� ������ ������ -'f', ��� ������ ��������� �������
			case 'f':             
				// ����� ������� ���������� � ��������. ������ ��������� �����
				// ��������� ��� ������� ������, ��� ��� � ���������� ����������,
				// ���� ��� ���� � �������.
				ReadFaceInfo();
                break;
			// ���� �������� ������ ����� ������ - ��� ������ ������, ������ �� ������.
            case '\n':
                break;
            default:				
				break;
		}
	}
    // ������ ��������� ��������� ����������� ������
    FillInObjectInfo(pModel);
}
// ���������� ���� ����������
void ObjClass::ReadMtlFile(StaticModelObjType& pModel){
	// ��� ����������
	std::string marker, filename, line;
	// ��������� ������ 'mtllib' � �������� �����
	_stream >> marker >> filename;
	// ���������� ���� �� �����, ��� ���� �� �������� � ��� �����
	filename = _path+filename;
	// ���������� ���� �� �����
	//path = _path + filename.c_str();
	std::ifstream mtlFile;
	mtlFile.open(filename);
	if(mtlFile.fail()){
		return;
	}
	bool bReadAMaterial = false;
	MaterialInfoType readMaterial;
	// ����� ������
	std::istringstream mtlStream;
	std::string readWord;
	//������ �� ����� ����� ���������
	while(std::getline(mtlFile, line)) {
		// ����������� ������ ������ � ������ ������
		mtlStream.clear();
		mtlStream.str(line);
		// ��������� ������
		mtlStream >> readWord;
		if (readWord == "newmtl"){
			// ��������� ������� ���������� ����������
			pModel.countOfMaterials+=1;
			// �������� �� ����������� ��������� ���������
			pModel.m_Materials.push_back(readMaterial);
			// ����� �������� ��������� ������� ������� ������������ ����������
			mtlStream >> pModel.m_Materials.back().nameMaterial;	
		}
		// ��������� �������� ���������
		// Ambient-�������� ��������� 
		if (readWord == "Ka"){
			mtlStream >> pModel.m_Materials.back().Ka.x >> pModel.m_Materials.back().Ka.y >> pModel.m_Materials.back().Ka.z;
		}
		// Diffuse-�������� ���������
		if (readWord == "Kd"){
			mtlStream >> pModel.m_Materials.back().Kd.x >> pModel.m_Materials.back().Kd.y >> pModel.m_Materials.back().Kd.z;
		}
		// Specular-�������� ���������
		if (readWord == "Ks"){
			mtlStream >> pModel.m_Materials.back().Ks.x >> pModel.m_Materials.back().Ks.y >> pModel.m_Materials.back().Ks.z;
		}
		// ������������ 0..1 
		if (readWord == "d"){
			mtlStream >> pModel.m_Materials.back().d;
		}
		// ���� ��������
		if (readWord == "Ns"){
			mtlStream >> pModel.m_Materials.back().Ns;
		}
		// ���������� �������� ���������
		// �������� Ambient 
		if (readWord == "map_Ka"){
			std::string pathTexture;
			mtlStream >> pathTexture;
			pModel.m_Materials.back().pathAmbient = _path + pathTexture;
		}
		// �������� Diffuse 
		if (readWord == "map_Kd"){
			std::string pathTexture;
			mtlStream >> pathTexture;
			pModel.m_Materials.back().pathDiffuse = _path + pathTexture;
		}
		// �������� Specular
		if (readWord == "map_Ks"){
			std::string pathTexture;
			mtlStream >> pathTexture;
			pModel.m_Materials.back().pathSpecular = _path + pathTexture;
		}
		
	}
	// ��������� ���� � ����������� 
	mtlFile.close();
	return;

}
void ObjClass::ReadUseMtl(StaticModelObjType& pModel){
	// ��� ����������
	std::string marker;
	// ��������� ������ 'semtl' � �������� ���������
	_stream >> marker >> lastMaterial;
}
//	��� ������� ������ ���������� � �������� ("v" ������� : "vt" UVCoord)
// ������������� ���������� ���������
void ObjClass::ReadVertexInfo(){
	D3DXVECTOR3 vNewVertex;
	D3DXVECTOR2 vNewTexCoord;
	D3DXVECTOR3 vNewNormal;
	char ch;
	float UV[3] = { 0.0f,0.0f,0.0f };
	int number=0;
    // ������ ��������� ������ � ����� , ����� ������� ��� ��� vertice/normal/UVCoord
    _stream.get(ch);
		//switch
	switch (ch){
		// ���� ��������� ������ ��� ������ �� ��� ������� ("v")
		case ' ':
			// Here we read in a vertice.  The format is "v x y z"
			_stream >> vNewVertex.x >> vNewVertex.y >> vNewVertex.z;
            // Add a new vertice to our list
            m_Vertex.push_back(vNewVertex);
			m_bDetailHasUV = false;	
			m_bDetailHasNormal = false;
			break;
		// If we get a 't' then it must be a texture coordinate ("vt")
		case 't':
			// Here we read in a texture coordinate.  The format is "vt u v"
			// ����� ���� 3 ����������
			while (_stream.eof() != true){
				_stream >> UV[number];
				if (number<2){number++;}
			}
			vNewTexCoord.x = UV[0];
			vNewTexCoord.y = 1-UV[1];
			m_TextureCoords.push_back(vNewTexCoord);
			//vNewTexCoord.x = 1 - vNewTexCoord.x; 
			//vNewTexCoord.y = 1 - vNewTexCoord.y;
			// Add a new texture coordinate to our list
			
			// Set the flag that tells us this object has texture coordinates.
			// Now we know that the face information will list the vertice AND UV index.
            // For example, ("f 1 3 2" verses "f 1/1 2/2 3/3")
            m_bDetailHasUV = true;	
			break;
		// Otherwise it's probably a normal so we don't care ("vn")
		case 'n':
			// Here we read in a vertice.  The format is "v x y z"
           _stream >> vNewNormal.x >> vNewNormal.y >> vNewNormal.z;
			// Add a new texture coordinate to our list
            m_Normal.push_back(vNewNormal);
			// ������������� ���� ����������� ��������
			m_bDetailHasNormal = true;
			break;
		default:
			break;
	}
      
}
// ��������� ����������� �� �������
void ObjClass::CreatFace(std::vector<VertexIndex> in, std::vector<FaceType>& out){
	FaceType buff;
	// ���������� ��������� � ��������������
	int count = in.size()-2;
	// ������ �����������
	buff.vertexIndex[0] =  in[0].vertexIndex;
	buff.normalIndex[0] =  in[0].normalIndex;
	buff.textureIndex[0] = in[0].textureIndex;
	buff.vertexIndex[1] =  in[1].vertexIndex;
	buff.normalIndex[1] =  in[1].normalIndex;
	buff.textureIndex[1] = in[1].textureIndex;
	buff.vertexIndex[2] =  in[2].vertexIndex;	
	buff.normalIndex[2] =  in[2].normalIndex;
	buff.textureIndex[2] = in[2].textureIndex;
	out.push_back(buff);
	int front = 2;
	int midlle = 3;
	int back = 0;
	// ���������
	for (int i=1; i<count; i++){
		buff.vertexIndex[0] =  in[front].vertexIndex;
		buff.normalIndex[0] =  in[front].normalIndex;
		buff.textureIndex[0] = in[front].textureIndex;
		buff.vertexIndex[1] =  in[midlle].vertexIndex;
		buff.normalIndex[1] =  in[midlle].normalIndex;
		buff.textureIndex[1] = in[midlle].textureIndex;
		buff.vertexIndex[2] =  in[back].vertexIndex;	
		buff.normalIndex[2] =  in[back].normalIndex;
		buff.textureIndex[2] = in[back].textureIndex;
		midlle += 1;
		if ( i%2 == 1){
			front = 0;
			back = 2;
		} else {
			front = 2;
			back = 0;
		}
		out.push_back(buff);	
	}
// 1 2 3 4 5 6
// // ��������� ��������
// 1 2 3 
// 3 4 1 
// 1 5 3
// 3 6 1
}
//  ������� ���������� ���������� �������� ("f")
void ObjClass::ReadFaceInfo(){
	FaceType newFace = {0};
	char strLine[255] = {0};
	// ��� ���������� ������� ����� ��������� �� ����� ������� � ����������� ��������
	std::vector<VertexIndex> vec_index;
	// ������� ������ ���������� � ��������� �������.
	// ��� ���������� - 3� �����, ������������ �������, � UV ����������,
	// ���� �� ������ �������� ��������.
	// ���� ������ ����� ���������� ����������, ������ ������ �����
	// �����: "f v1/uv1 v2/uv2 v3/uv3"
	// ����� �����: "f v1 v2 v3"
	// ��������! ������ �����������, ��� ��������� 1 �� ��������, ��� ���
	// ������� � c++ ���������� � 0, � ������� � .obj ���������� � 1.
	char b; // �����������
	// ������� �� ��������
	// ����� ������ ��������: �������/��������/�������
	if( (m_bDetailHasUV == true) && (m_bDetailHasNormal == true) ){
		// ��������� �� ����� ������
		while (_stream.eof() != true){
			VertexIndex indexVertex={0}; 
			// Here we read in the object's vertex, texture coordinate and normal indices.
			// Here is the format: "f vertexIndex1/coordIndex1/normalIndex1 ... vertexIndex3/coordIndex3/normalIndex3 "	
			_stream >>  indexVertex.vertexIndex >> b >> indexVertex.textureIndex >> b >> indexVertex.normalIndex;
			if (indexVertex.vertexIndex != 0){
				vec_index.push_back(indexVertex);
			}
		}
	}
	//f -3515/-7835/-2346 -3514/-7834/-2346 -3513/-7833/-2346 
	//f -3513/-7833/-2346 -3512/-7832/-2346 -3515/-7835/-2346 
	// ����� ������ ��������: �������/��������
    if( (m_bDetailHasUV == true) && (m_bDetailHasNormal == false) ){
		// ��������� �� ����� ������
		while (_stream.eof() != true){
			VertexIndex indexVertex={0}; 
			// Here we read in the object's vertex and texture coordinate indices.
			// Here is the format: "f vertexIndex1/coordIndex1 vertexIndex2/coordIndex2 vertexIndex3/coordIndex3"
			_stream >>  indexVertex.vertexIndex >> b >> indexVertex.textureIndex;
			if (indexVertex.vertexIndex != 0){
				vec_index.push_back(indexVertex);
			}
		}

	}
	// ����� ������ ��������: �������//�������
    if(  (m_bDetailHasUV == false) && (m_bDetailHasNormal == true) ){
		// ��������� �� ����� ������
		while (_stream.eof() != true){
			VertexIndex indexVertex={0}; 
			// Here we read in just the object's vertex indices.
			// Here is the format: "f vertexIndex1//normalIndex1 vertexIndex2//normalIndex2 vertexIndex3//normalIndex3"
			_stream >> indexVertex.vertexIndex >> b >> b >> indexVertex.normalIndex;
			if (indexVertex.vertexIndex != 0){
				vec_index.push_back(indexVertex);
			}
		}
    }

	// ����� ������ ��������: �������
	if(  (m_bDetailHasUV == false) && (m_bDetailHasNormal == false) ){
		// ��������� �� ����� ������
		while (_stream.eof() != true){
			VertexIndex indexVertex={0}; 
			// Here we read in just the object's vertex indices.
			// Here is the format: "f vertexIndex1 vertexIndex2 vertexIndex3"
			_stream >> indexVertex.vertexIndex;   
			if (indexVertex.vertexIndex != 0){
				vec_index.push_back(indexVertex);
			}
		}
	}	                        
	// ��������� �������� � ������, ���� ����� �� ���������
	std::vector<FaceType> outFace;
	// �������� �������������� �������� �� ������ ��������� �� ��������
	if ( vec_index.size() >= 3){
		CreatFace(vec_index, outFace);
	}
	for (unsigned int i=0; i<outFace.size(); i++){
		m_Faces.push_back(outFace[i]);
	}
	
	// ������������� ���� ���� � TRUE, ����� �����, ��� ������ ��� ������ �������. ���� 
	// ����� ����� �������� ������� - ������, �� ������� � ���������� ������� � �����
	// ��������� ����.
	m_bJustReadAFace = true;
}

// ���������� ������ � ������ ������
void ObjClass::FillInObjectInfo(StaticModelObjType& pModel){
	DetailObjClass newObject;
	int vertexOffset = 0;
    int textureOffset = 0;
	int normalOffset = 0;
    // If we get here then we just finished reading in an object
    // and need to increase the object count.
	// ������� ��������� ��� ��������� ��������� � ����� ������ � ������ ������

	// Now that we have our list's full of information, we can get the size
	// of these lists by calling size() from our vectors.  That is one of the
	// wonderful things that the Standard Template Library offers us.  Now you
	// never need to write a link list or constantly call malloc()/new.
	
	// �������� �������
	newObject.m_Vertex = m_Vertex;
	// �������� ���������� ����������
	newObject.m_TexVertex = m_TextureCoords;
	// �������� �������
	newObject.m_Normals = m_Normal;
	// �������� ������� ��������
	newObject.m_Faces = m_Faces;

	newObject.countOfFaces = m_Faces.size();
	newObject.countOfVertex = m_Vertex.size();
	newObject.countTexVertex = m_TextureCoords.size();
	newObject.countOfNormal = m_Normal.size();      
	// �������� ��� ������������� ����������
	if ( newObject.m_Faces[0].vertexIndex[0] >= 1){
		vertexOffset = 1;
		textureOffset = 1;
		normalOffset = 1;
	}
	// ������ ������
	/*if (pModel.m_Details.size() > 0){
		vertexOffset = newObject.m_Faces[0].vertexIndex[0];
		if(newObject.countTexVertex > 0) {		
			textureOffset = newObject.m_Faces[0].textureIndex[0];
		}
		if(newObject.countOfNormal > 0) {		 
			normalOffset = newObject.m_Faces[0].normalIndex[0];
		}
	}*/
	// �������� ��� ������������� ����������
	if ( newObject.m_Faces[0].vertexIndex[0] < -newObject.countOfVertex){
		vertexOffset = 0;
		if(newObject.countTexVertex > 0) {
			textureOffset = 0;
		}
		if(newObject.countOfNormal > 0) {
			normalOffset = 0;
		}
	}
	// ������ ������
	if (pModel.m_Details.size() > 0){
		vertexOffset = newObject.m_Faces[0].vertexIndex[0];
		if(newObject.countTexVertex > 0) {		
			textureOffset =  newObject.m_Faces[0].textureIndex[0];
		}
		if(newObject.countOfNormal > 0) {		 
			normalOffset =  newObject.m_Faces[0].normalIndex[0];
		}
	}
              
	// ��������� �� ���� ���������
	for(int i = 0; i < newObject.countOfFaces; i++){
		// / ����������� ������� ������� �� ���������� ������ � ������ ������ ������
		newObject.m_Faces[i] = m_Faces[i];
		// Because the face indices start at 1, we need to minus 1 from them due
		// to arrays being zero based.  This is VERY important!
		// ������� ������ �������� �� ������
		////////////////////////////////////////////////////////
		for(int j = 0; j < 3; j++){
			// ������������� ������� ������ ������� ����������� ������
			// �������
			if (newObject.m_Faces[i].vertexIndex[j] < 0){
				//newObject.m_Faces[i].vertexIndex[j] -= vertexOffset;
				newObject.m_Faces[i].vertexIndex[j] = newObject.countOfVertex + newObject.m_Faces[i].vertexIndex[j]-vertexOffset;
			} else {
				newObject.m_Faces[i].vertexIndex[j] -= vertexOffset;
			}
			// ��������
			if (newObject.m_Faces[i].textureIndex[j] < 0){
				//newObject.m_Faces[i].textureIndex[j] -= textureOffset;
				newObject.m_Faces[i].textureIndex[j] = newObject.countTexVertex + newObject.m_Faces[i].textureIndex[j]-textureOffset;
			} else {
				newObject.m_Faces[i].textureIndex[j] -= textureOffset;
			}
			// �������
			if (newObject.m_Faces[i].normalIndex[j] < 0){
				//newObject.m_Faces[i].normalIndex[j] -= normalOffset;
				newObject.m_Faces[i].normalIndex[j] = newObject.countOfNormal + newObject.m_Faces[i].normalIndex[j]-normalOffset;
			} else {				
				newObject.m_Faces[i].normalIndex[j] -= normalOffset;
			}
		}
	}
	
	D3DXVECTOR3 aux_max;
	D3DXVECTOR3 aux_min;
	if (newObject.countOfVertex>0){
		aux_max = D3DXVECTOR3(m_Vertex[0].x, m_Vertex[0].y, m_Vertex[0].z);
		aux_min = D3DXVECTOR3(m_Vertex[0].x, m_Vertex[0].y, m_Vertex[0].z);		
	}		
	// Go through all the vertices in the object
	for(int i = 0; i < newObject.countOfVertex; i++){
		// Copy the current vertice from the temporary list to our Model list
		newObject.m_Vertex[i] = m_Vertex[i];
		if(aux_max.x < m_Vertex[i].x) aux_max.x = m_Vertex[i].x;
		if(aux_max.y < m_Vertex[i].y) aux_max.y = m_Vertex[i].y;
		if(aux_max.z < m_Vertex[i].z) aux_max.z = m_Vertex[i].z;
							 							
		if(aux_min.x > m_Vertex[i].x) aux_min.x = m_Vertex[i].x;
		if(aux_min.y > m_Vertex[i].y) aux_min.y = m_Vertex[i].y;
		if(aux_min.z > m_Vertex[i].z) aux_min.z = m_Vertex[i].z;
	}
	//asigno el max y min al obj
	newObject.max = aux_max;
	newObject.min = aux_min;
	newObject.center = D3DXVECTOR3(	(aux_max.x + aux_min.x)/2.0f,
									(aux_max.y + aux_min.y)/2.0f,
									(aux_max.z + aux_min.z)/2.0f);
	///////////
	newObject.radius = sqrt(	(aux_max.x-aux_min.x)*(aux_max.x-aux_min.x) + 
							(aux_max.y-aux_min.y)*(aux_max.y-aux_min.y) + 
							(aux_max.z-aux_min.z)*(aux_max.z-aux_min.z))/2.0f;
	
	// ���������� ��� ���������
	for (int i=0; i<pModel.countOfMaterials; i++){
		// ���� ��������� ���������� �������� � ������ ����������
		if (lastMaterial == pModel.m_Materials[i].nameMaterial){
			newObject.materialID = i;
			break;
		}
	}
    // ������ � ��� ���� ��� ���������� �� �� ������, �� ������ �������� ��
    // ��� ��� �� ����� ���� ������ � ���������� �������, ��� �� ������ �.
	pModel.countOfDetails++;
    // ��������� ����� ������ � ������
    pModel.m_Details.push_back(newObject);
	m_Faces.clear();
	m_Vertex.clear();
	m_TextureCoords.clear();
	m_Normal.clear();
	
	// Reset these booleans to be prepared for the next object
	m_bDetailHasUV   = false;
	m_bDetailHasNormal = false;
	m_bJustReadAFace = false;
}

DetailObjClass::DetailObjClass(){
	countOfVertex = 0;		// ���������� ������
	countOfNormal = 0;		// ���������� ��������
	countOfFaces = 0;		// ���������� ���������
	countTexVertex = 0;		// �������� ������ �������
	materialID = -1;		// ������ ���������
	radius = 0.0f;
	nameDetail = "NULL";
}
DetailObjClass::~DetailObjClass(){
	m_Vertex.clear();      
	m_Normals.clear();		
	m_TexVertex.clear();	
	m_Faces.clear();		
}
// ��������� ������ ������, ��� �������� ������������, � ���� �������
// ������� �������
bool DetailObjClass::GetCamputeDetail(std::vector<VertexType>& vecVertex, std::vector<int>& vecIndex){
	VertexType inVertex;
	vecVertex.clear();
	vecIndex.clear();
	// ����� �������
	int vertexIndex;
	int textureIndex;
	int normalIndex;
	// �������� � ��� ���� ������ ���������
	unsigned  int count = m_Faces.size();

	// ��������� �� ���� ���������
	for (unsigned int i=0; i<count; i++){
		// ������������ ��� ������� ������ ��������
		for (int j=0; j<3; j++){

			// ����� ��������������� ������� �������
			vertexIndex = m_Faces[i].vertexIndex[j];
			// ������������� ���������� �������
			if (vertexIndex < 0){
				vertexIndex = m_Vertex.size()+vertexIndex-1;
			}
			// ����� ��������������� ������� ��������
			textureIndex = m_Faces[i].textureIndex[j];
			//if (textureIndex < 0){
			//	textureIndex = m_TexVertex.size()+textureIndex;
			//}
			// ����� ��������������� ������� �������
			normalIndex = m_Faces[i].normalIndex[j];
			//if (normalIndex < 0){
			//	normalIndex = m_Normals.size()+normalIndex;
			//}
			// �������� ��� ������� �� �������� ������ ��������
			inVertex.position = m_Vertex[vertexIndex];

			// ����������, ��� � ��� ���� ������� �������
			if (countTexVertex > 0){
				inVertex.texture = m_TexVertex[textureIndex];
			
			} else {
				inVertex.texture = D3DXVECTOR2(0.0f, 0.0f);
			}
			// ����������, �� � ��� ���� ������� ��������
			if (countOfNormal > 0){
				inVertex.normal = m_Normals[normalIndex];
			} else {
				inVertex.normal = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
			}
			// ���������� ������ � ��������
			AddVertexAndIndex(vecVertex, vecIndex, inVertex);
		}
	}
	// ��������� � �������� �������(���� �� ����), ������� � ���������
	CamputeDetailVectors(vecVertex);
	return true;
}

// ��������������� �������
int DetailObjClass::AddVertex(std::vector<VertexType>& vecVertex, const VertexType& vert){
	// ����, ��� �� ����� �� ������� � �������
	int count = vecVertex.size();
	for (int i=0; i<count; i++){
		if ((vert.position == vecVertex[i].position) && 
			(vert.texture == vecVertex[i].texture) && 
			(vert.normal ==  vecVertex[i].normal)){
			return i; // �����, ���������� ������
		}
	}
	// �� ����� - ���������
	vecVertex.push_back(vert);
	return (int)(vecVertex.size()-1);
}

void DetailObjClass::AddVertexAndIndex(std::vector<VertexType>& vecVertex, std::vector<int>& vecIndex, const VertexType& vert){
	// ��������� ������� � �������� ��� ������
	int idx = AddVertex(vecVertex, vert);
	// ��������� ������ � ������ ������
	vecIndex.push_back(idx);
}

void DetailObjClass::CamputeDetailVectors(std::vector<VertexType>& vertex){
	int index;
	VertexType vertex1, vertex2, vertex3;
	D3DVECTOR tangent;
	D3DVECTOR binormal;
	D3DVECTOR normal;

	// Initialize the index to the model data.
	index = 0;
	int faceCount = (int) (vertex.size()/3);
	// Go through all the faces and calculate the the tangent, binormal, and normal vectors.
	for(int i=0; i<faceCount; i++){
		// Get the three vertices for this face from the model.
		vertex1.position.x = vertex[index].position.x;
		vertex1.position.y = vertex[index].position.y;
		vertex1.position.z = vertex[index].position.z;
		vertex1.texture.x = vertex[index].texture.x;
		vertex1.texture.y = vertex[index].texture.y;
		vertex1.normal.x = vertex[index].normal.x;
		vertex1.normal.y = vertex[index].normal.y;
		vertex1.normal.z = vertex[index].normal.z;
		index++;
		vertex2.position.x = vertex[index].position.x;
		vertex2.position.y = vertex[index].position.y;
		vertex2.position.z = vertex[index].position.z;
		vertex2.texture.x = vertex[index].texture.x;
		vertex2.texture.y = vertex[index].texture.y;
		vertex2.normal.x = vertex[index].normal.x;
		vertex2.normal.y = vertex[index].normal.y;
		vertex2.normal.z = vertex[index].normal.z;
		index++;	
		vertex3.position.x = vertex[index].position.x;
		vertex3.position.y = vertex[index].position.y;
		vertex3.position.z = vertex[index].position.z;
		vertex3.texture.x = vertex[index].texture.x;
		vertex3.texture.y = vertex[index].texture.y;
		vertex3.normal.x = vertex[index].normal.x;
		vertex3.normal.y = vertex[index].normal.y;
		vertex3.normal.z = vertex[index].normal.z;
		index++;	
		// Calculate the tangent and binormal of that face.
		CamputeTangentBinormal(vertex1, vertex2, vertex3, tangent, binormal);
		// Calculate the new normal using the tangent and binormal.
		CamputeNormal(tangent, binormal, normal);

		// Store the tangent and binormal for this face back in the model structure.
		vertex[index-1].normal.x = normal.x;
		vertex[index-1].normal.y = normal.y;
		vertex[index-1].normal.z = normal.z;
		vertex[index-1].tangent.x = tangent.x;
		vertex[index-1].tangent.y = tangent.y;
		vertex[index-1].tangent.z = tangent.z;
		vertex[index-1].binormal.x = binormal.x;
		vertex[index-1].binormal.y = binormal.y;
		vertex[index-1].binormal.z = binormal.z;

		vertex[index-2].normal.x = normal.x;
		vertex[index-2].normal.y = normal.y;
		vertex[index-2].normal.z = normal.z;
		vertex[index-2].tangent.x = tangent.x;
		vertex[index-2].tangent.y = tangent.y;
		vertex[index-2].tangent.z = tangent.z;
		vertex[index-2].binormal.x = binormal.x;
		vertex[index-2].binormal.y = binormal.y;
		vertex[index-2].binormal.z = binormal.z;

		vertex[index-3].normal.x = normal.x;
		vertex[index-3].normal.y = normal.y;
		vertex[index-3].normal.z = normal.z;
		vertex[index-3].tangent.x = tangent.x;
		vertex[index-3].tangent.y = tangent.y;
		vertex[index-3].tangent.z = tangent.z;
		vertex[index-3].binormal.x = binormal.x;
		vertex[index-3].binormal.y = binormal.y;
		vertex[index-3].binormal.z = binormal.z;
	}

	return;
}

void  DetailObjClass::CamputeTangentBinormal(VertexType vertex1, VertexType vertex2, VertexType vertex3,
											D3DVECTOR& tangent, D3DVECTOR& binormal){
	float vector1[3], vector2[3];
	float tuVector[2], tvVector[2];
	float den;
	float length;


	// Calculate the two vectors for this face.
	vector1[0] = vertex2.position.x - vertex1.position.x;
	vector1[1] = vertex2.position.y - vertex1.position.y;
	vector1[2] = vertex2.position.z - vertex1.position.z;

	vector2[0] = vertex3.position.x - vertex1.position.x;
	vector2[1] = vertex3.position.y - vertex1.position.y;
	vector2[2] = vertex3.position.z - vertex1.position.z;

	// Calculate the tu and tv texture space vectors.
	tuVector[0] = vertex2.texture.x - vertex1.texture.x;
	tvVector[0] = vertex2.texture.y - vertex1.texture.y;

	tuVector[1] = vertex3.texture.x - vertex1.texture.x;
	tvVector[1] = vertex3.texture.y - vertex1.texture.y;

	// Calculate the denominator of the tangent/binormal equation.
	den = 1.0f / (tuVector[0] * tvVector[1] - tuVector[1] * tvVector[0]);

	// Calculate the cross products and multiply by the coefficient to get the tangent and binormal.
	tangent.x = (tvVector[1] * vector1[0] - tvVector[0] * vector2[0]) * den;
	tangent.y = (tvVector[1] * vector1[1] - tvVector[0] * vector2[1]) * den;
	tangent.z = (tvVector[1] * vector1[2] - tvVector[0] * vector2[2]) * den;

	binormal.x = (tuVector[0] * vector2[0] - tuVector[1] * vector1[0]) * den;
	binormal.y = (tuVector[0] * vector2[1] - tuVector[1] * vector1[1]) * den;
	binormal.z = (tuVector[0] * vector2[2] - tuVector[1] * vector1[2]) * den;

	// Calculate the length of this normal.
	length = sqrt((tangent.x * tangent.x) + (tangent.y * tangent.y) + (tangent.z * tangent.z));
			
	// Normalize the normal and then store it
	tangent.x = tangent.x / length;
	tangent.y = tangent.y / length;
	tangent.z = tangent.z / length;

	// Calculate the length of this normal.
	length = sqrt((binormal.x * binormal.x) + (binormal.y * binormal.y) + (binormal.z * binormal.z));
			
	// Normalize the normal and then store it
	binormal.x = binormal.x / length;
	binormal.y = binormal.y / length;
	binormal.z = binormal.z / length;

	return;
}
void  DetailObjClass::CamputeNormal(D3DVECTOR tangent, D3DVECTOR binormal, D3DVECTOR& normal){

	float length;
	// Calculate the cross product of the tangent and binormal which will give the normal vector.
	normal.x = (tangent.y * binormal.z) - (tangent.z * binormal.y);
	normal.y = (tangent.z * binormal.x) - (tangent.x * binormal.z);
	normal.z = (tangent.x * binormal.y) - (tangent.y * binormal.x);

	// Calculate the length of the normal.
	length = sqrt((normal.x * normal.x) + (normal.y * normal.y) + (normal.z * normal.z));

	// Normalize the normal.
	normal.x = normal.x / length;
	normal.y = normal.y / length;
	normal.z = normal.z / length;

	return;


}
/////////////////////////////////////////////////////////////////////////////////
// First, no .obj file format is going to be the same.  When you import/export
// .obj files anywhere, each application has their own way of saving it.  Some
// save normals, some save extra comments, some save object names, etc...  That is
// why I calculate my own normals because rarely are they including as "vn" in the file.
// The only thing you can depend on is the headers:
//
// "v"  - This is a line that contains a vertex (x y z)
//
//              IE:                     v       -1      -1      0
//
// After the 'v' it will list the X Y and Z information for that vertex.
//
// "vt" - This is a line that contains a UV texture coordinate (U V)
//
//              IE:                     vt      .99998  .99936
//
// After the "vt" it will list the U and V information for a vertex.
// Note that this will ONLY appear in the file if the object has a UVW map.
// Just dragging a texture onto an object doesn't create a UVW map.
//
// "f"  - This is a line that contains the vertex indices into the vertex array.
//        If there are UV coordinates for that object, it also contains the UV indices.
//
//              IE (Just vertices):								f       1       2       3
//              
//              IE (Verts and UV Indices)						f       1/1		2/2		3/3
//
//				IE (Vertex and UV Indeces and Normal Indices)	f		1/1/1	2/2/2	3/3/3
//
//				IE (Vertex and Normal Indices)					f		1//1	2//2	3//3
//
// After the f it will list the vertice indices, or the vertex / UV indices.
//
// Those are the only 3 you can count on 99% of the time.  The other one that
// isn't always there is "vn".  That is a vertex normal:  vn -1 0 0
// There are some other lines that are sometimes used by they can be generally ignored.
//
// Once again, I personally would not use this format for anything I do because it's
// huge in file size and doesn't contain much information about the model, but I would
// recommend that you start here, and then add your own information to the file and
// modify this tutorial to read it in.  Eventually jump on board with .3DS because it
// has key frame information and such for animation, where .obj does NOT.
//
// I will eventually add this to a 3D Loading Library along with the other formats.
// Let us know if this helps you out!
////////////////////////////////////////////////////////////////////////////////
// Filename: textclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _TEXTCLASS_H_
#define _TEXTCLASS_H_


///////////////////////
// MY CLASS INCLUDES //
///////////////////////
#include "fontclass.h"
#include <vector>

#include "widgetclass.h"

////////////////////////////////////////////////////////////////////////////////
// Class name: TextClass
////////////////////////////////////////////////////////////////////////////////
class TextClass
{
private:
	struct SentenceType{
		ID3D10Buffer *vertexBuffer, *indexBuffer;
		int vertexCount, indexCount, maxLength;
		float red, green, blue;
		float positionX, positionY;
	};

	struct VertexType{
		D3DXVECTOR3 position;
	    D3DXVECTOR2 texture;
	};

public:
	TextClass();
	TextClass(const TextClass&);
	~TextClass();

	bool Initialize(ID3D10Device*, int, int, float);
	bool AddSentence(ID3D10Device*, std::string, float, float, D3DXVECTOR3);
	unsigned int GetCountSentence();
	void Shutdown();
	void Render(ID3D10Device*, ShaderManagerClass*, TextureManagerClass*,
				D3DXMATRIX, D3DXMATRIX, D3DXMATRIX );
	int GetIndexCount(int);
	float GetFontSize(void);
	float GetLengthText(std::string text);
	D3DXVECTOR4 GetPixelColor(int);
	bool EditText(std::string,unsigned int);
	std::string GetTexture();
	

private:
	bool InitializeSentence(SentenceType**, int, ID3D10Device*);
	bool UpdateSentence(SentenceType*, std::string, float, float, float, float, float);
	void ReleaseSentence(SentenceType**);
	void RenderSentence(ID3D10Device*, SentenceType*);

private:
	FontClass* m_Font;
	std::vector<SentenceType*> m_sentence;
	int m_screenWidth, m_screenHeight;
};

#endif
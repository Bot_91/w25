// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
////////////////////////////////////////////////////////////////////////////////
// Filename: LabelClass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "labelclass.h"

LabelClass::LabelClass() {
	m_Text = NULL;
	this->Text = "Label";
}
// ������� ��������� ������
bool LabelClass::Create(ID3D10Device* device, TextureManagerClass* p_TextureManager,
	int screenWidth, int screenHeight) {
	// �����
	m_Text = new TextClass;
	m_Text->Initialize(device, screenWidth, screenHeight, 1);
	// ���� ������
	p_TextureManager->Add(device, m_Text->GetTexture());
	m_Text->AddSentence(device, this->Text, this->Left, this->Top, D3DXVECTOR3(1.0f, 1.0f, 1.0f));

	return true;
}
bool LabelClass::Render(ID3D10Device* device, ShaderManagerClass* p_ShaderManager,
						 TextureManagerClass* p_TextureManager,
						 D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix,
						 D3DXMATRIX projectionMatrix) {

	// ����� �� ������
	m_Text->Render(device, p_ShaderManager, p_TextureManager, worldMatrix, viewMatrix, projectionMatrix);

	return true;
}

void  LabelClass::SetText(std::string inText) {
	this->Text = inText;
	m_Text->EditText(this->Text, 0);
}

void LabelClass::Shutdown(void) {
	if (m_Text) {
		m_Text->Shutdown();
		delete m_Text;
		m_Text = 0;
	}
}
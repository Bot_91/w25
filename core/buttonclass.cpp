// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
////////////////////////////////////////////////////////////////////////////////
// Filename: buttonclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "buttonclass.h"


ButtonClass::ButtonClass(){
	// �������������� �������

	this->state = type_state_control::NORMAL;
	eventOnClick = NULL;
	this->hover.diffuse = "./data/buttonH.png";
	this->active.diffuse = "./data/buttonA.png";
	this->normal.diffuse = "./data/buttonN.png";
	m_vertexBuffer = NULL;
	m_indexBuffer = NULL;
	m_Text = NULL;
}
ButtonClass::~ButtonClass() {
}
// ���������� ������ � ������
bool ButtonClass::Create(ID3D10Device* device, TextureManagerClass* p_TextureManager, 
						 int screenWidth, int screenHeight) {
	m_screenWidth = screenWidth;
	m_screenHeight = screenHeight;
	// ����� �� ������, �� ���� ��� Widget � ��� ������ ��������� ������� ��������
	// �����
	m_Text = new TextClass; 
	m_Text->Initialize(device, screenWidth, screenHeight, 1);
	// ���� ������
	p_TextureManager->Add(device, m_Text->GetTexture());
	float leftText = this->Left + this->Width / 2.0f -
					 m_Text->GetLengthText(this->Text) / 2.0f;
	float topText = this->Top + this->Height / 2.0f - 8;
	m_Text->AddSentence(device, this->Text, leftText, topText, D3DXVECTOR3(1.0f, 1.0f, 1.0f));

	// �������� ������
	// ���� ������
	p_TextureManager->Add(device, this->hover.diffuse);
	p_TextureManager->Add(device, this->active.diffuse);
	p_TextureManager->Add(device, this->normal.diffuse);

	this->InitializeBuffers(device);
	this->UpdateBuffers(this->Left, this->Top, this->Width, this->Height, screenWidth, screenHeight);
	return true;
}
void ButtonClass::SetText(std::string inText) {
	this->Text = inText;
	m_Text->EditText(this->Text, 0);

}
bool ButtonClass::Render(	ID3D10Device* device, ShaderManagerClass* p_ShaderManager,
							TextureManagerClass* p_TextureManager,
							D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, 
							D3DXMATRIX projectionMatrix) {
	
	// ������ ���������������� �������������

	this->RenderBuffers(device);
	std::string keyTexturePick = "NULL";
	switch (this->state) {
		case type_state_control::ACTIVE: keyTexturePick = active.diffuse; break;
		case type_state_control::HOVER:  keyTexturePick = hover.diffuse; break;
		case type_state_control::NORMAL: keyTexturePick = normal.diffuse; break;
	}

	p_ShaderManager->RenderTextureShader(device, this->m_indexCount, 
										worldMatrix, viewMatrix, projectionMatrix, 
										p_TextureManager->GetTexture(keyTexturePick));
	// ����� �� ������
	m_Text->Render(device, p_ShaderManager, p_TextureManager, worldMatrix, viewMatrix, projectionMatrix);

	return true;
}

bool ButtonClass::Update(InputClass* p_Input, int mouseX, int mouseY){
	bool result = false;
	// ������ �� ������
	if (this->GetReside(mouseX, mouseY) == true) {
		// ������
		if (this->state == type_state_control::NORMAL) {
			this->state = type_state_control::HOVER;
		}
		// �������� ����� �������
		if (p_Input->IsMouseDown(MOUSE_LEFT)) {
			this->state = type_state_control::ACTIVE;
		}
		// ������ ����� ������, ������ �� ������
		if (p_Input->IsMousePress(MOUSE_LEFT)) {
			// �������
			if (this->state == type_state_control::ACTIVE) {
				this->state = type_state_control::HOVER;
				this->eventOnClick(MOUSE_LEFT, mouseX, mouseY);
			}
		}
		result = true;
	}
	else {
		// �� ������ ��� ������
		this->state = type_state_control::NORMAL;
	}

	return result;
}

void ButtonClass::Shutdown(void) {
	if (m_Text) {
		m_Text->Shutdown();
		delete m_Text;
		m_Text = 0;
	}
//	this->ShutdownBuffers();
	
}
// ��������� ������� ������ ����� ������� ��������
void ButtonClass::onClick(int mouseKey, int mouseX, int mouseY){
	if(!this->eventOnClick.IsNull()){
		eventOnClick(mouseKey, mouseX, mouseY);
	}
	return;	
}
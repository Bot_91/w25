// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
///////////////////////////////////////////////////////////////////////////////
// Filename: timerclass.cpp
///////////////////////////////////////////////////////////////////////////////
#include "timerclass.h"


TimerClass::TimerClass()
{
	m_frequency = 0;
	m_ticksPerMs = 0;
	m_startTime = 0;
	m_frameTime = 0;
}


TimerClass::TimerClass(const TimerClass& other){
	m_frequency = 0;
	m_ticksPerMs = 0;
	m_startTime = 0;
	m_frameTime = 0;
}


TimerClass::~TimerClass()
{
}


bool TimerClass::Initialize()
{
	// Check to see if this system supports high performance timers.
	QueryPerformanceFrequency((LARGE_INTEGER*)&m_frequency);
	if(m_frequency == 0)
	{
		return false;
	}

	// Find out how many times the frequency counter ticks every millisecond./////-/
	m_ticksPerMs = (float)(m_frequency);

	QueryPerformanceCounter((LARGE_INTEGER*)&m_startTime);

	return true;
}


bool TimerClass::Frame()
{
	INT64 currentTime;
	float timeDifference;


	QueryPerformanceCounter((LARGE_INTEGER*)&currentTime);

	timeDifference = (float)(currentTime - m_startTime);

	m_frameTime = timeDifference / m_ticksPerMs;

	m_startTime = currentTime;

	return true;
}

void TimerClass::Shutdown()
{
	return;
}

float TimerClass::GetFrameTime()
{
	return m_frameTime;
}
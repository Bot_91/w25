#ifndef _STATECLASS_MENU_H_
#define _STATECLASS_MENU_H_

#include "stateclass.h"
////////////////////////////////////////////////////////////////////////////////
// ��������� ������ ����
////////////////////////////////////////////////////////////////////////////////
class MenuState: public StateClass 
{ 

public:
	//���������� ��������� �� ������ ��� ���������� ��� � �������� ��������� 
	static MenuState* GetInstance() { return &m_MenuState; }
	// �����������
	MenuState(WCHAR*);
	// ��������� � ���������� ���������
	bool Render(D3DClass*); 
	bool Update(D3DClass*, InputClass*, float);
	// �������� ��� ����� � �����(�������� ������)
	bool Initialize(HWND, D3DClass*, MsgManagerClass*, ModelManagerClass*, TextureManagerClass*); 
	// �������� �� ������ �� ������(�������� �������� ��������)
	bool Exit(); 
	// ������������ ���� ��������
	bool Shutdown();
	// ������� ��� ������� �� ������
	void OnClickButtonStart(int, int,int);
	void OnClickButtonEnd(int, int,int);
private:
	bool RenderPrimary(D3DClass*); 
	bool UpdatePrimary(D3DClass*, InputClass*, float);
	// ��������� �� �����
	static MenuState m_MenuState;
	// ������
	CameraClass* m_Camera;
	// ������� ��������� ������
	FrustumClass* m_Frustum;

	// ����
	LightClass* m_Light;
	// �������� ��������
	ShaderManagerClass* m_ShaderManager;
	// �������� ��������
	ObjectManagerClass* m_ObjectManager;
	// ���������� �������
	int MouseX;
	int MouseY;
	int GetCountRenderVertex(D3DClass*);

};

#endif
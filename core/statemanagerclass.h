////////////////////////////////////////////////////////////////////////////////
// Filename: statemanagerclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _STATEMANAGERCLASS_H_
#define _STATEMANAGERCLASS_H_
//////////////
// INCLUDES //
//////////////

///////////////////////
// MY CLASS INCLUDES //
///////////////////////
#include "stateclass.h"
// ��� ��������� �������� �� � stateclass ��������� �����
#include "stateclass_menu.h"
#include "stateclass_debug.h"
#include "stateclass_play.h"

////////////////////////////////////////////////////////////////////////////////
// Class name: StateManagerClass
////////////////////////////////////////////////////////////////////////////////


class StateManagerClass{
private:
	std::vector<StateClass*> m_States;
	public:
	// ���������
	bool Render(D3DClass*);
	// ���������� �����
	bool Frame(D3DClass*, InputClass*, float);
	// ��������� ��������� � ������� ���������
	bool Inspector(HWND, D3DClass*);
	// C���� ���������
	bool ChangeState(HWND, D3DClass*, MsgManagerClass*, ModelManagerClass*, TextureManagerClass*, StateClass*);
	// ���������� ���������
	bool PushState(HWND, D3DClass*, MsgManagerClass*, ModelManagerClass*, TextureManagerClass*, StateClass*);
	// ������� � ����������� ���������
	bool PopState();
	bool EraseState(WCHAR*);
	bool CheckStateName(WCHAR*);
	// ������� ��� ��������� �� �����
	void Shutdown();
};

#endif
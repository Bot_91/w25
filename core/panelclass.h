////////////////////////////////////////////////////////////////////////////////
// Filename: panelclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _PANELCLASS_H_
#define _PANELCLASS_H_

#include "widgetclass.h"

class PanelClass : public BoxClass{
public:
	bool Update(InputClass*, int, int);
	bool Create(ID3D10Device*, TextureManagerClass*, int, int);
	bool Render(ID3D10Device*, ShaderManagerClass*, TextureManagerClass*,
				D3DXMATRIX, D3DXMATRIX, D3DXMATRIX);
	void Add(ID3D10Device* device, WidgetClass* widget);
	bool SetText(std::string, std::string);
	void Shutdown(void);
	PanelClass();
private:
	std::string keyTexture;
	std::vector<WidgetClass*> m_Widget;
	

};

#endif
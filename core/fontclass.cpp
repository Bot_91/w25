// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
///////////////////////////////////////////////////////////////////////////////
// Filename: fontclass.cpp
///////////////////////////////////////////////////////////////////////////////
#include "fontclass.h"


FontClass::FontClass(){
	fontChar = 0;
	font_size_X = 0;
	font_size_Y = 0;
}


FontClass::FontClass(const FontClass& other)
{
	fontChar = 0;
	font_size_X = 0;
	font_size_Y = 0;
}


FontClass::~FontClass()
{
}


bool FontClass::Initialize(ID3D10Device* device, std::string fontFilename, 
							std::string textureFilename, float fontSize)
{
	bool result;
	// Load in the text file containing the font data.
	result = LoadFontData(fontFilename);
	if(!result){
		return false;
	}
	font_size_Y = fontSize;
	font_size_X = fontSize;
	keyTexture = textureFilename;
	return true;
}


void FontClass::Shutdown(){

	// Release the font data.
	ReleaseFontData();

	return;
}

// ������ �������� ������� �� �����, ������� ���� ������ � ��������� ��� �������� �������, ���������
bool FontClass::LoadFontData(std::string filename){
	ifstream fin;
	char temp;
	// ������� ����� ��� ������� Unicode
	// ������ ��������� ������, �� ����� ��� ������ ��� ������� ������������ ������
	// ��������� �� ��� ����� ��������
	fontChar = new FontType[2048];
	if(!fontChar){
		return false;
	}

	// Read in the font size and spacing between chars.
	fin.open(filename);
	if(fin.fail()){
		return false;
	}

	// ��������� ��� ��������� ������� �� �����
	while(!fin.eof()){
		// ��������� ����� � ������� Unicode
		int numberChar;
		fin >> numberChar;
		fin.get(temp);
		while(temp != ' '){
			fin.get(temp);
		}
		fin.get(temp);
		while(temp != ' '){
			fin.get(temp);
		}
		// ��������� ���������� �������� � �������� ������
		fin >> fontChar[numberChar].left;
		fin >> fontChar[numberChar].right;
		fin >> fontChar[numberChar].size;
	}

	// Close the file.
	fin.close();

	return true;
}


void FontClass::ReleaseFontData(){
	// Release the font data array.
	if(fontChar){
		delete [] fontChar;
		fontChar = 0;
	}

	return;
}

std::string FontClass::GetTexture(){
	return keyTexture;
}


void FontClass::BuildVertexArray(void* vertices, std::string sentence, float drawX, float drawY){
	VertexType* vertexPtr;
	int numLetters, index, i, letter;
	// Coerce the input vertices into a VertexType structure.
	vertexPtr = (VertexType*)vertices;
	// Get the number of letters in the sentence.
	numLetters = (int)sentence.length();

	// Initialize the index to the vertex array.
	index = 0;
	// Draw each letter onto a quad.
	for(i=0; i<numLetters; i++){
		letter = ((int)sentence[i]);
		// ���� ������� ������������ ��������
		if(letter == 32){
			drawX = drawX + 3.0f*font_size_X;
		} else {
			// First triangle in quad.
			// Top left.
			vertexPtr[index].position = D3DXVECTOR3(drawX, drawY, 0.0f);  
			vertexPtr[index].texture = D3DXVECTOR2(fontChar[letter].left, 0.0f);
			index++;
			// Bottom right.
			vertexPtr[index].position = D3DXVECTOR3((drawX + fontChar[letter].size*font_size_X), 
													(drawY - 16*font_size_Y), 0.3f);  
			vertexPtr[index].texture = D3DXVECTOR2(fontChar[letter].right, 0.9f);
			index++;
			// Bottom left.
			vertexPtr[index].position = D3DXVECTOR3(drawX, (drawY - 16*font_size_Y), 0.0f);  
			vertexPtr[index].texture = D3DXVECTOR2(fontChar[letter].left, 0.9f);
			index++;

			// Second triangle in quad.
			// Top left.
			vertexPtr[index].position = D3DXVECTOR3(drawX, drawY, 0.0f);  
			vertexPtr[index].texture = D3DXVECTOR2(fontChar[letter].left, 0.0f);
			index++;
			// Top right.
			vertexPtr[index].position = D3DXVECTOR3(drawX + fontChar[letter].size*font_size_X, drawY, 0.0f); 
			vertexPtr[index].texture = D3DXVECTOR2(fontChar[letter].right, 0.0f);
			index++;
			// Bottom right.
			vertexPtr[index].position = D3DXVECTOR3((drawX + fontChar[letter].size*font_size_X),
													(drawY - 16*font_size_Y), 0.0f);  
			vertexPtr[index].texture = D3DXVECTOR2(fontChar[letter].right, 0.9f);
			index++;

			// Update the x location for drawing by the size of the letter and one pixel.
			drawX = drawX + fontChar[letter].size*font_size_X + 2.0f*font_size_X;
		}
	}

	return;
}
float FontClass::GetLengthText(std::string text) {
	int numLetters = (int)text.length();
	float result = 0;
	// Draw each letter onto a quad.
	for (int i = 0; i<numLetters; i++){
		int letter = ((int)text[i]);
		// ���� ������� ������������ ��������
		if (letter == 32){
			result = result + 3.0f*font_size_X;
		}else{
			// Update the x location for drawing by the size of the letter and one pixel.
			result = result + fontChar[letter].size*font_size_X + 2.0f*font_size_X;
		}
	}
	return result;

}

float FontClass::GetFontSize(void){
	return font_size_Y;
}
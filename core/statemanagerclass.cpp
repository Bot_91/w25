// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
////////////////////////////////////////////////////////////////////////////////
// Filename: statemanagerclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "statemanagerclass.h"



// ���������� 
// ��������� ���� ���������
bool StateManagerClass::Render(D3DClass* p_D3D)
{
	bool result=true;
	//�������� ������ �������� ���������
	p_D3D->BeginScene(0.1f,  0.3f, 0.1f, 1.0f);
	for (unsigned int i=0; i<m_States.size(); i++){
		if (!m_States[i]->GetMode().pause){	
			result = m_States[i]->Render(p_D3D);	
		}
			// ��������� ������ ���������� ����� ������ ������ ��������� � ����������
		if (!result){
			p_D3D->EndScene();
			return result;
		}
	}
	// �������� ��� �����.
	p_D3D->EndScene();
	return true;
}
// ����������, ������������� ������ ������ ��������� �������� ������� ���������
// ��������� ���� ���������
bool StateManagerClass::Frame(D3DClass* p_D3D, InputClass* p_Input, float time)
{
	bool result=true;
	//�������� ���������� ������ �������� ���������

	for (unsigned int i=0; i<m_States.size(); i++){
		if (!m_States[i]->GetMode().pause){	
			result = m_States[i]->Update(p_D3D, p_Input, time);	
		}
			// ��������� ������ ���������� ����� ������ ������ ��������� � ����������
		if (!result){
			return result;
		}
	}
		
	return result;	
}
bool StateManagerClass::Inspector(HWND hwnd, D3DClass* p_D3D)
{
	return true;
}
// ������� ��� ���������, ���������� ���������� ��� ������
void StateManagerClass::Shutdown()
{
	// �������� ����� �� ��������� � ������� �� �� �������
	while ( !m_States.empty() ) {
		m_States.back()->Exit();
		m_States.pop_back(); 
	}
}
bool StateManagerClass::ChangeState(HWND hwnd, D3DClass* p_D3D,  
									MsgManagerClass* p_MsgManager, ModelManagerClass* p_ModelManager, 
									TextureManagerClass* p_TextureManager, StateClass* state) //����� ���������
{
	// ����� �� �������� ���������
	if ( !m_States.empty() ) 
	{
		m_States.back()->Exit();
		m_States.pop_back(); 
	}
	// ������������� ������ ������
	m_States.push_back(state);
	return false;//m_States.back()->Initialize(hwnd, p_D3D, p_MsgManager, p_ModelManager, p_TextureManager);
}

// ���������� ������ ���������
bool StateManagerClass::PushState(HWND hwnd, D3DClass* p_D3D, MsgManagerClass* p_MsgManager, 
									ModelManagerClass* p_ModelManager, 
									TextureManagerClass* p_TextureManager, StateClass* state)
{
	// ������������� ������ ������
	bool result = false;
	unsigned int number;
	// ���� ��������� � �����
	for (unsigned int i=0; i<m_States.size(); i++) {
		if (m_States[i]->GetNameState() == state->GetNameState()) {
			result = true;
			number = i;
			m_States[i]->Resume();
		}
	}
	// ���� ��������� ��� �� ���������� ��������� ��� � ����
	if (!result){
		m_States.push_back(state);
		return m_States.back()->Initialize(hwnd, p_D3D, p_MsgManager, p_ModelManager, p_TextureManager);
	} 
	return true;
}
// ������� � �����
bool StateManagerClass::PopState(void) 
{
	// ����� �� �������� ������
	if ( m_States.size()>0 ) 
	{
		m_States.back()->Exit();
		m_States.pop_back();
	}
	// ������������ ����������
	/*if ( !mStates.empty() ) 
	{
		return mStates.back()->Resume();
	}*/
	return true;
}
// �������� ��������� �� �����
bool StateManagerClass::EraseState(WCHAR* name){
	for (unsigned int i=0; i<m_States.size(); i++){
		if (m_States[i]->GetNameState() == name){
			m_States[i]->Exit();
			m_States.erase(m_States.begin() + i);
		}
	}
	return true;
}
bool StateManagerClass::CheckStateName(WCHAR* name){
	for (unsigned int i=0; i<m_States.size(); i++){
		if (m_States[i]->GetNameState() == name){
			return true;
		}
	}
	return false;
}
////////////////////////////////////////////////////////////////////////////////
// Filename: systemclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _SYSTEMCLASS_H_
#define _SYSTEMCLASS_H_


///////////////////////////////
// PRE-PROCESSING DIRECTIVES //
///////////////////////////////
#define WIN32_LEAN_AND_MEAN


//////////////
// INCLUDES //
//////////////
#include <windows.h>

///////////////////////
// MY CLASS INCLUDES //
///////////////////////
#include "timerclass.h"
#include "./input/inputclass.h"
#include "./graphic/shader/d3d/d3dclass.h"
#include "msgmanagerclass.h"
#include "statemanagerclass.h"
void PrintScreen(HWND hwnd);
////////////////////////////////////////////////////////////////////////////////
// Class name: SystemClass
////////////////////////////////////////////////////////////////////////////////
class SystemClass
{
private:
	bool Frame();
	void InitializeWindows(int&, int&);
	void ShutdownWindows();
private:
	// �������� ���������
	float timeDelayRender;
	float timeDelayFrame;
	int WinCursorX;
	int WinCursorY;
	int prevScreenWidth;
	int prevScreenHeight;
	LPCWSTR m_applicationName;
	HINSTANCE m_hinstance;
	HWND m_hwnd;
	// ������ �������
	D3DClass* m_D3D;
	// ������ �������
	TimerClass* m_Timer;	
	// ������ �����
	InputClass* m_Input;
	// �������� ���������
	MsgManagerClass* m_MsgManager;
	// �������� ���������
	StateManagerClass* m_StateManager;
	// �������� �������
	TextureManagerClass* m_TextureManager;
	// �������� �������
	ModelManagerClass* m_ModelManager;
public:
	SystemClass();
	SystemClass(const SystemClass&);
	~SystemClass();
public:
	bool Initialize();
	void Shutdown();
	void Run();
	LRESULT CALLBACK MessageHandler(HWND, UINT, WPARAM, LPARAM);
};


/////////////////////////
// FUNCTION PROTOTYPES //
/////////////////////////
static LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

/////////////
// GLOBALS //
/////////////
static SystemClass* ApplicationHandle = 0;


#endif
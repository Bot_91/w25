// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "panelclass.h"

// ������� ������ �������
bool PanelClass::Create(ID3D10Device* device, TextureManagerClass* p_TextureManager,
						int screenWidth, int screenHeight) {
	m_screenWidth = screenWidth;
	m_screenHeight = screenHeight;
	p_TextureManager->Add(device, keyTexture);
	this->InitializeBuffers(device);
	this->UpdateBuffers(this->Left, this->Top, this->Width, this->Height, screenWidth, screenHeight);
	for (unsigned int i = 0; i < m_Widget.size(); i++) {
		m_Widget[i]->Left += this->Left;
		m_Widget[i]->Top += this->Top;
		m_Widget[i]->Create(device, p_TextureManager, screenWidth, screenHeight);
	}
	
	return true;
}
// ��������� ������� � ������
void PanelClass::Add(ID3D10Device* device, WidgetClass* widget) {
	this->m_Widget.push_back(widget);
}
// map �� ����� NAME ������� �������� �� �����
bool PanelClass::SetText(std::string NameWidget, std::string inText) {
	for (unsigned int i = 0; i < m_Widget.size(); i++) {
		if (m_Widget[i]->Name == NameWidget) {
			m_Widget[i]->SetText(inText);
			return true;
		}
	}
	return false;
}
bool PanelClass::Render(ID3D10Device* device, ShaderManagerClass* p_ShaderManager,
						TextureManagerClass* p_TextureManager,
						D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix,
						D3DXMATRIX projectionMatrix) {

	this->RenderBuffers(device);
	p_ShaderManager->RenderTextureShader(device, this->m_indexCount,
										 worldMatrix, viewMatrix, projectionMatrix,
										 p_TextureManager->GetTexture(keyTexture));
	for (unsigned int i = 0; i < m_Widget.size(); i++) {
		m_Widget[i]->Render(device, p_ShaderManager, p_TextureManager, worldMatrix, viewMatrix, projectionMatrix);
	}
	return true;
}

bool PanelClass::Update(InputClass* p_Input, int mouseX, int mouseY) {
	bool result = false;
	for (unsigned int i = 0; i < m_Widget.size(); i++) {
		// ���� ���� �� ���� ������� ��� ������
		result |= m_Widget[i]->Update(p_Input, mouseX, mouseY);
	}
	// �������� ���� ������
	result |= this->GetReside(mouseX, mouseY);
	return result;
}

void PanelClass::Shutdown(void) {
	for (unsigned int i = 0; i < m_Widget.size(); i++) {
		this->m_Widget[i]->Shutdown();
		delete m_Widget[i];
		m_Widget[i] = 0;
	}
	m_Widget.clear();
}

PanelClass::PanelClass(){
	keyTexture = "./data/metal.dds";
}




// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "imageclass.h"

// ������� ��������
bool ImageClass::Create(ID3D10Device* device, TextureManagerClass* p_TextureManager,
						int screenWidth, int screenHeight) {
	m_screenWidth = screenWidth;
	m_screenHeight = screenHeight;
	this->InitializeBuffers(device);
	this->UpdateBuffers(this->Left, this->Top, this->Width, this->Height, screenWidth, screenHeight);
	return true;
}

bool ImageClass::Render(ID3D10Device* device, ShaderManagerClass* p_ShaderManager,
						TextureManagerClass* p_TextureManager,
						D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix,
						D3DXMATRIX projectionMatrix) {

	this->RenderBuffers(device);
	p_ShaderManager->RenderTextureShader(device, this->m_indexCount,
										 worldMatrix, viewMatrix, projectionMatrix,
										 p_TextureManager->GetTexture(keyTexture));
	return true;
}


ImageClass::ImageClass()
{
	keyTexture = "NULL";
}


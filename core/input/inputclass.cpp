// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
////////////////////////////////////////////////////////////////////////////////
// Filename: inputclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "inputclass.h"


InputClass::InputClass()
{
	m_directInput = 0;
	m_keyboard = 0;
	m_mouse = 0;
	for (int i=0; i<256; i++){
		m_KeyState[i]=0;
	}
}


InputClass::InputClass(const InputClass& other)
{
}


InputClass::~InputClass()
{
}

// ������������ ���������� � �����
bool InputClass::Initialize(HINSTANCE hinstance, HWND hwnd, int screenWidth, int screenHeight)
{
	HRESULT result;


	// Store the screen size which will be used for positioning the mouse cursor.
	m_screenWidth = screenWidth;
	m_screenHeight = screenHeight;

	// Initialize the location of the mouse on the screen.
	POINT WinCursor;
	GetCursorPos(&WinCursor);
	m_mouseX = WinCursor.x;
	m_mouseY = WinCursor.y;

	// Initialize the main direct input interface.
	result = DirectInput8Create(hinstance, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&m_directInput, NULL);
	if(FAILED(result))
	{
		return false;
	}

	// Initialize the direct input interface for the keyboard.
	result = m_directInput->CreateDevice(GUID_SysKeyboard, &m_keyboard, NULL);
	if(FAILED(result))
	{
		return false;
	}

	// Set the data format.  In this case since it is a keyboard we can use the predefined data format.
	result = m_keyboard->SetDataFormat(&c_dfDIKeyboard);
	if(FAILED(result))
	{
		return false;
	}

	// Set the cooperative level of the keyboard to not share with other programs.
	result = m_keyboard->SetCooperativeLevel(hwnd, DISCL_FOREGROUND | DISCL_EXCLUSIVE);
	if(FAILED(result))
	{
		return false;
	}

	// Now acquire the keyboard.
	result = m_keyboard->Acquire();
	if(FAILED(result))
	{
		return false;
	}

	// Initialize the direct input interface for the mouse.
	result = m_directInput->CreateDevice(GUID_SysMouse, &m_mouse, NULL);
	if(FAILED(result))
	{
		return false;
	}

	// Set the data format for the mouse using the pre-defined mouse data format.
	result = m_mouse->SetDataFormat(&c_dfDIMouse);
	if(FAILED(result))
	{
		return false;
	}

	// Set the cooperative level of the mouse to share with other programs.
	result = m_mouse->SetCooperativeLevel(hwnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);
	if(FAILED(result))
	{
		return false;
	}

	// Acquire the mouse.
	result = m_mouse->Acquire();
	if(FAILED(result))
	{
		return false;
	}

	return true;
}


void InputClass::Shutdown()
{
	// Release the mouse.
	if(m_mouse)
	{
		m_mouse->Unacquire();
		m_mouse->Release();
		m_mouse = 0;
	}

	// Release the keyboard.
	if(m_keyboard)
	{
		m_keyboard->Unacquire();
		m_keyboard->Release();
		m_keyboard = 0;
	}

	// Release the main interface to direct input.
	if(m_directInput)
	{
		m_directInput->Release();
		m_directInput = 0;
	}

	return;
}


bool InputClass::Frame()
{
	bool result;

	// ��������� ��������� ����������
	result = ReadKeyboard();
	if(!result){
		return false;
	}

	// ��������� ��������� �����
	result = ReadMouse();
	if(!result){
		return false;
	}

	// Process the changes in the mouse and keyboard.
	ProcessInput();

	return true;
}


bool InputClass::ReadKeyboard(){
	HRESULT result;

	// Read the keyboard device.
	result = m_keyboard->GetDeviceState(sizeof(m_keyboardState), (LPVOID)&m_keyboardState);
	if(FAILED(result)){
		// If the keyboard lost focus or was not acquired then try to get control back.
		if((result == DIERR_INPUTLOST) || (result == DIERR_NOTACQUIRED)){
			m_keyboard->Acquire();
		}else{
			return false;
		}
	}
		
	return true;
}


bool InputClass::ReadMouse(){
	HRESULT result;

	// ��������� �����
	result = m_mouse->GetDeviceState(sizeof(DIMOUSESTATE), (LPVOID)&m_mouseState);
	if(FAILED(result)){
		// ���� �������� ����� �� ����������, �������������� ������ ����
		if((result == DIERR_INPUTLOST) || (result == DIERR_NOTACQUIRED)){
			m_mouse->Acquire();
		}else{
			return false;
		}
	}

	return true;
}

// ������� �������
bool InputClass::IsKeyDown(int key){
	// ��������� �� ������� ������� ��� ������� key
	return (m_keyboardState[key] & 0x80) ? true : false;
}

// ������� �������� �������
bool InputClass::IsKeyUp(int key){

	 return (m_keyboardState[key] & 0x80) ? false : true; 
}

// ������� ��� ������� ������� ��� ��������
bool InputClass::IsKeyPress(int key){

	 // �������� ������� �������
	 if ( IsKeyDown(key) ){
		 m_KeyState[key] = 1;
	 }

	 // check for key reaching the keydown state
	 if (m_KeyState[key] == 1){
		 //check for key release
		 if (IsKeyUp(key)){
			 m_KeyState[key] = 2;
		 }
	 }

	 // ������� ���� ������ ���� ������ � ��������
	 if (m_KeyState[key] == 2){
		 // ���������� ��������� ������
		 m_KeyState[key] = 0;
		 return true;
	 }

	 return false;
}

// ������ ���� ������
bool InputClass::IsMouseDown(int key){

	return (m_mouseState.rgbButtons[key] & 0x80) ? true : false;
}

// ������ ���� ������
bool InputClass::IsMouseUp(int key){
	return (m_mouseState.rgbButtons[key] & 0x80) ? false : true; 
}

// ������ ��� ������ ������ ����
bool InputClass::IsMousePress(int key){

	 // �������� ������� �������
	 if ( IsMouseDown(key) ){
		 m_KeyMouseState[key] = 1;
	 }

	 // check for key reaching the keydown state
	 if (m_KeyMouseState[key] == 1){
		 //check for key release
		 if (IsMouseUp(key)){
			 m_KeyMouseState[key] = 2;
		 }
	 }

	 // ������� ���� ������ ���� ������ � ��������
	 if (m_KeyMouseState[key] == 2){
		 // ���������� ��������� ������
		 m_KeyMouseState[key] = 0;
		 return true;
	 }

	return false;
}


void InputClass::ProcessInput(){
	// ��������� ��������� �������
	// �������� �������� ��������� �������
	m_mouseX += m_mouseState.lX;
	m_mouseY += m_mouseState.lY;
	// ���������� �� ������� ��������� ������
	m_dmouseX = m_mouseState.lX;
	m_dmouseY = m_mouseState.lY;

	// �� �������� ������� ����� �� ����� ����
	if(m_mouseX < 0)  { m_mouseX = 0; }
	if(m_mouseY < 0)  { m_mouseY =  0; }
	
	if(m_mouseX > m_screenWidth)  { m_mouseX = m_screenWidth; }
	if(m_mouseY > m_screenHeight) { m_mouseY = m_screenHeight; }
	
	return;
}

void InputClass::GetMouseLocation(int& mouseX, int& mouseY){
	mouseX = m_mouseX;
	mouseY = m_mouseY;
	return;
}

void InputClass::GetMouseMove(int& dmouseX, int& dmouseY){
	dmouseX = m_dmouseX;
	dmouseY = m_dmouseY;
	return;
}
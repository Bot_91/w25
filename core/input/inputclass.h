////////////////////////////////////////////////////////////////////////////////
// Filename: inputclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _INPUTCLASS_H_
#define _INPUTCLASS_H_


///////////////////////////////
// PRE-PROCESSING DIRECTIVES //
///////////////////////////////
#define DIRECTINPUT_VERSION 0x0800

#define MOUSE_LEFT 0
#define MOUSE_RIGHT 1
#define MOUSE_MIDDLE 2

/////////////
// LINKING //
/////////////
#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")


//////////////
// INCLUDES //
//////////////
#include <dinput.h>

// ����������� ������, ������� ��� �� �� ����� ��� �����

////////////////////////////////////////////////////////////////////////////////
// Class name: InputClass
////////////////////////////////////////////////////////////////////////////////
class InputClass
{
public:
	InputClass();
	InputClass(const InputClass&);
	~InputClass();

	bool Initialize(HINSTANCE, HWND, int, int);
	void Shutdown();
	bool Frame();

	bool IsKeyDown(int key);
	bool IsKeyPress(int key);
	bool IsKeyUp(int key);

	void GetMouseLocation(int&, int&);
	void GetMouseMove(int&, int&);

	bool IsMouseDown(int key);
	bool IsMousePress(int key);
	bool IsMouseUp(int key);
private:
	bool ReadKeyboard();
	bool ReadMouse();
	void ProcessInput();

private:
	IDirectInput8* m_directInput;
	IDirectInputDevice8* m_keyboard;
	IDirectInputDevice8* m_mouse;

	unsigned char m_keyboardState[256];
	int m_KeyState[256];

	DIMOUSESTATE m_mouseState;
	int m_KeyMouseState[256];

	int m_screenWidth, m_screenHeight;
	int m_mouseX, m_mouseY;
	int m_dmouseX, m_dmouseY;
};

#endif
// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
///////////////////////////////////////////////////////////////////////////////
// Filename: textclass.cpp
///////////////////////////////////////////////////////////////////////////////
#include "textclass.h"


TextClass::TextClass(){
	m_Font = 0;
	for (unsigned int i=0; i<m_sentence.size(); i++){
		m_sentence[i] = 0;
	}
	m_sentence.clear();

	m_screenWidth = 0;
	m_screenHeight = 0;


}


TextClass::TextClass(const TextClass& other){
	m_Font = 0;
	m_screenWidth = 0;
	m_screenHeight = 0;
}


TextClass::~TextClass(){
}

// ������� �������� ������, ������� ����� ������� �� �������
bool TextClass::Initialize(ID3D10Device* device, int screenWidth, int screenHeight, float fontSize){
	bool result;

	m_screenWidth = screenWidth; 
	m_screenHeight = screenHeight; 
	// Create the font object.
	m_Font = new FontClass;
	if(!m_Font){
		return false;
	}

	// Initialize the font object.
	result = m_Font->Initialize(device, "./data/fontdata.txt", "./data/font.dds", fontSize);
	if(!result){
		return false;
	}

	return true;
}

// ����������� ������ ������ ������ ������������ 256
bool TextClass::AddSentence(ID3D10Device* device, std::string text, 
							float positionX, float positionY, 
							D3DXVECTOR3 color){
	bool result;
	// �������� ������ �������
	SentenceType* m_sentence_buff=0;
	// Initialize the first sentence.
	result = InitializeSentence(&m_sentence_buff, (256), device);
	if(!result){
		return false;
	}
	// ������ ������������������ ������ ��������� � ������
	m_sentence.push_back(m_sentence_buff);

	// Now update the sentence vertex buffer with the new string information.
	result = UpdateSentence(m_sentence.back(), text, positionX, positionY, color.x, color.y, color.z);
	if(!result){
		return false;
	}

	return true;
}
unsigned int TextClass::GetCountSentence(){
	return m_sentence.size();
}
void TextClass::Shutdown(){
	// �� ������� ����������� sentence.
	for (unsigned int i=0; i<m_sentence.size(); i++){
		ReleaseSentence(&m_sentence[i]);
		m_sentence[i]=0;
	}
	m_sentence.clear();

	// ����������� ������ ������.
	if(m_Font){
		m_Font->Shutdown();
		delete m_Font;
		m_Font = 0;
	}

	return;
}


void TextClass::Render( ID3D10Device* device, ShaderManagerClass* p_ShaderManager,
						TextureManagerClass* p_TextureManager,
						D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, 
						D3DXMATRIX projectionMatrix){
	// ��������� ����� ������� ��� ����� number sentence.
	for (unsigned int index=0; index < m_sentence.size(); index++){
		RenderSentence(device, m_sentence[index]);
		p_ShaderManager->RenderFontShader(device, m_sentence[index]->indexCount, worldMatrix, viewMatrix,
			projectionMatrix, p_TextureManager->GetTexture(this->GetTexture()), this->GetPixelColor(index));
	}
	return; 
}
// ������ ����� ������ � ������� ������� ������
float TextClass::GetLengthText(std::string text) {
	return m_Font->GetLengthText(text);
}
float TextClass::GetFontSize(void){
	return m_Font->GetFontSize();
}
int  TextClass::GetIndexCount(int index){
	return m_sentence[index]->indexCount;
}
std::string TextClass::GetTexture(){
	return m_Font->GetTexture();	
}
D3DXVECTOR4 TextClass::GetPixelColor(int index){
	// Pixel color vector with the input sentence color.
	return D3DXVECTOR4(m_sentence[index]->red, m_sentence[index]->green, m_sentence[index]->blue, 1.0f);
}

bool TextClass::EditText(std::string text, unsigned int number){
	bool result;
	// Update the sentence vertex buffer with the new string information.
	if (number >= m_sentence.size()) {
		return false;
	}
	result = UpdateSentence(m_sentence[number], text, 
							m_sentence[number]->positionX, m_sentence[number]->positionY, 
							m_sentence[number]->red, m_sentence[number]->green, m_sentence[number]->blue);
	if(!result){
		return false;
	}

	return true;
}
bool TextClass::InitializeSentence(SentenceType** sentence, int maxLength, ID3D10Device* device){
	VertexType* vertices;
	unsigned long* indices;
	D3D10_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
    D3D10_SUBRESOURCE_DATA vertexData, indexData;
	HRESULT result;
	int i;


	// Create a new sentence object.
	(*sentence) = new SentenceType;
	if(!*sentence){
		return false;
	}

	// Initialize the sentence buffers to null.
	(*sentence)->vertexBuffer = 0;
	(*sentence)->indexBuffer = 0;

	// Set the maximum length of the sentence.
	(*sentence)->maxLength = maxLength;

	// Set the number of vertices in the vertex array.
	(*sentence)->vertexCount = 6 * maxLength;

	// Set the number of indexes in the index array.
	(*sentence)->indexCount = (*sentence)->vertexCount;

	// Create the vertex array.
	vertices = new VertexType[(*sentence)->vertexCount];
	if(!vertices){
		return false;
	}

	// Create the index array.
	indices = new unsigned long[(*sentence)->indexCount];
	if(!indices){
		return false;
	}
	// Initialize vertex array to zeros at first.
	memset(vertices, 0, (sizeof(VertexType) * (*sentence)->vertexCount));

	// Initialize the index array.
	for(i=0; i<(*sentence)->indexCount; i++)
	{
		indices[i] = i;
	}

	// Set up the description of the dynamic vertex buffer.
    vertexBufferDesc.Usage = D3D10_USAGE_DYNAMIC;
    vertexBufferDesc.ByteWidth = sizeof(VertexType) * (*sentence)->vertexCount;
    vertexBufferDesc.BindFlags = D3D10_BIND_VERTEX_BUFFER;
    vertexBufferDesc.CPUAccessFlags = D3D10_CPU_ACCESS_WRITE;
    vertexBufferDesc.MiscFlags = 0;

	// Give the subresource structure a pointer to the vertex data.
    vertexData.pSysMem = vertices;

	// Create the vertex buffer.
    result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &(*sentence)->vertexBuffer);
	if(FAILED(result)){
		return false;
	}

	// Set up the description of the static index buffer.
    indexBufferDesc.Usage = D3D10_USAGE_DEFAULT;
    indexBufferDesc.ByteWidth = sizeof(unsigned long) * (*sentence)->indexCount;
    indexBufferDesc.BindFlags = D3D10_BIND_INDEX_BUFFER;
    indexBufferDesc.CPUAccessFlags = 0;
    indexBufferDesc.MiscFlags = 0;

	// Give the subresource structure a pointer to the index data.
    indexData.pSysMem = indices;

	// Create the index buffer.
	result = device->CreateBuffer(&indexBufferDesc, &indexData, &(*sentence)->indexBuffer);
	if(FAILED(result)){
		return false;
	}

	// Release the vertex array as it is no longer needed.
	delete [] vertices;
	vertices = 0;

	// Release the index array as it is no longer needed.
	delete [] indices;
	indices = 0;

	return true;
}

// ���� ����� �������� ��������?
bool TextClass::UpdateSentence(SentenceType* sentence, std::string text,
								float positionX, float positionY, 
								float red, float green, float blue){
	int numLetters;
	VertexType* vertices;
	float drawX, drawY;
	void* verticesPtr;
	HRESULT result;

	// Store the position of the sentence.
	sentence->positionX = positionX;
	sentence->positionY = positionY;
	// Store the color of the sentence.
	sentence->red = red;
	sentence->green = green;
	sentence->blue = blue;

	// Get the number of letters in the sentence.
	numLetters = (int)text.length();

	// Check for possible buffer overflow.
	if(numLetters > sentence->maxLength){
		return false;
	}

	// Create the vertex array.
	vertices = new VertexType[sentence->vertexCount];
	if(!vertices){
		return false;
	}

	// Initialize vertex array to zeros at first.
	memset(vertices, 0, (sizeof(VertexType) * sentence->vertexCount));

	// Calculate the X and Y pixel position on the screen to start drawing to.
	drawX = positionX - (float)(m_screenWidth/2);
	drawY = (float)(m_screenHeight/2) - positionY;

	// Use the font class to build the vertex array from the sentence text and sentence draw location.
	m_Font->BuildVertexArray((void*)vertices, text, drawX, drawY);

	// Initialize the vertex buffer pointer to null first.
	verticesPtr = 0;

	// Lock the vertex buffer.
	result = sentence->vertexBuffer->Map(D3D10_MAP_WRITE_DISCARD, 0, (void**)&verticesPtr);
	if(FAILED(result)){
		return false;
	}

	// Copy the vertex array into the vertex buffer.
	memcpy(verticesPtr, (void*)vertices, (sizeof(VertexType) * sentence->vertexCount));

	// Unlock the vertex buffer.
	sentence->vertexBuffer->Unmap();

	// Release the vertex array as it is no longer needed.
	delete [] vertices;
	vertices = 0;
	return true;
}


void TextClass::ReleaseSentence(SentenceType** sentence){
	if(*sentence){
		// Release the sentence vertex buffer.
		if((*sentence)->vertexBuffer){
			(*sentence)->vertexBuffer->Release();
			(*sentence)->vertexBuffer = 0;
		}

		// Release the sentence index buffer.
		if((*sentence)->indexBuffer){
			(*sentence)->indexBuffer->Release();
			(*sentence)->indexBuffer = 0;
		}

		// Release the sentence.
		delete *sentence;
		*sentence = 0;
	}

	return;
}


void TextClass::RenderSentence(ID3D10Device* device, SentenceType* sentence){
	unsigned int stride, offset;

	// Set vertex buffer stride and offset.
    stride = sizeof(VertexType); 
	offset = 0;

	// Set the vertex buffer to active in the input assembler so it can be rendered.
	device->IASetVertexBuffers(0, 1, &sentence->vertexBuffer, &stride, &offset);

    // Set the index buffer to active in the input assembler so it can be rendered.
    device->IASetIndexBuffer(sentence->indexBuffer, DXGI_FORMAT_R32_UINT, 0);

    // Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
    device->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	return;
}
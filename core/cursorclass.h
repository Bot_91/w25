////////////////////////////////////////////////////////////////////////////////
// Filename: cursorclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _CURSORCLASS_H_
#define _CURSORCLASS_H_

#include "widgetclass.h"
////////////////////////////////////////////////////////////////////////////////
// Class name: CursorClass
////////////////////////////////////////////////////////////////////////////////
class CursorClass: public BoxClass {
public:
	CursorClass();
	bool Update(InputClass*, int, int );
	bool Create(ID3D10Device*, TextureManagerClass*, int, int);
	bool Render(ID3D10Device*, ShaderManagerClass*, TextureManagerClass*,
				D3DXMATRIX, D3DXMATRIX, D3DXMATRIX);
	void Shutdown(void);
	std::string keyTexture;
	enum type_state_cursor {ACTIVE, NORMAL, PRESSURE};
	int itemAnimation;
	float timeAnimation;
};

#endif
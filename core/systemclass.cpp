// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
////////////////////////////////////////////////////////////////////////////////
// Filename: systemclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "systemclass.h"

/////////////
// GLOBALS //
/////////////
const bool FULL_SCREEN = false;
const bool VSYNC_ENABLED = false;
const float SCREEN_DEPTH = 250.0f;
const float SCREEN_NEAR = 0.1f;
/////////////////////////
// �������� ��������� //
////////////////////////
PlayState PlayState::m_PlayState(L"PLAY");
AboutState AboutState::m_AboutState(L"ABOUT");
DebugState DebugState::m_DebugState(L"DEBUG");
MenuState MenuState::m_MenuState(L"MENU");

void SaveCapturedBitmap(char* szFileName, HBITMAP hBitmap);

SystemClass::SystemClass(){
	timeDelayFrame = 0;
	timeDelayRender = 0;
	m_D3D = 0;
	m_Timer = 0;	
	m_Input = 0;
	m_MsgManager = 0;
	m_StateManager = 0;
	m_TextureManager = 0;
	m_ModelManager = 0;
	WinCursorX = 0;
	WinCursorY = 0;
	prevScreenWidth = 0;
	prevScreenHeight = 0;
	m_applicationName = 0;
	m_hwnd = 0;
	m_hinstance = 0;
}


SystemClass::SystemClass(const SystemClass& other){
}


SystemClass::~SystemClass(){
}


bool SystemClass::Initialize()
{
	int screenWidth, screenHeight;
	bool result;
	////////////////////////////////////////////////////////////////////////////////
	// �������������� ������ � ������ ������ � ���� ����� ��������� ���������� � �������.
	screenWidth = 0;
	screenHeight = 0;
	// �������������� ���� API.
	InitializeWindows(screenWidth, screenHeight);
	////////////////////////////////////////////////////////////////////////////////
	// ������� ������ �������.
	m_Timer = new TimerClass;
	if(!m_Timer){
		return false;
	}
	// �������������� ������ �������
	result = m_Timer->Initialize();
	if(!result){
		MessageBox(m_hwnd, L"Could not initialize the timer object.", L"Error", MB_OK);
		return false;
	}
	////////////////////////////////////////////////////////////////////////////////
	// ������� ������ �����.
	m_Input = new InputClass;
	if(!m_Input){
		return false;
	}
	// �������������� ������ �����
	result = m_Input->Initialize(m_hinstance, m_hwnd, screenWidth, screenHeight);
	if(!result){
		MessageBox(m_hwnd, L"�� ������� ���������������� ������ �����.", L"Error", MB_OK);
		return false;
	}
	result = m_Input->Frame();
	if (!result) {
		return false;
	}
	////////////////////////////////////////////////////////////////////////////////
	// ������� ����������� ������.  ���� ������ ������������ ��� ������� ��� ����� ����������.
	m_D3D = new D3DClass;
	if(!m_D3D){	
		return false;
	}
	// �������������� ����������� ������
	result = m_D3D->Initialize(screenWidth, screenHeight, VSYNC_ENABLED, 
								m_hwnd, FULL_SCREEN, SCREEN_DEPTH, SCREEN_NEAR);
	if(!result){
		MessageBox(m_hwnd, L"�� ������� ���������������� Direct3D.", L"Error", MB_OK);
		return false;
	}
	////////////////////////////////////////////////////////////////////////////////
	// ������� ������ ����������� ���������.
	m_TextureManager = new TextureManagerClass;
	if(!m_TextureManager){
		MessageBox(m_hwnd, L"Could not initialize the texture manager object.", L"Error", MB_OK);
		return false;
	}
	// ������������, �������� �������� ������ ��������
	m_TextureManager->Initialize(m_D3D->GetDevice());
	////////////////////////////////////////////////////////////////////////////////
	m_ModelManager = new ModelManagerClass;
	if(!m_ModelManager){
		MessageBox(m_hwnd, L"Could not initialize the model manager object.", L"Error", MB_OK);
		return false;
	}
	// ������������, �������� �������� ������ ��������
	m_ModelManager->Initialize(m_D3D->GetDevice());
	////////////////////////////////////////////////////////////////////////////////
	// ������� �������� ���������
	m_MsgManager = new MsgManagerClass;
	if (!m_MsgManager){
		return false;
	}
	////////////////////////////////////////////////////////////////////////////////
	// ������� �������� �������� ���������
	m_StateManager = new StateManagerClass;
	if(!m_StateManager){	
		return false;
	}
	// �������� ��������� � ����, ��������� ��������� Intro
	result = m_StateManager->PushState(m_hwnd, m_D3D, m_MsgManager, m_ModelManager, 
									   m_TextureManager, MenuState::GetInstance());
	if (!result){
		MessageBox(m_hwnd, L"Could not initialize the first state.", L"Error", MB_OK);
		return false;
	}
	////////////////////////////////////////////////////////////////////////////////
	return true;
}


void SystemClass::Shutdown()
{
	// ����������� ������ ��������� ���������
	if(m_MsgManager){
		m_MsgManager->Shutdown();
		delete m_MsgManager;
		m_MsgManager = 0;
	}
	// ����������� ������ ��������� ���������
	if(m_StateManager){
		m_StateManager->Shutdown();
		delete m_StateManager;
		m_StateManager = 0;
	}
	// ����������� ������ ��������� �������
	if(m_TextureManager){
		m_TextureManager->Shutdown();
		delete m_TextureManager;
		m_TextureManager = 0;
	}
	// ����������� ������ ��������� �������
	if(m_ModelManager){
		m_ModelManager->Shutdown();
		delete m_ModelManager;
		m_ModelManager = 0;
	}
	// ����������� ����������� ������
	if(m_D3D){
		m_D3D->Shutdown();
		delete m_D3D;
		m_D3D = 0;
	}
	// ����������� ������ �����
	if(m_Input){
		m_Input->Shutdown();
		delete m_Input;
		m_Input = 0;
	}
	// ����������� ������ �������
	if(m_Timer){
		m_Timer->Shutdown();
		delete m_Timer;
		m_Timer = 0;
	}
	// ����������� ����
	ShutdownWindows();
	
	return;
}

// ���� ���������
// ����������� �������� �������, ��������� 100 ��� � ���� �� �����������
// ��������� ����� �������� � ��������� �����
void SystemClass::Run()
{
	MSG msg;
	bool done, result;
	// ������������� ��������� ���������
	ZeroMemory(&msg, sizeof(MSG));
	
	// ��������� ���� �� ����� ��������� ���������� ������ ���������
	done = false;
	while(!done){
		// Handle the windows messages.
		while(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)){
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		} 

		// ������ ������ ������ �� ����������, ��������� ����������
		if(msg.message == WM_QUIT){
			done = true;
		} else{
			// ��������� ����������
			result = Frame();
			// ��� ������ ��������� ����������
			if(!result){
				done = true;
			}
		}

		// ������ ������ ESC ����� �� ����������
		if(m_Input->IsKeyPress(DIK_ESCAPE) == true){
			done = true;
		}
		if (m_Input->IsKeyPress(DIK_SYSRQ) == true) {
			PrintScreen(m_hwnd);
		}
		
	}

	return;
}


// ��������� ����������
bool SystemClass::Frame(){
	bool result;
	
	// ����������� ������ � ���� ����
	if (GetForegroundWindow() == m_hwnd) {
		SetCursorPos(prevScreenWidth/2, prevScreenHeight/2);
	}
	// ��������� �������
	result = m_Timer->Frame();
	if (!result){
		return false;
	}

	// ��������
	PackMsg sendMsg;
	if (m_Input->IsKeyPress(DIK_F1) == true) {
		sendMsg.aimStateId = type_state::DEBUG;
		if (m_StateManager->CheckStateName(L"DEBUG") == false) {
			sendMsg.type = type_msg::ADD;
		}
		else {
			sendMsg.type = type_msg::ERASE;
		}
	}
	if (m_Input->IsKeyPress(DIK_F2) == true) {
		sendMsg.aimStateId = type_state::MENU;
		if (m_StateManager->CheckStateName(L"MENU") == false) {
			sendMsg.type = type_msg::ADD;
		}
		else {
			sendMsg.type = type_msg::ERASE;
		}
	}
	if (m_Input->IsKeyPress(DIK_F3) == true) {
		sendMsg.aimStateId = type_state::PLAY;
		if (m_StateManager->CheckStateName(L"PLAY") == false) {
			sendMsg.type = type_msg::ADD;
		}
		else {
			sendMsg.type = type_msg::ERASE;
		}
	}
	/////////////////////////////////
	//////////////////////////

	m_MsgManager->AddMsg(sendMsg);
	// ��������� ����� ��� �������� ���������	
	// ���������� ���������
	timeDelayFrame += m_Timer->GetFrameTime();
	if (timeDelayFrame > 1.0f/120.0f) {
		// ��������� ��������� ����� (�������������� ����� ������)
		result = m_Input->Frame();
		if (!result) {
			return false;
		}
		result = m_StateManager->Frame(m_D3D, m_Input, timeDelayFrame);
		timeDelayFrame = 0;
		if (!result) {
			return false;
		}
	}
	// ��������� �������� ���������
	// 62 ����� � �������
	timeDelayRender += m_Timer->GetFrameTime();
	if (timeDelayRender>0.016f){
		result = m_StateManager->Render(m_D3D);
		timeDelayRender = 0;
		if(!result){
			return false;
		}
	}
	////
	// ������������ ������� �� ������������ ���������
	PackMsg msgState;
	
	msgState = m_MsgManager->GetBackMsg();
	switch (msgState.type){
		//////////////////////////////////////////////
		case type_msg::NONE:
			break;
		//////////////////////////////////////////////
		case type_msg::ADD:
			// ����� �������� � ���� �������, ����� ������ �� ������, �������� ��� ������ ����
			switch(msgState.aimStateId){
				case type_state::NONE:
					break;
				case type_state::ABOUT:
					result = m_StateManager->PushState(m_hwnd, m_D3D, m_MsgManager, m_ModelManager, 
														m_TextureManager, AboutState::GetInstance());
					if (!result) {return false;}
					break;
				case type_state::DEBUG:
					result = m_StateManager->PushState(m_hwnd, m_D3D, m_MsgManager, m_ModelManager, 
														m_TextureManager, DebugState::GetInstance());
					if (!result) {return false;}
					break;
				case type_state::MENU:
					result = m_StateManager->PushState(m_hwnd, m_D3D, m_MsgManager, m_ModelManager, 
														m_TextureManager, MenuState::GetInstance());
					if (!result) {return false;}
					break;
				case type_state::PLAY:
					result = m_StateManager->PushState(m_hwnd, m_D3D, m_MsgManager, m_ModelManager, 
														m_TextureManager, PlayState::GetInstance());
					if (!result) {return false;}
					break;
			}
			break;
		//////////////////////////////////////////////
		case type_msg::ERASE:
			// �� ����� ������� �������
			switch (msgState.aimStateId){
				case type_state::NONE:
					result = m_StateManager->EraseState(L"NONE");
					if (!result) {return false;}
					break;
				case type_state::ABOUT:
					result = m_StateManager->EraseState(L"ABOUT");
					if (!result) {return false;}
					break;
				case type_state::DEBUG:
					result = m_StateManager->EraseState(L"DEBUG");
					if (!result) {return false;}
					break;
				case type_state::MENU:
					result = m_StateManager->EraseState(L"MENU");
					if (!result) {return false;}
					break;
				case type_state::PLAY:
					result = m_StateManager->EraseState(L"PLAY");
					if (!result) {return false;}
					break;
			}
			break;
		//////////////////////////////////////////////
		case type_msg::PAUSE:
			break;
		//////////////////////////////////////////////
		case type_msg::RESUME:
			break;
		case type_msg::EXIT:
			return false;
			break;
	}
	////
	

	return true;
}


LRESULT CALLBACK SystemClass::MessageHandler(HWND hwnd, UINT umsg, WPARAM wparam, LPARAM lparam)
{
	return DefWindowProc(hwnd, umsg, wparam, lparam);
}

// ������������ ���� ����������
void SystemClass::InitializeWindows(int& screenWidth, int& screenHeight)
{
	WNDCLASSEX wc;
	DEVMODE dmScreenSettings;
	int posX, posY;


	// Get an external pointer to this object.	
	ApplicationHandle = this;

	// Get the instance of this application.
	m_hinstance = GetModuleHandle(NULL);

	// Give the application a name.
	m_applicationName = L"W25";

	// ��������� ���� Windows
	wc.style         = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc   = WndProc;
	wc.cbClsExtra    = 0;
	wc.cbWndExtra    = 0;
	wc.hInstance     = m_hinstance;
	wc.hIcon		 = LoadIcon(NULL, IDI_WINLOGO);
	wc.hIconSm       = wc.hIcon;
	wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wc.lpszMenuName  = NULL;
	wc.lpszClassName = m_applicationName;
	wc.cbSize        = sizeof(WNDCLASSEX);
	
	// ������������ ���� ���������� 
	RegisterClassEx(&wc);

	// Determine the resolution of the clients desktop screen.
	screenWidth  = GetSystemMetrics(SM_CXSCREEN);
	screenHeight = GetSystemMetrics(SM_CYSCREEN);
	prevScreenWidth = screenWidth;
	prevScreenHeight = screenHeight;
	// �������� ��� ��� ��������������� ������ ���������� �� ���� ����� ��� �������
	if(FULL_SCREEN){
		// If full screen set the screen to maximum size of the users desktop and 32bit.
		memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));
		dmScreenSettings.dmSize       = sizeof(dmScreenSettings);
		dmScreenSettings.dmPelsWidth  = (unsigned long)screenWidth;
		dmScreenSettings.dmPelsHeight = (unsigned long)screenHeight;
		dmScreenSettings.dmBitsPerPel = 32;			
		dmScreenSettings.dmFields     = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		// Change the display settings to full screen.
		ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN);

		// Set the position of the window to the top left corner.
		posX = posY = 0;
	}else{
		// If windowed then set it to 800x600 resolution.
		//screenWidth  = 1366;
		//screenHeight = 200;
		

		// Place the window in the middle of the screen.
		posX = (GetSystemMetrics(SM_CXSCREEN) - screenWidth)  / 2;
		posY = (GetSystemMetrics(SM_CYSCREEN) - screenHeight) / 2;
	}

	// Create the window with the screen settings and get the handle to it.
	m_hwnd = CreateWindowEx(WS_EX_APPWINDOW, m_applicationName, m_applicationName, 
						    WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_POPUP,
						    posX, posY, screenWidth, screenHeight, NULL, NULL, m_hinstance, NULL);

	// Bring the window up on the screen and set it as main focus.
	ShowWindow(m_hwnd, SW_SHOW);
	SetForegroundWindow(m_hwnd);
	SetFocus(m_hwnd);

	// �������� ������ �����
	ShowCursor(false);
	//SetCursor(NULL);
	return;
}


void SystemClass::ShutdownWindows(){
	// Show the mouse cursor.
	ShowCursor(true);

	// Fix the display settings if leaving full screen mode.
	if(FULL_SCREEN){
		ChangeDisplaySettings(NULL, 0);
	}

	// Remove the window.
	DestroyWindow(m_hwnd);
	m_hwnd = NULL;

	// Remove the application instance.
	UnregisterClass(m_applicationName, m_hinstance);
	m_hinstance = NULL;

	// Release the pointer to this class.
	ApplicationHandle = NULL;

	return;
}


LRESULT CALLBACK WndProc(HWND hwnd, UINT umessage, WPARAM wparam, LPARAM lparam){
	switch(umessage){
		// Check if the window is being destroyed.
		case WM_DESTROY:
			PostQuitMessage(0);
			return 0;
			break;
		// Check if the window is being closed.
		case WM_CLOSE:
			PostQuitMessage(0);		
			return 0;
			break;	
		// ����� ��������� ����
		case WM_ACTIVATEAPP:
			return 0;
			break;
		// All other messages pass to the message handler in the system class.
		default:
			return ApplicationHandle->MessageHandler(hwnd, umessage, wparam, lparam);
			break;
		
	}
}

void PrintScreen(HWND hwnd) {
	// ��������� ����� ������
	if (OpenClipboard(hwnd)){
		// ������� �����
		EmptyClipboard();
	}
	int nScreenWidth = GetSystemMetrics(SM_CXSCREEN);
	int nScreenHeight = GetSystemMetrics(SM_CYSCREEN);
	HWND hDesktopWnd = GetDesktopWindow();
	HDC hDesktopDC = GetDC(hDesktopWnd);
	HDC hCaptureDC = CreateCompatibleDC(hDesktopDC);
	HBITMAP hCaptureBitmap = CreateCompatibleBitmap(hDesktopDC,
		nScreenWidth, nScreenHeight);
	SelectObject(hCaptureDC, hCaptureBitmap);
	BitBlt(hCaptureDC, 0, 0, nScreenWidth, nScreenHeight,
		   hDesktopDC, 0, 0, SRCCOPY | CAPTUREBLT); 
	// ������ � ����
	//SaveCapturedBitmap("PrintScreen.bmp", hCaptureBitmap); //Place holder - Put your code
	//											 //here to save the captured image to disk
	// ������ � ����� ������
	SetClipboardData(CF_BITMAP, hCaptureBitmap);
	ReleaseDC(hDesktopWnd, hDesktopDC);
	DeleteDC(hCaptureDC);
	DeleteObject(hCaptureBitmap);
	// ��������� ����� ������
	CloseClipboard(); 

}
/////////////////////////////////////////////////
void SaveCapturedBitmap(char* szFileName, HBITMAP hBitmap) {
	HDC    hdc = NULL;
	FILE * fp = NULL;
	LPVOID pBuf = NULL;
	BITMAPINFO bmpInfo;
	BITMAPFILEHEADER bmpFileHeader;

	hdc = GetDC(NULL);
	ZeroMemory(&bmpInfo, sizeof(BITMAPINFO));
	bmpInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	GetDIBits(hdc, hBitmap, 0, 0, NULL, &bmpInfo, DIB_RGB_COLORS);
	if (bmpInfo.bmiHeader.biSizeImage <= 0) {
		bmpInfo.bmiHeader.biSizeImage = bmpInfo.bmiHeader.biWidth * 
										abs(bmpInfo.bmiHeader.biHeight) * 
										(bmpInfo.bmiHeader.biBitCount + 7) / 8;
	}

	if ((pBuf = malloc(bmpInfo.bmiHeader.biSizeImage)) == NULL) {

		MessageBox(NULL, L"Unable to Allocate Bitmap Memory", L"Error", MB_OK | MB_ICONERROR);
		return;
	}

	bmpInfo.bmiHeader.biCompression = BI_RGB;
	//GetDIBits(hdc, hBitmap, 0, bmpInfo.bmiHeader.biHeight, pBuf, DIB_RGB_COLORS);

	GetDIBits(hdc, hBitmap, 0, bmpInfo.bmiHeader.biHeight, pBuf, &bmpInfo, DIB_RGB_COLORS);

	if (fopen_s(&fp, szFileName, "wb")) {
		MessageBox(NULL, L"Unable to Create Bitmap File", L"Error", MB_OK | MB_ICONERROR);
		return;
	}
	if (fp == 0) {
		MessageBox(NULL, L"Unable to Create Bitmap File", L"Error", MB_OK | MB_ICONERROR);
		return;
	}

	bmpFileHeader.bfReserved1 = bmpFileHeader.bfReserved2 = 0;
	bmpFileHeader.bfSize = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + bmpInfo.bmiHeader.biSizeImage;
	bmpFileHeader.bfType = 'MB';
	bmpFileHeader.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
	fwrite(&bmpFileHeader, sizeof(BITMAPFILEHEADER), 1, fp);
	fwrite(&bmpInfo.bmiHeader, sizeof(BITMAPINFOHEADER), 1, fp);
	fwrite(pBuf, bmpInfo.bmiHeader.biSizeImage, 1, fp);

	if (hdc != NULL) ReleaseDC(NULL, hdc);
	if (pBuf != NULL) free(pBuf);
	if (fp != NULL) fclose(fp);
}
/////////////////////////////////////////////////
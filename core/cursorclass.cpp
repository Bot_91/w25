// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
////////////////////////////////////////////////////////////////////////////////
// Filename: cursorclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "cursorclass.h"

CursorClass::CursorClass() {
	keyTexture = "./data/cursor.png";
	this->Width = 32;
	this->Height = 32;
	itemAnimation = 0;
	timeAnimation = 0.0f;
}

// ���������� ������ � ������
bool CursorClass::Create(ID3D10Device* device, TextureManagerClass* p_TextureManager,
						 int screenWidth, int screenHeight) {
	m_screenWidth = screenWidth;
	m_screenHeight = screenHeight;
	// �������� ������
	// ���� ������
	this->Left = (float)screenWidth / 2.0f;
	this->Top = (float)screenHeight / 2.0f;
	p_TextureManager->Add(device, this->keyTexture);
	this->InitializeBuffers(device);
	this->UpdateBuffers(this->Left, this->Top, this->Width, this->Height, screenWidth, screenHeight);
	return true;
}
bool CursorClass::Render(ID3D10Device* device, ShaderManagerClass* p_ShaderManager,
						 TextureManagerClass* p_TextureManager,
						 D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix,
						 D3DXMATRIX projectionMatrix) {

	this->RenderBuffers(device);
	p_ShaderManager->RenderTextureShader(device, this->m_indexCount,
										 worldMatrix, viewMatrix, projectionMatrix,
										 p_TextureManager->GetTexture(keyTexture));
	return true;
}

bool CursorClass::Update(InputClass* p_Input, int mouseX, int mouseY) {
	this->Left =(float) mouseX;
	this->Top = (float) mouseY;
	this->UpdateBuffers(this->Left, this->Top, this->Width, this->Height, m_screenWidth, m_screenHeight);
	return true;
}

void CursorClass::Shutdown(void) {
//	this->ShutdownBuffers();
}
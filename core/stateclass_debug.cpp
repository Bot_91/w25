// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
////////////////////////////////////////////////////////////////////////////////
// Filename: stateclass_debug.cpp
////////////////////////////////////////////////////////////////////////////////
#include "stateclass_debug.h"
////////////////////////////////////////////////////////////////////////////////
// ��������� ������ ���������� ��� �������
////////////////////////////////////////////////////////////////////////////////
// �����������
DebugState::DebugState(WCHAR* name):StateClass(name)
{
	m_Cpu = 0;	
	m_Fps = 0;
	m_Camera = 0;
	m_ShaderManager = 0;
}

// ���������� ��������� (������������)
bool DebugState::Initialize(HWND hwnd, D3DClass* p_D3D, MsgManagerClass* msgManager, 
							ModelManagerClass* modelManager, TextureManagerClass* textureManager){
	/////////////////////////////////////////////
	// ����������� ��������� ��� �������� �����
	this->p_MsgManager = msgManager;
	this->p_ModelManager = modelManager;
	this->p_TextureManager = textureManager;
	/////////////////////////////////////////////
	bool result;
	// ������� ������ CPU. �����������
	/*m_Cpu = new CpuClass;
	if(!m_Cpu){
		return false;
	}
	// �������������� ������ CPU.
	result = m_Cpu->Initialize();
	if(!result){
		MessageBox(hwnd, L"Could not initialize the CPU object.", L"Error", MB_OK);
		return false;
	}*/

	

	// ������� ������ �������� �������.
	m_ShaderManager = new ShaderManagerClass;
	if(!m_ShaderManager){
		MessageBox(hwnd, L"Could not initialize the texture manager object.", L"Error", MB_OK);
		return false;
	}
	// �������������� ������ �������� �������.
	result = m_ShaderManager->Initialize(p_D3D->GetDevice(), hwnd, this->p_TextureManager);
	if(!result){
		MessageBox(hwnd, L"Could not initialize the shader manager object.", L"Error", MB_OK);
		return false;
	}


	// Create the bitmap object. ������� � GUI
	/*m_Bitmap = new BitmapClass;
	if(!m_Bitmap)
	{
		return false;
	}*/
	
	int screenWidth, screenHeight;
	p_D3D->GetScreenSize(screenWidth, screenHeight);
	// Initialize the bitmap object.
	/*result = m_Bitmap->Initialize(p_D3D->GetDevice(), screenWidth, screenHeight, "./data/cm.bmp", 128, 128);
	if(!result){
		MessageBox(hwnd, L"Could not initialize the bitmap object.", L"Error", MB_OK);
		return false;
	}*/
	// ������� ������ ������.
	/*m_Text = new TextClass;
	if(!m_Text){
		return false;
	}
	///////////////////////
	// ������������ ���������� �������
	result = m_Text->Initialize(p_D3D->GetDevice(), hwnd, screenWidth, screenHeight, 1.0);
	if(!result){
		MessageBox(hwnd, L"Could not initialize the text object.", L"Error", MB_OK);
		return false;
	}*/
	// ��������� ��������� ����� ��������� ������ ��������� �� �����?
	/*for (unsigned int i=0; i<7; i++){
		// ��������� � �������������� ������
		result = m_Text->AddSentence(p_D3D->GetDevice(), "Flyght done", 0.85f, 
									(float)(0.05f+m_Text->GetFontSize()*16*i/screenHeight), 1.0f, 1.0f, 1.0f);
		if(!result){
			MessageBox(hwnd, L"Could not initialize the sentence object.", L"Error", MB_OK);
			return false;
		}
	}*/
	// ������� ������ ������.
	m_Camera = new CameraClass;
	if(!m_Camera){
		return false;
	}
	// ������������� ��������� ��������� ������
	m_Camera->SetPosition(0.0f, 0.0f, 0.0f);
	m_Camera->SetRotation(0.0f, 0.0f, 0.0f);
	m_Camera->Render();


	return true;
}

// �������� ��������� (������������ ������)
bool DebugState::Exit(){
	Shutdown();
	return true;
}
bool DebugState::Render(D3DClass* p_D3D){
	return RenderPrimary(p_D3D);
}
// ���������
bool DebugState::RenderPrimary(D3DClass* p_D3D){
	// ������� 
	D3DXMATRIX  orthoMatrix, worldMatrix, viewMatrix, projectionMatrix, translateMatrix, guiMatrix;
	// ��������� ��������� ������
	m_Camera->Render();
	///////////////////////////////////////////////////////////////////////////////////
	// GUI 2D �������
	///////////////////////////////////////////////////////////////////////////////////	
	p_D3D->TurnZBufferOff();
	// ��� ����� ����������� � 2� ���������, �� ����� ���� ��� ��������� ����� ������������� �����
	p_D3D->GetWorldMatrix(worldMatrix);
	p_D3D->GetOrthoMatrix(orthoMatrix);
	m_Camera->GetMatrixGUI(guiMatrix);

	/*for (unsigned int i=0; i<p_TextureManager->GetCountPack(); i++){
		m_Bitmap->Render(p_D3D->GetDevice(),(i/(float)p_TextureManager->GetCountPack()),0.8f, 128,128);
		// ������������ ������, ��������� ���������� ������
		m_ShaderManager->RenderTextureShader(p_D3D->GetDevice(), m_Bitmap->GetIndexCount(), worldMatrix, 
			guiMatrix, orthoMatrix, p_TextureManager->GetTextureByItem(i));
	}*/

	// ������������ �����, ��������� ������� ��������� ������
	/*for (unsigned int j=0; j<m_Text->GetCountSentence(); j++){
		m_Text->Render(p_D3D->GetDevice(),j);
		m_ShaderManager->RenderFontShader(p_D3D->GetDevice(), m_Text->GetIndexCount(j), worldMatrix, guiMatrix, 
									orthoMatrix, m_Text->GetTexture(), m_Text->GetPixelColor(j));
	}*/
	p_D3D->TurnZBufferOn();
	///////////////////////////////////////////////////////////////////////////////////
	return true;
}
bool DebugState::Update(D3DClass* p_D3D, InputClass* p_Input, float time){
	return UpdatePrimary(p_D3D, p_Input, time);
}
// ���������� �����
bool DebugState::UpdatePrimary(D3DClass* p_D3D, InputClass* p_Input, float time){
	// StateClass
	runTime += time;
	/////////////
	int MouseX;
	int MouseY;
	p_Input->GetMouseLocation(MouseX,MouseY);
	// ��������� ���������� � CPU
	//m_Cpu->Frame();
	// ��������� ���������� � FPS
	//m_Fps->Frame();
	// Convert the mouseX integer to string format.
	
	std::string text;
	char buff[16]; // ��� �������������� float
	// Setup the mouseX string.
	text = "Mouse X:" + std::to_string(MouseX);
	//m_Text->EditText(text,0);	
	// Setup the mouseY string.
	text = "Mouse Y:" + std::to_string(MouseY);
	//m_Text->EditText(text,1);
	// ����� ���������� � �������
	//sprintf_s(buff,"%.4f",p_Timer->GetFrameTime());
	text = "Timer:";
	text = text+buff;
	//m_Text->EditText(text,2);
	//  ����� ���������� � cpu
	//text = "CPU:" + std::to_string(m_Cpu->GetCpuPercentage());
	//m_Text->EditText(text,3);
	//  ����� ���������� � fps
	//text = "FPS:" + std::to_string(m_Fps->GetFps());
	//m_Text->EditText(text,4);
	//  ����� ���������� � ������� ������
	// ��������� ���������� ���� ����� �������
	sprintf_s(buff,"%.2f",runTime);
	text = "Run Time:";
	text = text+buff;
	//m_Text->EditText(text,5);

	//sendString[0] = L'�';
	//_itow_s((int)(sendString[0]), tempString, 10);
	//wcscpy_s(sendString, L"Number:");
	//wcscat_s(sendString, tempString);
	//m_Text->EditText(sendString,6);
	////wcscpy_s(sendString, L"����:");
	////_itow_s((int)(sendString[0]), tempString, 10);
	////wcscat_s(sendString, tempString);
	//wcscpy_s(sendString, L"F-��");
	//m_Text->EditText(sendString,6);
	return true;
}
// ������������� ��������
bool DebugState::Shutdown(){
	// ����������� ������ CPU
	if(m_Cpu){
		m_Cpu->Shutdown();
		delete m_Cpu;
		m_Cpu = 0;
	}
	// ����������� ������ FPS
	if(m_Fps){
		m_Fps->Shutdown();
		delete m_Fps;
		m_Fps = 0;
	}

	// ����������� ������ ������
	if(m_Camera){
		delete m_Camera;
		m_Camera = 0;
	}
	// ����������� ������ ������ �������
	if(m_ShaderManager){
		m_ShaderManager->Shutdown();
		delete m_ShaderManager;
		m_ShaderManager = 0;
	}
	// StateClass
	runTime = 0;
	/////////////////////////////////////////////
	// ���������� ��������� ��� �������� �����
	this->p_MsgManager = 0;
	this->p_ModelManager = 0;
	this->p_TextureManager = 0;
	////////////////////////////////////////////
	return true;
}
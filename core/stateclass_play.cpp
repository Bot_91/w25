// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
////////////////////////////////////////////////////////////////////////////////
// Filename: stateclass_play.cpp
////////////////////////////////////////////////////////////////////////////////
#include "stateclass_play.h"
////////////////////////////////////////////////////////////////////////////////
// ��������� ����
////////////////////////////////////////////////////////////////////////////////
// �����������
// ���������� ����������� � ���������� ���������� � ������
PlayState::PlayState(WCHAR* name):StateClass(name){
	m_Light = 0;
	m_Frustum = 0;
	m_Camera = 0;
	m_Fps = 0;
	m_Player = 0;

	m_Terrain = 0;
	//m_Text = 0;

	m_ShaderManager = 0;
	m_ObjectManager = 0;
	MouseX = 0;
	MouseY = 0;

}

// ���������� ��������� (������������)
bool PlayState::Initialize(HWND hwnd, D3DClass* p_D3D, MsgManagerClass* msgManager, 
							ModelManagerClass* modelManager, TextureManagerClass* textureManager)
{
	/////////////////////////////////////////////
	// ����������� ��������� ��� �������� �����
	this->p_MsgManager = msgManager;
	this->p_ModelManager = modelManager;
	this->p_TextureManager = textureManager;
	/////////////////////////////////////////////
	bool result;
	int screenWidth, screenHeight;
	p_D3D->GetScreenSize(screenWidth, screenHeight);
	// ������� ������ �������� �������.
	// ������ ������ ���� ����� ����������, ����� �������� ��� ��������
	m_ShaderManager = new ShaderManagerClass;
	if(!m_ShaderManager){
		MessageBox(hwnd, L"Could not initialize the texture manager object.", L"Error", MB_OK);
		return false;
	}
	// �������������� ������ �������� �������.
	result = m_ShaderManager->Initialize(p_D3D->GetDevice(), hwnd, this->p_TextureManager);
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize the shader manager object.", L"Error", MB_OK);
		return false;
	}
	
	// ������� ������ FPS.
	m_Fps = new FpsClass;
	if (!m_Fps) {
		return false;
	}
	// �������������� ������ FPS.
	result = m_Fps->Initialize();
	if (!result) {
		MessageBox(hwnd, L"Could not initialize the FPS object.", L"Error", MB_OK);
		return false;
	}


	// ������� ������ �����
	m_Light = new LightClass;
	if(!m_Light){
		return false;
	}
	// ������������ ������� �����
	m_Light->SetAmbientColor(0.15f, 0.15f, 0.15f, 1.0f);
	m_Light->SetDiffuseColor(1.0f, 1.0f, 1.0f, 0.5f);
	m_Light->SetDirection(-0.35f, -0.75f, -0.3f);
	m_Light->SetSpecularColor(0.0f, 0.5f, 0.2f, 1.0f);
	m_Light->SetSpecularPower(32.0f);
	
	// ������� ������ ����������
	m_Terrain = new TerrainClass;
	if (!m_Terrain) {
		return false;
	}
	// ������������ ������� ���������� ����� ��� �������� �������
	result = m_Terrain->Initialize(p_D3D->GetDevice(), m_ShaderManager, 
									p_TextureManager, m_Light, "./data/map.W25map");
	if (!result) {
		MessageBox(hwnd, L"Could not initialize the terrain object.", L"Error", MB_OK);
		return false;
	}
	// ������� ������ ������.
	m_Camera = new CameraClass;
	if(!m_Camera){
		return false;
	}
	// ������������� ��������� ��������� ������
	m_Camera->SetPosition(100.0f, 10.0f, 110.0f);
	m_Camera->SetRotation(45.0f, 0.0f, 0.0f);
	m_Camera->Render();
	// �������������� �������� ��������
	m_ObjectManager = new ObjectManagerClass;
	if (!m_ObjectManager) {
		return false;
	}
	PackObject inObject;
	inObject.key_Model = p_ModelManager->Add(p_D3D->GetDevice(), 
											 p_TextureManager, "./data/smooth_icosphere.obj");
	inObject.position = m_Light->GetDirection();
	inObject.Name = "MoveTarget";
	m_ObjectManager->AddObject(inObject);
	// ������� ������ ��������� ������
	m_Frustum = new FrustumClass;
	if (!m_Frustum){
		return false;
	}

	m_Player = new PlayerClass;
	m_Player->Initialize(m_Camera, "./data/T-44-85.s3m");
	p_ModelManager->Add(p_D3D->GetDevice(), p_TextureManager, "./data/T-44-85.s3m");
	m_Player->position = D3DXVECTOR3(100.0f, 0.0f , 120.0f);
	m_Player->markerMove = m_Player->position;

	m_GuiManager = new GuiManagerClass;
	if (!m_GuiManager) {
		return false;
	}
	m_GuiManager->Initialize(p_D3D->GetDevice(), m_ShaderManager,
							 p_TextureManager, screenWidth, screenHeight);

	LabelClass* Label1 = new LabelClass;
	Label1->Name = "Label1";
	Label1->Text = "X: 0.0";
	Label1->Left = 50;
	Label1->Top = 50;
	m_GuiManager->Add(p_D3D->GetDevice(), Label1);

	LabelClass* Label2 = new LabelClass;
	Label2->Name = "Label2";
	Label2->Text = "Y: 0.0";
	Label2->Left = 50;
	Label2->Top = 75;
	m_GuiManager->Add(p_D3D->GetDevice(), Label2);

	LabelClass* Label3 = new LabelClass;
	Label3->Name = "Label3";
	Label3->Text = "Y: 0.0";
	Label3->Left = 50;
	Label3->Top = 100;
	m_GuiManager->Add(p_D3D->GetDevice(), Label3);

	LabelClass* Label4 = new LabelClass;
	Label4->Name = "Label4";
	Label4->Text = "FPS: 0.0";
	Label4->Left = 50;
	Label4->Top = 125;
	m_GuiManager->Add(p_D3D->GetDevice(), Label4);

	ButtonClass* Button1 = new ButtonClass;
	Button1->Name = "Button1";
	Button1->Text = "Create Off";
	Button1->Width = 100;
	Button1->Height = 50;
	Button1->eventOnClick = NewDelegate(PlayState::GetInstance(), &PlayState::OnClickButtonCreate);
	Button1->Left = 50;
	Button1->Top = 720;
	m_GuiManager->Add(p_D3D->GetDevice(), Button1);

	ButtonClass* Button2 = new ButtonClass;
	Button2->Name = "Button2";
	Button2->Text = "Height Off";
	Button2->Width = 100;
	Button2->Height = 50;
	Button2->eventOnClick = NewDelegate(PlayState::GetInstance(), &PlayState::OnClickButtonFlagHeight);
	Button2->Left = 158;
	Button2->Top = 720;
	m_GuiManager->Add(p_D3D->GetDevice(), Button2);

	ButtonClass* Button3 = new ButtonClass;
	Button3->Name = "Button3";
	Button3->Text = "Mask 1 Off";
	Button3->Width = 100;
	Button3->Height = 50;
	Button3->eventOnClick = NewDelegate(PlayState::GetInstance(), &PlayState::OnClickButtonFlagMask1);
	Button3->Left = 266;
	Button3->Top = 720;
	m_GuiManager->Add(p_D3D->GetDevice(), Button3);

	m_RenderTexture = new RenderTextureClass;
	m_RenderTexture->Initialize(p_D3D->GetDevice(), screenWidth, screenHeight);
	PackTexture buffTexture;
	buffTexture.countLink = 1;
	buffTexture.m_Path = "RenderTexture";
	buffTexture.m_Texture = new TextureClass;
	buffTexture.m_Texture->SetTexture(m_RenderTexture->GetShaderResourceView());
	p_TextureManager->Add(p_D3D->GetDevice(), buffTexture);
	
	LabelClass* Label5 = new LabelClass;
	Label5->Name = "Label5";
	Label5->Text = "X: 0.0 Y: 0.0 Z: 0.0";
	Label5->Left = 25;
	Label5->Top = 160+25;

	LabelClass* Label6 = new LabelClass;
	Label6->Name = "Label6";
	Label6->Text = "Vertex count: 0";
	Label6->Left = 25;
	Label6->Top = 160 + 50;

	LabelClass* Label7 = new LabelClass;
	Label7->Name = "Label7";
	Label7->Text = "Model: NULL";
	Label7->Left = 25;
	Label7->Top = 160 + 75;

	ImageClass* Image1 = new ImageClass;
	Image1->Width = 300;
	Image1->Height = 270;
	Image1->Left = 0;
	Image1->Top = 0;
	Image1->Visible = true;
	Image1->Name = "Image1";
	Image1->keyTexture = "./data/marble.dds";

	ImageClass* Image2 = new ImageClass;
	Image2->Width = 290; 
	Image2->Height = 290/1.78f;
	Image2->Left = 4;
	Image2->Top = 4;
	Image2->Visible = true;
	Image2->Name = "Image2";
	Image2->keyTexture = "RenderTexture";

	PanelClass* Panel1 = new PanelClass;
	Panel1->Width = 300;
	Panel1->Height = 270;
	Panel1->Top = 0;
	Panel1->Left = 0;
	Panel1->Name = "Panel1";
	Panel1->Visible = true;
	Panel1->Add(p_D3D->GetDevice(), Image1);
	Panel1->Add(p_D3D->GetDevice(), Label5);
	Panel1->Add(p_D3D->GetDevice(), Label6);
	Panel1->Add(p_D3D->GetDevice(), Label7);
	Panel1->Add(p_D3D->GetDevice(), Image2);

	m_GuiManager->Add(p_D3D->GetDevice(), Panel1);
	// ���������� ���������
	brushWidth = 7;
	p_TextureManager->Add(p_D3D->GetDevice(), "./data/RED.bmp");
	return true;
}
void PlayState::OnClickButtonFlagHeight(int mouseKey, int mouseX, int mouseY) {
	if (flagHeight == false) {
		flagHeight = true;
		m_GuiManager->SetText("Button2", "Hight On");
	}
	else {
		flagHeight = false;
		m_GuiManager->SetText("Button2", "Hight Off");
	}
}
void PlayState::OnClickButtonFlagMask1(int mouseKey, int mouseX, int mouseY) {
	if (flagMask1 == false) {
		flagMask1 = true;
		m_GuiManager->SetText("Button3", "Mask 1 On");
	}
	else {
		flagMask1 = false;
		m_GuiManager->SetText("Button3", "Mask 1 Off");
	}
}
void PlayState::OnClickButtonCreate(int mouseKey, int mouseX, int mouseY) {
	if (flagCreate == false) {
		flagCreate = true;
		m_GuiManager->SetText("Button1", "Create On");
	} else{
		flagCreate = false;
		m_GuiManager->SetText("Button1", "Create Off");
	}
}
// �������� ��������� (������������ ������)
bool PlayState::Exit(){
	Shutdown();
	return true;
}

bool PlayState::Update(D3DClass* p_D3D, InputClass* p_Input, float time){
	return UpdatePrimary(p_D3D, p_Input, time);
}
// ���������� �����
bool PlayState::UpdatePrimary(D3DClass* p_D3D, InputClass* p_Input, float time){
	// StateClass
	runTime += time;
	// ��������� ���������� � FPS
	m_Fps->Frame();
	/////////////
	int screenWidth;
	int screenHeight;
	p_D3D->GetScreenSize(screenWidth, screenHeight);
	std::string text;
	int dMouseX;
	int dMouseY;
	p_Input->GetMouseLocation(MouseX,MouseY);
	p_Input->GetMouseMove(dMouseX, dMouseY);
	// �������� �� GUI
	bool result = m_GuiManager->Update(p_Input, MouseX, MouseY);
	D3DXVECTOR3 MouseLine = m_Camera->GetCursorView(screenWidth, screenHeight,
													(float)MouseX, (float)MouseY, 15.0f);
	float speed = 75.0f;
	// 2.13 3.8 1366,768/360 �� ��������� ������
	if (p_Input->IsMouseDown(MOUSE_RIGHT) == true){
		m_Camera->AddRotation((float)dMouseY/2.13f, (float)dMouseX/3.8f, 0.0f);	
	}
	
	if (p_Input->IsMouseDown(MOUSE_LEFT) == true && (result == false)) {
		// ������������� ������ �����
		if (flagCreate == true) {
			D3DXVECTOR3 pos;
			if (m_Terrain->GetIntersects(m_Camera->GetPosition(), MouseLine, pos) == true) {
				alter_argument argument = alter_argument::DEFAULT;
				float value = 0.0f;
				if (flagHeight == true) {
					argument = alter_argument::POSSITION;
					value = 10.0f;
				}
				if (flagMask1 == true) {
					argument = alter_argument::MASK1;
					value = 10.0f;
				}
				if (p_Input->IsKeyDown(DIK_LCONTROL) == true) {
					m_Terrain->AlterArea(p_D3D->GetDevice(), pos, -value*time, brushWidth, argument);
				} else {
					m_Terrain->AlterArea(p_D3D->GetDevice(), pos, value*time, brushWidth, argument);
				}
			}
		} else {
			D3DXVECTOR3 pos;
			int target = m_ObjectManager->GetIndexIntersection(m_Camera->GetPosition(), MouseLine);
			// ������� ����
			if ( target > 0) {
				selectIndexObject = target;
			}else {
				// ������ �� ��������
				if (m_Terrain->GetIntersects(m_Camera->GetPosition(), MouseLine, pos) == true) {
					m_Player->SetMoveTo(pos);
				}
				PackObject obj;
				obj = m_ObjectManager->GetPackObject("MoveTarget");

				obj.position = pos;
				m_ObjectManager->SetPackObject(obj);
			}

		}
	}
	if (flagCreate == true) {
		D3DXVECTOR3 pos;
		m_Terrain->GetIntersects(m_Camera->GetPosition(), MouseLine, pos);
		m_Terrain->CreateRound(p_D3D->GetDevice(), pos, (float)brushWidth);							
	}

	D3DXVECTOR3 normal[6];
	float heightPlayer[6] = { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };
	m_Player->MoveTo(time);
	if (m_Player->flagRun == true) {

		D3DXVECTOR3 directionPlayer;
		m_Player->GetDirection(directionPlayer);
		D3DXVECTOR3 directionTangent(0.0f, 1.0f, 0.0f);
		D3DXVec3Cross(&directionTangent, &directionPlayer, &directionTangent);
		D3DXVec3Normalize(&directionTangent, &directionTangent);
		m_Terrain->GetHeight(m_Player->position - 0.5f*directionPlayer - 0.5f*directionTangent,
			heightPlayer[0], normal[0]);
		m_Terrain->GetHeight(m_Player->position - 0.5f*directionTangent,
			heightPlayer[1], normal[1]);
		m_Terrain->GetHeight(m_Player->position + 0.5f*directionPlayer - 0.5f*directionTangent,
			heightPlayer[2], normal[2]);
		m_Terrain->GetHeight(m_Player->position - 0.5f*directionPlayer + 0.5f*directionTangent,
			heightPlayer[3], normal[3]);
		m_Terrain->GetHeight(m_Player->position + 0.5f*directionTangent,
			heightPlayer[4], normal[4]);
		m_Terrain->GetHeight(m_Player->position + 0.5f*directionPlayer + 0.5f*directionTangent,
			heightPlayer[5], normal[5]);
		D3DXVECTOR3 normalAvr = (normal[0] + normal[1] + normal[2] + normal[3] + normal[4] + normal[5]) / 6.0f;
		D3DXVec3Normalize(&normalAvr, &normalAvr);
		m_Player->position.y = (heightPlayer[1] + heightPlayer[4]) / 2.0f + 0.2f;

		D3DXVECTOR3 bCross;
		D3DXVec3Cross(&bCross, &normalAvr, &directionPlayer);
		D3DXVECTOR3 directionPlayerCross;
		D3DXVec3Cross(&directionPlayerCross, &directionPlayer, &D3DXVECTOR3(0.0f, 1.0f, 0.0f));
		// ������
		m_Player->rotation.x = -atan2(D3DXVec3Length(&bCross),
			D3DXVec3Dot(&directionPlayer, &normalAvr)) - (float)D3DX_PI*3.0f / 2.0f;
		D3DXVec3Cross(&bCross, &normalAvr, &directionPlayerCross);
		// ������
		m_Player->rotation.z = -atan2(D3DXVec3Length(&bCross),
			D3DXVec3Dot(&directionPlayerCross, &normalAvr)) - (float)D3DX_PI*3.0f / 2.0f;
	}
	// ��������� �������
	/*if (p_Input->IsKeyPress(DIK_T) == true) {
		selectIndexObject = m_ObjectManager->GetIndexIntersection(m_Camera->GetPosition(), MouseLine);
		
		//m_ObjectManager->Intersection(MouseLine);
	}*/
	if (p_Input->IsKeyPress(DIK_1) == true) {
		m_Terrain->SetStateRender(0);
	}
	if (p_Input->IsKeyPress(DIK_2) == true) {
		m_Terrain->SetStateRender(1);
	}
	if (p_Input->IsKeyPress(DIK_3) == true) {
		m_Terrain->SetStateRender(2);
	}
	// ������������ ����������� ������
	if (selectIndexObject > 0 && flagCreate == false) {
		PackObject target = m_ObjectManager->GetPackObject(selectIndexObject);

		if (target.valueHP <= 0.0f) {
			m_ObjectManager->DeleteObject(p_ModelManager, p_TextureManager, selectIndexObject);
		} else {
			m_Terrain->CreateRound(p_D3D->GetDevice(), target.position, 4.0f);
			text = "X:" + std::to_string(target.position.x) +
				"Y:" + std::to_string(target.position.y) +
				"Z:" + std::to_string(target.position.z);
			m_GuiManager->SetText("Panel1", "Label5", text);
			text = "HP:" + std::to_string(target.valueHP);
			m_GuiManager->SetText("Panel1", "Label6", text);
			text = "ID: " + std::to_string(selectIndexObject);
			m_GuiManager->SetText("Panel1", "Label7", text);
			// ������ � ��������
			RenderModelInTexture(p_D3D, m_ObjectManager->GetModel(selectIndexObject));
		}
	}
	// ��������� ������� ������� � ������������
	//float height;
	if (p_Input->IsKeyDown(DIK_S) == true){
		m_Camera->MovePosition(-speed*time);
		
	}
	if (p_Input->IsKeyDown(DIK_W) == true){
		m_Camera->MovePosition(speed*time);

		/*if (m_Terrain->GetHeight(m_Camera->GetPosition(), height) == true) {
			m_Camera->SetPosition(m_Camera->GetPosition().x, height+1.0f, m_Camera->GetPosition().z);
		}*/
	}
	if (p_Input->IsKeyDown(DIK_A) == true){		
		m_Camera->ShiftPosition(-speed*time,0.0f);
	}
	if (p_Input->IsKeyDown(DIK_D) == true){
		m_Camera->ShiftPosition(speed*time,0.0f);
	}

	D3DXVECTOR3 direction = m_Light->GetDirection();
	if (p_Input->IsKeyDown(DIK_DOWN) == true) {
		m_Light->SetDirection(direction.x, direction.y - 0.01f, direction.z);
	}
	if (p_Input->IsKeyDown(DIK_UP) == true) {
		m_Light->SetDirection(direction.x, direction.y + 0.01f, direction.z);
	}
	if (p_Input->IsKeyDown(DIK_LEFT) == true) {
		m_Light->SetDirection(direction.x , direction.y, direction.z- 0.01f);
	}
	if (p_Input->IsKeyDown(DIK_RIGHT) == true) {
		m_Light->SetDirection(direction.x, direction.y, direction.z + 0.01f);
	}
	// ������������� ������
	if (p_Input->IsKeyPress(DIK_C) == true) {
		// �����
		D3DXVECTOR3 pos;
		if (m_Terrain->GetIntersects(m_Camera->GetPosition(), MouseLine, pos) == true) {
			m_Player->position = pos;
			m_Player->markerMove = m_Player->position;
		}

		
	}
	
	if (p_Input->IsKeyPress(DIK_R) == true) {
		D3DXVECTOR3 pos;
		if (m_Terrain->GetIntersects(m_Camera->GetPosition(), MouseLine, pos) == true) {
			// ��������� �������
			PackObject inObject;
			// ����������� ������, ������ �� �����
			inObject.angleY = m_Camera->GetRotation().y * 0.0174532925f;
			inObject.angleX = 0.0f;
			inObject.Name = "T44" + std::to_string(m_ObjectManager->GetCountObject());
			inObject.key_Model = p_ModelManager->Add(p_D3D->GetDevice(),
													p_TextureManager, "./data/T-44-85.s3m");
			inObject.position = pos;
			// ��������� ������ � ������
			m_ObjectManager->AddObject(inObject);
		}
	}
	
	if (p_Input->IsKeyPress(DIK_F) == true) {
		D3DXVECTOR3 pos;
		if (m_Terrain->GetIntersects(m_Camera->GetPosition(), MouseLine, pos) == true) {
			// ��������� �������
			PackObject inObject;
			// ����������� ������, ������ �� �����
			inObject.angleY = m_Camera->GetRotation().y * 0.0174532925f;
			inObject.angleX = 0.0f;
			inObject.Name = "ED209";
			inObject.key_Model = p_ModelManager->Add(p_D3D->GetDevice(),
				p_TextureManager, "./data/ED-209.obj");
			inObject.position = pos;
			// ��������� ������ � ������
			m_ObjectManager->AddObject(inObject);
		}
	}
	if (p_Input->IsKeyPress(DIK_F8) == true) {
		m_Terrain->SaveTerrainMap("./data/map.W25map");
	}
	if (p_Input->IsKeyPress(DIK_F9) == true) {
		m_Terrain->Initialize(p_D3D->GetDevice(), m_ShaderManager, 
							  p_TextureManager, m_Light, "./data/map.W25map");
	}

	//// Setup the mouseX string.
	//text = "time:" + std::to_string((int)m_Terrain->delayTime);
	m_GuiManager->SetText("Label1", text);
	text = "Y:" + std::to_string(m_Player->position.y);
	m_GuiManager->SetText("Label2", text);
	text = "RENDER TIME:" + std::to_string(m_Terrain->delayTime);
	m_GuiManager->SetText("Label3", text);
	text = "FPS:" + std::to_string(m_Fps->GetFps());
	m_GuiManager->SetText("Label4", text);

	if (p_Input->IsKeyPress(DIK_P) == true) {
		//m_GuiManager->SetVisible("RenderImage", true);
		this->RenderTexture(p_D3D);
		//m_RenderTexture->SaveTexture("PrintScreen.png");
		//D3DX10SaveTextureToFile(p_D3D->m_depthStencilBuffer, D3DX10_IFF_PNG, L"PrintScreen.png");
		//m_GuiManager->SetVisible("RenderImage", true);

	}
	// ��������/���������� ������ ����������
	if (p_Input->IsKeyPress(DIK_X) == true) {
		m_GuiManager->SetVisible("Panel1", false);
	}
	if (p_Input->IsKeyPress(DIK_Z) == true) {
		m_GuiManager->SetVisible("Panel1", true);
	}
	return true;
}
bool PlayState::Render(D3DClass* p_D3D) {
	return RenderPrimary(p_D3D);
}
// ���������
bool PlayState::RenderPrimary(D3DClass* p_D3D) {
	// ������� 

	D3DXMATRIX  orthoMatrix, worldMatrix, viewMatrix, projectionMatrix, translateMatrix, guiMatrix;
	D3DXMATRIX rotationMatrix_X, rotationMatrix_Y, rotationMatrix_Z, rotationMatrix, centerMatrix;
	// ��������� ��������� ������
	m_Camera->Render();
	// ����������� �������: �������, ����, �������� 
	p_D3D->GetWorldMatrix(worldMatrix);
	p_D3D->GetOrthoMatrix(orthoMatrix);
	p_D3D->GetProjectionMatrix(projectionMatrix);
	m_Camera->GetViewMatrix(viewMatrix);

	///////////////////////////////////////////////////////////////////////////////////
	// 3D �������
	///////////////////////////////////////////////////////////////////////////////////
	
	m_Camera->GetViewMatrix(viewMatrix);
	// ��������� ������� ��������� ������
	m_Frustum->ConstructFrustum(250.0f, projectionMatrix, viewMatrix);

	m_Terrain->Render(p_D3D->GetDevice(), worldMatrix, viewMatrix, projectionMatrix, m_Frustum);
	////////////////////////
	// ������ ������
	/////////////////
	for (int i = 0; i < p_ModelManager->GetCoutDetail(m_Player->keyModel); i++) {
		D3DXVECTOR3 position = m_Player->position;
		D3DXVECTOR3 rotation = m_Player->rotation;

		// ���������� ����� �������
		D3DXVECTOR3 center = p_ModelManager->GetCenterDetail(m_Player->keyModel, i);
		// ������� ���������
		// ������� ��������
		D3DXMatrixRotationYawPitchRoll(&rotationMatrix, rotation.y, rotation.x, rotation.z);

		// ����� ����� �������
		//D3DXMatrixMultiply(&centerMatrix, &translateMatrix, &rotationMatrix);
		// ������� �����������
		D3DXMatrixTranslation(&translateMatrix, position.x, position.y, position.z);
		// �������� ������ ���� ������
		D3DXMatrixMultiply(&worldMatrix, &rotationMatrix, &translateMatrix);

		// �������� ������ ������ �� ������
		p_ModelManager->Render(p_D3D->GetDevice(), m_Player->keyModel, i);
		MaterialInfoType materialDetail = p_ModelManager->GetMaterialDetail(m_Player->keyModel, i);
		// ���������� ������ � ������
		m_ShaderManager->RenderLightShader(p_D3D->GetDevice(),
			p_ModelManager->GetIndexCount(m_Player->keyModel, i),
			worldMatrix, viewMatrix, projectionMatrix,
			p_TextureManager->GetTexture(materialDetail.pathDiffuse),
			m_Light->GetDirection(), m_Light->GetDiffuseColor(),
			m_Light->GetDiffuseColor(),
			m_Light->GetDirection(), m_Light->GetDiffuseColor(), 32);

	}

	// ��������� �� ���� ��������
	for (int k = 0; k<m_ObjectManager->GetCountObject(); k++) {
		// ��������� �� ������ ������ �������
		for (int i = 0; i<p_ModelManager->GetCoutDetail(m_ObjectManager->GetModel(k)); i++) {
			// ������ ���������� �������
			D3DXVECTOR3 position = m_ObjectManager->GetPosition(k);
			// ���������� ����� �������
			D3DXVECTOR3 center = p_ModelManager->GetCenterDetail(m_ObjectManager->GetModel(k), i);
			// ������� ��������
			D3DXMatrixRotationX(&rotationMatrix_X, m_ObjectManager->GetAngleX(k));
			/////////////////////////////////////
			D3DXMatrixRotationY(&rotationMatrix_Y, m_ObjectManager->GetAngleY(k));
			// ���������� ������ ���� ������
			D3DXMatrixMultiply(&rotationMatrix, &rotationMatrix_X, &rotationMatrix_Y);
			// ����� ����� �������
			D3DXVec3TransformCoord(&center, &center, &rotationMatrix);
			// ������ ������ �������
			float radius = p_ModelManager->GetRadiusDetail(m_ObjectManager->GetModel(k), i);
			bool renderModel;
			// ��������� ����� �� ������ � ������ (������ ��� �����)
			renderModel = m_Frustum->CheckSphere(	position.x + center.x,
													position.y + center.y,
													position.z + center.z,
													radius);
			// ������ ������� �������� � ����
			if (renderModel == true) {
				// ���������� ��� ������ ����� ������
				this->countRenderVertex += p_ModelManager->GetVertexCount(m_ObjectManager->GetModel(k), i);
				// ������� �������� 
				//D3DXMatrixRotationY(&worldMatrix, m_ObjectManager->GetAngleY(k));
				//D3DXMatrixRotationX(&rotationMatrix, m_ObjectManager->GetAngleX(k));
				// �������
				//D3DXMatrixScaling(&rotationMatrix, 0.5f, 0.5f, 0.5f);
				// ������� �����������
				D3DXMatrixTranslation(&translateMatrix, position.x, position.y, position.z);
				// �������� ������ ���� ������
				D3DXMatrixMultiply(&worldMatrix, &rotationMatrix, &translateMatrix);

				// �������� ������ ������ �� ������
				p_ModelManager->Render(p_D3D->GetDevice(), m_ObjectManager->GetModel(k), i);
				MaterialInfoType materialDetail = p_ModelManager->GetMaterialDetail(
					m_ObjectManager->GetModel(k), i);
				// ���������� ������ � ������
				m_ShaderManager->RenderLightShader(p_D3D->GetDevice(),
					p_ModelManager->GetIndexCount(m_ObjectManager->GetModel(k), i),
					worldMatrix, viewMatrix, projectionMatrix,
					p_TextureManager->GetTexture(materialDetail.pathDiffuse),
					m_Light->GetDirection(), m_Light->GetDiffuseColor(),
					m_Light->GetDiffuseColor(),
					m_Light->GetDirection(), m_Light->GetDiffuseColor(), 32);
			}
		}
	}
	///////////////////////////////////////////////////////////////////////////////////
	// GUI 2D �������
	///////////////////////////////////////////////////////////////////////////////////	
	p_D3D->TurnZBufferOff();
	p_D3D->GetWorldMatrix(worldMatrix);
	p_D3D->GetOrthoMatrix(orthoMatrix);
	m_Camera->GetMatrixGUI(guiMatrix);
	// ������ GUI
	m_GuiManager->Render(p_D3D->GetDevice(), worldMatrix, guiMatrix, orthoMatrix);
	p_D3D->TurnZBufferOn();

	///////////////////////////////////////////////////////////////////////////////////
	return true;
}

bool PlayState::RenderModelInTexture(D3DClass* p_D3D, std::string keyModel) {
	// Set the render target to be the render to texture.
	m_RenderTexture->SetRenderTarget(p_D3D->GetDevice(), p_D3D->GetDepthStencilView());
	//Clear the render to texture background to blue so we can differentiate it 
	// from the rest of the normal scene.

	// Clear the render to texture.
	m_RenderTexture->ClearRenderTarget(p_D3D->GetDevice(), p_D3D->GetDepthStencilView(),
		1.0f, 1.0f, 1.0f, 1.0f);

	// ������ ������
	// Render the scene now and it will draw to the render to texture instead of the back buffer.
	D3DXMATRIX  worldMatrix, viewMatrix, projectionMatrix;
	// ��������� ��������� ������
	m_Camera->Render();
	// ����������� �������: �������, ����, �������� 
	p_D3D->GetWorldMatrix(worldMatrix);
	p_D3D->GetProjectionMatrix(projectionMatrix);
	m_Camera->GetMatrixModel(viewMatrix);
	for (int i = 0; i < p_ModelManager->GetCoutDetail(keyModel); i++) {
		// �������� ������ ������ �� ������
		p_ModelManager->Render(p_D3D->GetDevice(), keyModel, i);
		MaterialInfoType materialDetail = p_ModelManager->GetMaterialDetail(keyModel, i);
		// ���������� ������ � ������
		m_ShaderManager->RenderLightShader(p_D3D->GetDevice(),
			p_ModelManager->GetIndexCount(keyModel, i),
			worldMatrix, viewMatrix, projectionMatrix,
			p_TextureManager->GetTexture(materialDetail.pathDiffuse),
			m_Light->GetDirection(), m_Light->GetDiffuseColor(),
			m_Light->GetDiffuseColor(),
			m_Light->GetDirection(), m_Light->GetDiffuseColor(), 32);
	}



	// Reset the render target back to the original back buffer and not the render to texture anymore.
	p_D3D->SetBackBufferRenderTarget();
	return true;
}
// ������������� ��������
bool PlayState::Shutdown(){
	// ����������� ������ �����������
	// ����������� ������ �����
	if(m_Terrain){
		m_Terrain->Shutdown();
		delete m_Terrain;
		m_Terrain = 0;
	}
	// Release the frustum object.
	if(m_Frustum){
		delete m_Frustum;
		m_Frustum = 0;
	}
	// ����������� ������ ������
	if(m_Camera){
		delete m_Camera;
		m_Camera = 0;
	}
	// ����������� ������ FPS
	if (m_Fps) {
		m_Fps->Shutdown();
		delete m_Fps;
		m_Fps = 0;
	}
	// ����������� ������ ������ �������
	if(m_ShaderManager){
		m_ShaderManager->Shutdown();
		delete m_ShaderManager;
		m_ShaderManager = 0;
	}
	// ����������� ������ �������� ��������
	if (m_ObjectManager) {
		m_ObjectManager->Shutdown(p_ModelManager, p_TextureManager);
		delete m_ObjectManager;
		m_ObjectManager = 0;
	}
	if (m_Player) {
		// �� ������� ������ ������ � ��� �������� ��� ������ ������������ �� ��������?!
		delete m_Player;
		m_Player = 0;
	}
	// StateClass
	runTime = 0;
	return true;
}
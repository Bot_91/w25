// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
////////////////////////////////////////////////////////////////////////////////
// Filename: msgmanagerclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "msgmanagerclass.h"
// ������������ ���������
PackMsg::PackMsg(){
	this->aimStateId =  type_state::NONE; 
	this->stateId =  type_state::NONE;
	this->type = type_msg::NONE;
	this->time = 0.0f;
}
// ��������� ���������
bool MsgManagerClass::AddMsg(PackMsg msg){
	m_Msg.push_back(msg);
	return true;
}
// ������� ���� ��������� �� ������
bool MsgManagerClass::EraseMsg(int item){
	if (item < (int)m_Msg.size()){
		m_Msg.erase(m_Msg.begin() + item);
		return true;
	}
	return false;
}
// ������� ��� ���������
void MsgManagerClass::Shutdown(){
	m_Msg.clear();
}
// ���������� ���������� ���������
int MsgManagerClass::GetCountMsg(){

	return m_Msg.size();
}
// ���������� ��������� �� ������
PackMsg MsgManagerClass::GetMsg(int item){

	return m_Msg[item];
}
// ���������� ������ ��������� � ������� ���
PackMsg MsgManagerClass::GetBackMsg(){
	PackMsg result;
	
	// ������� ��������� �� �������
	if (this->GetCountMsg()>0){
		result = m_Msg[0];
		this->EraseMsg(0);
	}
	return result;
}
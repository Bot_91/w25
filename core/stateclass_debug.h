#ifndef _STATECLASS_DEBUG_H_
#define _STATECLASS_DEBUG_H_

#include "stateclass.h"
////////////////////////////////////////////////////////////////////////////////
// ��������� ������ ���������� ��� �������
////////////////////////////////////////////////////////////////////////////////
class DebugState: public StateClass 
{
public:
	DebugState(WCHAR*);
	//���������� ��������� �� ������ ��� ��������� � �������
	static DebugState* GetInstance() { return &m_DebugState; }
		// ��������� � ���������� ���������
	bool Render(D3DClass*); 
	bool Update(D3DClass*, InputClass*, float);
	// �������� ��� ����� � �����(�������� ������)
	bool Initialize(HWND, D3DClass*, MsgManagerClass*, ModelManagerClass*, TextureManagerClass*); 
	// �������� �� ������ �� ������(�������� �������� ��������)
	bool Exit(); 
	// ������������ ���� ��������
	bool Shutdown();
private:
	bool RenderPrimary(D3DClass*); 
	bool UpdatePrimary(D3DClass*, InputClass*, float);
	static DebugState m_DebugState; // ������������
	// ������ CPU
	CpuClass* m_Cpu;
	// ������ FPS
	FpsClass* m_Fps;
	// ������
	CameraClass* m_Camera;

	// �������� ��������
	ShaderManagerClass* m_ShaderManager;
};

#endif
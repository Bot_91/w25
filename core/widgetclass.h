////////////////////////////////////////////////////////////////////////////////
// Filename: widget.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _WIDGETCLASS_H_
#define _WIDGETCLASS_H_



#include ".\graphic\texturemanagerclass.h"
#include ".\graphic\shadermanagerclass.h"


#include ".\input\inputclass.h"



#include "delegate.h"
///////////////////////
// MY CLASS INCLUDES //
///////////////////////
class WidgetClass {
public:
	WidgetClass();
	virtual ~WidgetClass();
	float Width;
	float Height;
	float Top;
	float Left;
	int Cursor; // ��� ������� ��� ���������
	std::string Name;
	std::string Text;
	bool Enabled;
	bool Visible;
	int m_screenWidth;
	int m_screenHeight;
	// ������ �� �������
	bool GetReside(int, int);
	virtual bool Update(InputClass*, int, int);
	virtual bool Render(ID3D10Device*, ShaderManagerClass*, TextureManagerClass*,
						D3DXMATRIX, D3DXMATRIX, D3DXMATRIX);
	virtual bool Create(ID3D10Device*, TextureManagerClass*, int, int);	
	virtual void Shutdown(void);
	virtual void SetText(std::string);
	virtual bool SetText(std::string, std::string);
	virtual bool SetVisible(bool);
	std::string GetText(void);
};

// ������� � ����������
class BoxClass : public WidgetClass {
public:
	~BoxClass();
	void RenderBuffers(ID3D10Device*);
	bool InitializeBuffers(ID3D10Device*);
	bool UpdateBuffers(float, float, float, float, int screenWidth, int screenHeight);
	void ShutdownBuffers();
	ID3D10Buffer *m_vertexBuffer, *m_indexBuffer;
	int m_vertexCount, m_indexCount;
	float m_previousPosX, m_previousPosY;

};
#endif

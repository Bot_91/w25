////////////////////////////////////////////////////////////////////////////////
// Filename: cursorclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _IMAGECLASS_H_
#define _IMAGECLASS_H_

#include "widgetclass.h"

class ImageClass : public BoxClass{
public:
	bool Create(ID3D10Device*, TextureManagerClass*, int, int);
	bool Render(ID3D10Device*, ShaderManagerClass*, TextureManagerClass*,
				D3DXMATRIX, D3DXMATRIX, D3DXMATRIX);
	std::string keyTexture;
	ImageClass();

};

#endif
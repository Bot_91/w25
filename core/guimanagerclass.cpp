// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
////////////////////////////////////////////////////////////////////////////////
// Filename: guimanagerclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "guimanagerclass.h"
//
GuiManagerClass::GuiManagerClass(void) {
	m_Cursor = 0;
	p_TextureManager = 0;
	p_ShaderManager = 0;
	screenWidth = 0;
	screenHeight = 0;
}
bool  GuiManagerClass::Initialize(  ID3D10Device* device, ShaderManagerClass* p_ShaderMng, 
									TextureManagerClass* p_TextureMng, int scrnWidth, int scrnHeight) {
	p_TextureManager = p_TextureMng;
	p_ShaderManager = p_ShaderMng;
	screenWidth = scrnWidth;
	screenHeight = scrnHeight;
	m_Cursor = new CursorClass;
	m_Cursor->Create(device, p_TextureManager, screenWidth, screenHeight);
	return true;

}
void GuiManagerClass::Render(	ID3D10Device* device, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, 
								D3DXMATRIX projectionMatrix) {
	for (unsigned int i = 0; i < m_Widget.size(); i++) {
		if (m_Widget[i]->Visible == true) {
			m_Widget[i]->Render(device, p_ShaderManager, p_TextureManager,
				worldMatrix, viewMatrix, projectionMatrix);
		}
	}
	if (m_Cursor) {
		m_Cursor->Render(device, p_ShaderManager, p_TextureManager,
						 worldMatrix, viewMatrix, projectionMatrix);
	}
}
// ��������� ������ ������, � ������ � � ���� ���������� ������, �� �� ����� (:
void GuiManagerClass::Add(ID3D10Device* device, WidgetClass* widget){

	widget->Create(device, p_TextureManager, screenWidth, screenHeight);
	m_Widget.push_back(widget);
}

// ������� ��� �������
void GuiManagerClass::Shutdown(void){
	for (unsigned int i=0; i< m_Widget.size(); i++){
		m_Widget[i]->Shutdown();
		delete m_Widget[i];
		m_Widget[i] = 0;
	}
	m_Widget.clear();
	if (m_Cursor) {
		m_Cursor->Shutdown();
		delete m_Cursor;
		m_Cursor = 0;
	}
	return;
}

bool GuiManagerClass::Update(InputClass* p_Input,int mouseX, int mouseY) {
	bool result = false;
	for (unsigned int i = 0; i < m_Widget.size(); i++) {
		if (m_Widget[i]->Visible == true) {
			if (m_Widget[i]->Update(p_Input, mouseX, mouseY) == true) {
				result = true;
			}
		}
	}
	m_Cursor->Update(p_Input, mouseX, mouseY);
	return result;
}
// ���� � ��������
bool GuiManagerClass::SetText(std::string NameWidget, std::string Text) {
	// map �� ����� NAME ������� �������� �� �����
	for (unsigned int i = 0; i < m_Widget.size(); i++) {
		if (m_Widget[i]->Name == NameWidget) {
			m_Widget[i]->SetText(Text);
			return true;
		}
	}
	return false;
}
// ���� � �������
bool GuiManagerClass::SetText(std::string NamePanel, std::string NameWidget, std::string Text){
	// map �� ����� NAME ������� �������� �� �����
	for (unsigned int i = 0; i < m_Widget.size(); i++) {
		if (m_Widget[i]->Name == NamePanel) {
			m_Widget[i]->SetText(NameWidget, Text);
			return true;
		}
	}
	return false;
}
bool GuiManagerClass::SetVisible(std::string NameWidget, bool flag) {
	for (unsigned int i = 0; i < m_Widget.size(); i++) {
		if (m_Widget[i]->Name == NameWidget) {
			m_Widget[i]->SetVisible(flag);
			return true;
		}
	}
	return false;
}

unsigned int GuiManagerClass::GetCount(void){
		return m_Widget.size();
}

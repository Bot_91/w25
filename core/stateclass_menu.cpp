// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
////////////////////////////////////////////////////////////////////////////////
// Filename: stateclass_menu.cpp
////////////////////////////////////////////////////////////////////////////////
#include "stateclass_menu.h"
////////////////////////////////////////////////////////////////////////////////
// ��������� ����
////////////////////////////////////////////////////////////////////////////////
// �����������
MenuState::MenuState(WCHAR* name):StateClass(name){
	MouseX = 0;
	MouseY = 0;
	m_Light = 0;
	m_Camera = 0;
	m_Frustum = 0;
	//m_Terrain = 0;
	m_ShaderManager = 0;
	m_ObjectManager = 0;

}
// ���������� ��������� (������������)
bool MenuState::Initialize(HWND hwnd, D3DClass* p_D3D, MsgManagerClass* msgManager, 
							ModelManagerClass* modelManager, TextureManagerClass* textureManager)
{
	/////////////////////////////////////////////
	// ����������� ��������� ��� �������� �����
	this->p_MsgManager = msgManager;
	this->p_ModelManager = modelManager;
	this->p_TextureManager = textureManager;
	/////////////////////////////////////////////
	D3DXMATRIX baseViewMatrix;
	int screenWidth, screenHeight;
	bool result;
	// �������������� ������ � ������ ������ � ���� ����� ��������� ���������� � �������.
	screenWidth = 0;
	screenHeight = 0;

	// ������ ������� ����.

	p_D3D->GetScreenSize(screenWidth, screenHeight);


	// ������� ������ �������� �������.
	m_ShaderManager = new ShaderManagerClass;
	if(!m_ShaderManager){
		return false;
	}
	// �������������� ������ ��������� �������.
	result = m_ShaderManager->Initialize(p_D3D->GetDevice(), hwnd, this->p_TextureManager);
	if(!result){
		MessageBox(hwnd, L"Could not initialize the shader manager object.", L"Error", MB_OK);
		return false;
	}


	// ������ ���� ��������
	// ������� ������ �����
	m_Light = new LightClass;
	if(!m_Light){
		return false;
	}
	// ������������ ������� �����
	m_Light->SetAmbientColor(0.15f, 0.15f, 0.15f, 1.0f);
	m_Light->SetDiffuseColor(1.0f, 1.0f, 1.0f, 0.5f);
	m_Light->SetDirection(1.0f, -0.8f, 0.0f);
	m_Light->SetSpecularColor(0.0f, 0.5f, 0.2f, 1.0f);
	m_Light->SetSpecularPower(64.0f);	

	// ���������� ������������
	// Create the bitmap object.
	// Initialize the bitmap object.
	
	// �������������� �������� ��������
	m_ObjectManager = new ObjectManagerClass;
	if (!m_ObjectManager){
		return false;
	}

	// ������� ������ ������.
	m_Camera = new CameraClass;
	if(!m_Camera){
		return false;
	}
	// ������������� ��������� ��������� ������
	m_Camera->SetPosition(0.0f, 0.0f, 0.0f);
	m_Camera->SetRotation(0.0f, 0.0f, 0.0f);
	m_Camera->GetViewMatrix(baseViewMatrix);
	m_Camera->Render();
	// ������� ������ ��������� ������
	m_Frustum = new FrustumClass;
	if (!m_Frustum){
		return false;
	}
	///////////////////////
	
	// ��������� ��������� ����� �������� ������ ��������� �� �����?
	/*for (unsigned int i=0; i<11; i++){
		// ��������� � �������������� ������
		result = m_Text->AddSentence(p_D3D->GetDevice(), "Flyght done", 0.05f, 
									(float)(0.05f+m_Text->GetFontSize()*16*i/screenHeight), 1.0f, 1.0f, 1.0f);
		if(!result){
			MessageBox(hwnd, L"Could not initialize the sentence object.", L"Error", MB_OK);
			return false;
		}
	}*/
	// ��������� ��� ���� ���������
	// ������� ������ �� �����, � ���������� ���������, � �� �������� ����� ���������
	m_GuiManager = new GuiManagerClass;
	if (!m_GuiManager) {
		return false;
	}
	m_GuiManager->Initialize(p_D3D->GetDevice(), m_ShaderManager, 
							 p_TextureManager, screenWidth, screenHeight);

	// ������ ��� ����� ���
	// ������ ������
	// ��� ������ ������������ ������� ��� ���������������� � ������
	ButtonClass* ButtonStart = new ButtonClass;
	// ����������� ������� � ��������
	ButtonStart->Name = "Button1";
	ButtonStart->eventOnClick = NewDelegate(MenuState::GetInstance(), &MenuState::OnClickButtonStart);
	ButtonStart->Height = 50;
	ButtonStart->Width = 200;
	ButtonStart->Top = 50;
	ButtonStart->Left = 25;
	ButtonStart->Text = "Play";
	//m_GuiManager->Add(p_D3D->GetDevice(), ButtonStart);

	// ������ ������
	ButtonClass* ButtonEnd = new ButtonClass;
	// ����������� ������� � ��������
	ButtonEnd->Name = "Button2";
	ButtonEnd->eventOnClick = NewDelegate(MenuState::GetInstance(), &MenuState::OnClickButtonEnd);
	ButtonEnd->Height = 50;
	ButtonEnd->Width = 200;
	ButtonEnd->Top = 100;
	ButtonEnd->Left = 25;
	ButtonEnd->Text = "Exit";
	//m_GuiManager->Add(p_D3D->GetDevice(), ButtonEnd);

	PanelClass* Panel1 = new PanelClass;
	Panel1->Width = 250;
	Panel1->Height = 300;
	Panel1->Top = 384 - Panel1->Height/2;
	Panel1->Left = 683 - Panel1->Width/2;
	Panel1->Add(p_D3D->GetDevice(), ButtonStart);
	Panel1->Add(p_D3D->GetDevice(), ButtonEnd);
	m_GuiManager->Add(p_D3D->GetDevice(), Panel1);

	LabelClass* Label_1 = new LabelClass;
	Label_1->Name = "Label1";
	Label_1->Left = 10;
	Label_1->Top = 50;
	Label_1->Text = "Text in Label";
	m_GuiManager->Add(p_D3D->GetDevice(), Label_1);
    //&this->m_MenuState

	
	return true;
}
void MenuState::OnClickButtonStart(int mouseKey, int mouseX,int mouseY){
	PackMsg sendMsg;
	sendMsg.aimStateId = type_state::PLAY;
	sendMsg.type = type_msg::ADD;
	p_MsgManager->AddMsg(sendMsg);

	sendMsg.aimStateId = type_state::MENU;
	sendMsg.type = type_msg::ERASE;
	p_MsgManager->AddMsg(sendMsg);
	return;
}
void MenuState::OnClickButtonEnd(int mouseKey, int mouseX,int mouseY){
	PackMsg sendMsg;
	sendMsg.aimStateId = type_state::NONE;
	sendMsg.type = type_msg::EXIT;
	p_MsgManager->AddMsg(sendMsg);

	return;
}
// �������� ��������� (������������ ������)
bool MenuState::Exit()
{
	Shutdown();
	return true;
}
bool MenuState::Render(D3DClass* p_D3D)
{
	return RenderPrimary(p_D3D);
}
// ���������
bool MenuState::RenderPrimary(D3DClass* p_D3D)
{
	// ������� 
	D3DXMATRIX  orthoMatrix, worldMatrix, viewMatrix, projectionMatrix,  guiMatrix;
	D3DXMATRIX translateMatrix,rotationMatrix_X, rotationMatrix_Y, rotationMatrix;
	// ��������� ��������� ������
	m_Camera->Render();
	this->countRenderVertex = 0;
	// ����������� �������: �������, ����, �������� 
	p_D3D->GetWorldMatrix(worldMatrix);
	p_D3D->GetOrthoMatrix(orthoMatrix);
	p_D3D->GetProjectionMatrix(projectionMatrix);
	m_Camera->GetViewMatrix(viewMatrix);
	
	///////////////////////////////////////////////////////////////////////////////////
	// GUI 2D �������
	///////////////////////////////////////////////////////////////////////////////////	
	p_D3D->TurnZBufferOff();
	// ���������� ������� ������// ���� �� �������� � �������� �� ���� ���� ��
	int screenWidth;
	int screenHeight;
	p_D3D->GetScreenSize(screenWidth, screenHeight);
	// ��� ����� ����������� � 2� ���������, �� ����� ���� ��� ��������� ����� ������������� �����
	p_D3D->GetWorldMatrix(worldMatrix);
	p_D3D->GetOrthoMatrix(orthoMatrix);
	m_Camera->GetMatrixGUI(guiMatrix);
	// ������ GUI
	m_GuiManager->Render(	p_D3D->GetDevice(), worldMatrix, guiMatrix, orthoMatrix);
	/*for (unsigned int i=0; i<m_GuiManager->GetCount(); i++){
		// �������� ��������
		
		m_Bitmap->Render(p_D3D->GetDevice(),((float)m_GuiManager->GetLeft(i)/(float)screenWidth),
											((float)m_GuiManager->GetTop(i)/(float)screenHeight),
											(int)m_GuiManager->GetWidth(i),
											(int)m_GuiManager->GetHeight(i));
		// ���������� �������� ������� ��������
		KeyTextureClass textureKey = m_GuiManager->GetKeyTexture(i);
		m_ShaderManager->RenderTextureShader(p_D3D->GetDevice(), m_Bitmap->GetIndexCount(),  
											 worldMatrix, guiMatrix, orthoMatrix, 
											 p_TextureManager->GetTexture(textureKey.diffuse));
	}*/
	/*
		// ������������ ������, ��������� ���������� ������
	*/
	// ������������ �����, ��������� ������� ��������� ������
	/*for (unsigned int j=0; j<m_GuiManager->m_Text->GetCountSentence(); j++){
		m_GuiManager->m_Text->Render(p_D3D->GetDevice(),j);
		m_ShaderManager->RenderFontShader(p_D3D->GetDevice(), m_GuiManager->m_Text->GetIndexCount(j),
										  worldMatrix, guiMatrix, orthoMatrix,
										 m_GuiManager->m_Text->GetTexture(), 
										m_GuiManager->m_Text->GetPixelColor(j));
	}*/
	/*for (unsigned int i=0; i<m_GuiManager->GetCount(); i++){
		m_Bitmap->Render(p_D3D->GetDevice(),float(m_GuiManager->GetLeft(i)/1366.0f),float(m_GuiManager->GetTop(i)/768.0f));
		// ������������ ������, ��������� ���������� ������
		m_ShaderManager->RenderTextureShader(p_D3D->GetDevice(), m_Bitmap->GetIndexCount(), worldMatrix, 
											guiMatrix, orthoMatrix, m_Bitmap->GetTexture());
	}*/
	// �������� �������
	// �������� ��������
	/*m_Bitmap->Render(p_D3D->GetDevice(),float(MouseX/(float)screenWidth),float(MouseY/(float)screenHeight), 32, 32);
	// ������������ ������, ��������� ���������� ������
	m_ShaderManager->RenderTextureShader(p_D3D->GetDevice(), m_Bitmap->GetIndexCount(), worldMatrix, 
											guiMatrix, orthoMatrix, m_Bitmap->GetTexture());*/

	p_D3D->TurnZBufferOn();
	///////////////////////////////////////////////////////////////////////////////////	
	return true;
}
bool MenuState::Update(D3DClass* p_D3D, InputClass* p_Input, float time)
{
	return UpdatePrimary(p_D3D, p_Input, time);
}
// ���������� �����
bool MenuState::UpdatePrimary(D3DClass* p_D3D, InputClass* p_Input, float time){	
	//bool result;
	// StateClass
	runTime += time;
	/////////////
	int dMouseX;
	int dMouseY;
	p_Input->GetMouseLocation(MouseX,MouseY);
	p_Input->GetMouseMove(dMouseX, dMouseY);
	// 2.13 3.8 �� ��������� ������ 768/360 1366/360 
	float speed = 25.0f;
	bool result = m_GuiManager->Update(p_Input, MouseX, MouseY);
	
	if (p_Input->IsMousePress(MOUSE_LEFT) == true && result == false) {
		
	}
	// ��� ������ ������ ������ ���� �� ������ �������
	if (p_Input->IsMouseDown(MOUSE_RIGHT) == true && result == false){
		m_Camera->AddRotation((float)dMouseY/2.13f, (float)dMouseX/3.8f, 0.0f);	
	}
	//m_Camera->AddRotation((float)dMouseY/2.13f, (float)dMouseX/3.8f, 0.0f);	
	if (p_Input->IsKeyDown(DIK_DOWN)==true){
		m_Camera->MovePosition(-speed*time);	
	}
	if (p_Input->IsKeyDown(DIK_UP)==true){
		m_Camera->MovePosition(speed*time);
	}
	if (p_Input->IsKeyDown(DIK_LEFT)==true){
		m_Camera->ShiftPosition(-speed*time,0.0f);
	}
	if (p_Input->IsKeyDown(DIK_RIGHT)==true){
		m_Camera->ShiftPosition(speed*time,0.0f);
	}

	if (p_Input->IsKeyPress(DIK_C)==true){
		// ��������� �������
		PackObject inObject;
		// ����������� ������, ������ �� �����
		inObject.angleY =  m_Camera->GetRotation().y * 0.0174532925f;
		inObject.angleX = 0.0f;
		inObject.key_Model  = p_ModelManager->Add(p_D3D->GetDevice(),p_TextureManager, "./data/T-44-85.s3m");
		// ����������� ����� �������
		inObject.position = inObject.position + m_Camera->GetPosition() + 40.0f*m_Camera->GetView();
		// ��������� ������ � ������
		m_ObjectManager->AddObject(inObject);
	}

	if (p_Input->IsKeyPress(DIK_1) == true){
		PackObject inObject;
		// ����������� ������, ������ �� �����
		// ������������ ������ �� ������
		inObject.angleY = m_Camera->GetRotation().y * 0.0174532925f;
		inObject.angleX = -1.5707963f;
		// ��������� ������						
		inObject.key_Model = p_ModelManager->Add(p_D3D->GetDevice(),
												 p_TextureManager, "./data/t-90a.s3m");
		// ����������� ����� �������
		inObject.position = inObject.position + m_Camera->GetPosition() + 40.0f*m_Camera->GetView();
		// ��������� ������ � ������
		m_ObjectManager->AddObject(inObject);
	}

	if (p_Input->IsKeyPress(DIK_2) == true){
		PackObject inObject;
		// ����������� ������, ������ �� �����
		inObject.angleY =0.0f;
		inObject.angleX = -1.5707963f;

		inObject.position = inObject.position + m_Camera->GetPosition() + 20*m_Camera->GetView();
								
		inObject.key_Model = p_ModelManager->Add(p_D3D->GetDevice(),
												 p_TextureManager, "./data/t-90.3sm");
		// ��������� ������ � ������
		m_ObjectManager->AddObject(inObject);
	}

	if (p_Input->IsKeyPress(DIK_3) == true){
	}
	if (p_Input->IsKeyPress(DIK_4) == true){

		p_TextureManager->Add( p_D3D->GetDevice(), "./data/cm.bmp");
		p_TextureManager->Add( p_D3D->GetDevice(), "./data/hm.bmp");
	}
	if (p_Input->IsKeyPress(DIK_5) == true){
		// �������� �������
	}
	// ��������� ������ �� �� �������� ��������� ��� ������� ����� ������ ����
	if (p_Input->IsMousePress(MOUSE_LEFT) == true){
		for (unsigned int i=0; i<m_GuiManager->GetCount(); i++){
			//m_GuiManager->Update(0, MouseX, MouseY);
		}
	}
	

	if (p_Input->IsKeyPress(DIK_6) == true){
		PackMsg sendMsg;
		sendMsg.aimStateId = type_state::PLAY;
		sendMsg.type = type_msg::ADD;
		p_MsgManager->AddMsg(sendMsg);	
	}
	//m_Light->SetDirection(m_Camera->GetPosition().x, 0.0, 1.0f);
	//m_Light->SetSpecularColor((float)X_Mouse,(float)Y_Mouse,100,0);
	
	// Convert the mouseX integer to string format.
	std::string text;
	/*for (unsigned int k=0; k<m_Text->GetCountSentence(); k++){
		text = "NULL";
		m_Text->EditText(text,k);
	}*/
	// Setup the mouseX string.
	//_itow_s(p_ModelManager->GetCountPack(), tempString, 10);
	// ���������� �������
	//text = "Count Model:" + std::to_string(p_ModelManager->GetCountPack());
	//m_Text->EditText(text,0);
	// ���������� ������
	text = "coorX:" + std::to_string(m_Camera->GetPosition().x);
	m_GuiManager->SetText("Label1",text);
	//text = "coorY:" + std::to_string(m_Camera->GetPosition().y);
	//m_Text->EditText(text,2);
	//text = "coorZ:" + std::to_string(m_Camera->GetPosition().z);
	//m_Text->EditText(text,3);
	// ����������� ������
	//text = "viewX:" + std::to_string(m_Camera->GetView().x);
	//m_Text->EditText(text,4);
	//text = "viewY:" + std::to_string(m_Camera->GetView().y);
	//m_Text->EditText(text,5);
	//text = "viewZ:" + std::to_string(m_Camera->GetView().z);
	//m_Text->EditText(text,6);

	/////
	// ����������� ���������� ��������� ������
	//text ="Vertex:" +  std::to_string(this->countRenderVertex);
	//m_Text->EditText(text,7);
	/////
	/*for (unsigned int i=0; i<p_TextureManager->GetCountPack(); i++)
		if(m_Text->GetCountSentence() >= p_TextureManager->GetCountPack()+3){	
			text = p_TextureManager->GetPathTexture(i);
			m_Text->EditText(text,i+8);	
		}	*/
	return true;	
}
// ������������� ��������
bool MenuState::Shutdown(){
	// ����������� ������ �����������

	
	// ����������� ������ �����
	if(m_Light){
		delete m_Light;
		m_Light = 0;
	}
	// Release the frustum object.
	if(m_Frustum){
		delete m_Frustum;
		m_Frustum = 0;
	}
	// ����������� ������ ������
	if(m_Camera){
		delete m_Camera;
		m_Camera = 0;
	}
	// ����������� ������ ������ �������
	if(m_ShaderManager){
		m_ShaderManager->Shutdown();
		delete m_ShaderManager;
		m_ShaderManager = 0;
	}
	// ����������� ������ �������� ��������
	if(m_ObjectManager){
		m_ObjectManager->Shutdown(p_ModelManager, p_TextureManager);
		delete m_ObjectManager;
		m_ObjectManager = 0;
	}
	// StateClass
	runTime = 0;
	////////////////////////////////////////////
	return true;
}
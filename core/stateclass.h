////////////////////////////////////////////////////////////////////////////////
// Filename: statesclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _STATECLASS_H_
#define _STATECLASS_H_
//////////////
// INCLUDES //
//////////////
#include <iostream>
#include <vector>
///////////////////////
// MY CLASS INCLUDES //
///////////////////////
#include "timerclass.h"

#include ".\input\inputclass.h"

#include ".\graphic\shadermanagerclass.h"
#include ".\graphic\texturemanagerclass.h"
#include ".\graphic\modelmanagerclass.h"

#include "guimanagerclass.h"

#include "msgmanagerclass.h"

#include ".\graphic\objectmanagerclass.h"

#include ".\graphic\cameraclass.h"
#include ".\graphic\frustumclass.h"

#include ".\graphic\staticmodel3dclass.h"



#include ".\graphic\lightclass.h"


#include ".\graphic\object\fpsclass.h"
#include ".\graphic\object\cpuclass.h"

#include ".\graphic\playerclass.h"

#include ".\graphic\terrainclass.h"

#include ".\rendertextureclass.h"

//#include "objectclass.h"
////////////////////////////////////////////////////////////////////////////////
// Struct name: MsgMode
////////////////////////////////////////////////////////////////////////////////
struct ModeStruct
{
	bool pause; // ����� ���������� ����������
	unsigned int itemRender; // ����� ������� ���������� 
	unsigned int itemFrame; // ����� ������� ���������� �����
};
////////////////////////////////////////////////////////////////////////////////
// Structe name: MsgAction
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Class name: State
////////////////////////////////////////////////////////////////////////////////

class StateClass
{ 
public:
	StateClass(WCHAR*);
	virtual ~StateClass();
	// ������ ������ ���������
	virtual bool Initialize(HWND, D3DClass*, MsgManagerClass*, ModelManagerClass*, TextureManagerClass*);	
	// ������������� ���������
	virtual bool Resume();
	// ���������� ��������� � ������ �����
	virtual bool Pause();
	// ����� �� ���������
	virtual bool Exit();
	// ��������� �������� ������� ������ ������� �� �����, ������ ����������
	virtual bool Render(D3DClass*);
	// ���������� ������
	virtual bool Update(D3DClass*, InputClass*, float);
	bool RenderTexture(D3DClass* p_D3D);
	// ���������� �������� ���������
	WCHAR* GetNameState();
	// ���������� ����� ������ ���������
	float GetRunTime();
	// ���������� � ����� ������ ��������
	ModeStruct GetMode();
protected:
	// ����� ���������, �� �������� ���������� �������� ���������
	MsgManagerClass* p_MsgManager; 
	ModelManagerClass* p_ModelManager;
	TextureManagerClass* p_TextureManager;
	RenderTextureClass* m_RenderTexture;
	// �������� ����������
	GuiManagerClass* m_GuiManager;
	// ������������ ���� ��������
	virtual bool Shutdown();
	// �������� ���������
	WCHAR* nameState;
	// ����� ������ ���������
	float runTime;
	// ���������� ����������� ������
	int countRenderVertex;
	// ����� ��������� � ������ �����
	ModeStruct mode;
	// ����� ��� ����?
};
// � ����� ���������� ��� ��������� �������� ��������� �� ��������������� ����������
////////////////////////////////////////////////////////////////////////////////
// C�������� � ���������
////////////////////////////////////////////////////////////////////////////////
class AboutState: public StateClass 
{
public:
	AboutState(WCHAR* name):StateClass(name){}
	//���������� ��������� �� ������ ��� ��������� � �������
	static AboutState* GetInstance() { return &m_AboutState; }
	static AboutState m_AboutState; 
};


#endif
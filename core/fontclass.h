////////////////////////////////////////////////////////////////////////////////
// Filename: fontclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _FONTCLASS_H_
#define _FONTCLASS_H_


//////////////
// INCLUDES //
//////////////
#include <fstream>
//#include <TCHAR.h>
using namespace std;


///////////////////////
// MY CLASS INCLUDES //
///////////////////////
#include "./graphic/textureclass.h"


////////////////////////////////////////////////////////////////////////////////
// Class name: FontClass
////////////////////////////////////////////////////////////////////////////////
class FontClass
{
private:
	struct FontType
	{
		float left, right;
		int size;
	};

	struct VertexType
	{
		D3DXVECTOR3 position;
	    D3DXVECTOR2 texture;
	};

public:
	FontClass();
	FontClass(const FontClass&);
	~FontClass();

	
	bool Initialize(ID3D10Device*, std::string, std::string, float);
	void Shutdown();

	std::string GetTexture();

	void BuildVertexArray(void*, std::string, float, float);
	float GetFontSize(void);
	float GetLengthText(std::string text);
private:
	bool LoadFontData(std::string);
	void ReleaseFontData();

private:
	FontType* fontChar;
	std::string keyTexture;
	float font_size_Y;
	float font_size_X;
};

#endif
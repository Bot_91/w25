#ifndef _STATECLASS_PLAY_H_
#define _STATECLASS_PLAY_H_

#include "stateclass.h"
////////////////////////////////////////////////////////////////////////////////
// ��������� ����
////////////////////////////////////////////////////////////////////////////////
class PlayState: public StateClass 
{ 

public:
	//���������� ��������� �� ������ ��� ���������� ��� � �������� ��������� 
	static PlayState* GetInstance() { return &m_PlayState; }
	// �����������
	PlayState(WCHAR*);
	// ��������� � ���������� ���������
	bool Render(D3DClass*); 
	// ������ � ��������
	bool RenderModelInTexture(D3DClass* p_D3D, std::string);
	bool Update(D3DClass*, InputClass*, float);
	// �������� ��� ����� � �����(�������� ������)
	bool Initialize(HWND, D3DClass*, MsgManagerClass*, ModelManagerClass*, TextureManagerClass*); 
	// �������� �� ������ �� ������(�������� �������� ��������)
	bool Exit(); 
	// ������������ ���� ��������
	bool Shutdown();
private:
	bool flagCreate;
	bool flagHeight;
	bool flagMask1;
	bool flagMask2;
	bool flagMask3;
	int selectIndexObject;
	int brushWidth;
	void OnClickButtonCreate(int mouseKey, int mouseX, int mouseY);
	void OnClickButtonFlagHeight(int mouseKey, int mouseX, int mouseY);
	void OnClickButtonFlagMask1(int mouseKey, int mouseX, int mouseY);
	void OnClickButtonFlagMask2(int mouseKey, int mouseX, int mouseY);
private:
	
	bool RenderPrimary(D3DClass*); 
	bool UpdatePrimary(D3DClass*, InputClass*, float);
	static PlayState m_PlayState;
	// ������ FPS
	FpsClass* m_Fps;
	// ������
	CameraClass* m_Camera;
	PlayerClass* m_Player;
	// ������� ��������� ������
	FrustumClass* m_Frustum;
	// ��������� ����������
	TerrainClass* m_Terrain;
	// �������� ��������
	ObjectManagerClass* m_ObjectManager;
	// ����
	LightClass* m_Light;
	// �������� ��������
	ShaderManagerClass* m_ShaderManager;
	// �����
	//TextClass* m_Text;
	int MouseX;
	int MouseY;
};

#endif
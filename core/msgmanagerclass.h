////////////////////////////////////////////////////////////////////////////////
// Filename: msgmanagerclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _MSGMANAGERCLASS_H_
#define _MSGMANAGERCLASS_H_


//////////////
// INCLUDES //
//////////////
#include <vector>
////////////////////////////////////////////////////////////////////////////////
// Class name: msgmanagerclass
////////////////////////////////////////////////////////////////////////////////
enum class type_state {NONE,DEBUG,ABOUT,MENU,PLAY};
enum class type_msg {NONE,ADD,ERASE,PAUSE,RESUME,EXIT};
class PackMsg{
public:
	PackMsg();
	type_state stateId; // ��� ��������
	type_msg type; // � ��� ��������
	type_state aimStateId;   // ��� ���� ��������
	float time; // ����� ��������
};

class MsgManagerClass{
public:
	bool AddMsg(PackMsg);
	bool EraseMsg(int);
	void Shutdown();
	int GetCountMsg();
	PackMsg GetMsg(int);
	PackMsg GetBackMsg();
private:
	std::vector<PackMsg> m_Msg;
};
#endif
////////////////////////////////////////////////////////////////////////////////
// Filename: guimanagerclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _GUICLASS_H_
#define _GUICLASS_H_

///////////////////////
// MY CLASS INCLUDES //
///////////////////////
#include <vector>

#include ".\graphic\shadermanagerclass.h"
#include ".\graphic\texturemanagerclass.h"

#include "buttonclass.h"


#include "textclass.h"
#include "buttonclass.h"
#include "labelclass.h"
#include "imageclass.h"
#include "panelclass.h"

#include "cursorclass.h"

#include ".\input\inputclass.h"

class GuiManagerClass{
public:
	GuiManagerClass(void);
	void Add(ID3D10Device* device, WidgetClass*);
	void Shutdown(void);
	bool Update(InputClass*, int, int);
	void Render(ID3D10Device*, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX);
	bool SetText(std::string, std::string);
	bool SetText(std::string, std::string, std::string);
	bool SetVisible(std::string, bool);
	unsigned int GetCount(void);
	bool Initialize(ID3D10Device*, ShaderManagerClass*, TextureManagerClass*, int, int);
private:
	ShaderManagerClass* p_ShaderManager;
	TextureManagerClass* p_TextureManager;
	int screenWidth;
	int screenHeight;
private:
	std::vector<WidgetClass*> m_Widget;
	CursorClass* m_Cursor;
	// �����
	
};

#endif

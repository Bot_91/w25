// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
////////////////////////////////////////////////////////////////////////////////
// Filename: stateclass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "stateclass.h"
// �����������
StateClass::StateClass(WCHAR* name)
{
	this->runTime = 0.0f;
	this->countRenderVertex = 0;
	this->nameState = name;
	this->mode.itemRender = 0;
	this->mode.itemFrame = 0;
	this->mode.pause = false;
	this->m_GuiManager = 0;
	this->m_RenderTexture = 0;
	this->p_MsgManager = 0;
	this->p_ModelManager = 0;
	this->p_TextureManager = 0;
}
// �������� � ������� ������ ��� �� ������ ��� ��������� �� ����������������
StateClass::~StateClass(){
	// ������ ������� (�� �����������), ������������ ��� � �������� ��������� 
	this->p_MsgManager = 0; 
	this->p_ModelManager = 0;
	this->p_TextureManager = 0;
	// ������� GUI ������� ���������
	if(this->m_GuiManager){
		this->m_GuiManager->Shutdown();
		delete this->m_GuiManager;
		this->m_GuiManager = 0;
	}
	// ������� ������ ��� ������� � ��������
	if (this->m_RenderTexture) {
		this->m_RenderTexture->Shutdown();
		delete this->m_RenderTexture;
		this->m_RenderTexture = 0;
	}
	this->nameState = 0;
}

bool StateClass::Initialize(HWND hwnd, D3DClass* p_D3D, MsgManagerClass* msgManager, 
							ModelManagerClass* modelManager, TextureManagerClass* textureManager){
	return true;
}

WCHAR* StateClass::GetNameState(){
	return nameState;
}

float StateClass::GetRunTime(){
	return runTime;
}
bool StateClass::Exit(){
	this->Shutdown();
	return true;
}

bool StateClass::Render(D3DClass* p_D3D){
	return true;
}
bool StateClass::Update(D3DClass* p_D3D, InputClass* p_Input, float){
	return true;
}

bool StateClass::Pause(){
	mode.pause = true;
	return true;
}

bool StateClass::Resume(){
	mode.pause = false;
	return true;
}

bool StateClass::Shutdown(){
	return true;
}

ModeStruct StateClass::GetMode(){
	return mode;
}

bool StateClass::RenderTexture(D3DClass* p_D3D){
	// Set the render target to be the render to texture.
	m_RenderTexture->SetRenderTarget(p_D3D->GetDevice(), p_D3D->GetDepthStencilView());
	//Clear the render to texture background to blue so we can differentiate it 
	// from the rest of the normal scene.

		// Clear the render to texture.
	m_RenderTexture->ClearRenderTarget(p_D3D->GetDevice(), p_D3D->GetDepthStencilView(), 
									   0.1f, 0.3f, 0.1f, 1.0f);

	// Render the scene now and it will draw to the render to texture instead of the back buffer.
	Render(p_D3D);

	// Reset the render target back to the original back buffer and not the render to texture anymore.
	p_D3D->SetBackBufferRenderTarget();

	return true;
}




